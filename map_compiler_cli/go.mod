module map_compiler_cli

go 1.22.4

require (
	gitlab.com/coaljoe-test/t2/map_compiler v0.0.0
	kristallos.ga/rx v0.0.0-20230730105147-bf226ad5b775
)

require (
	github.com/StephaneBunel/bresenham v0.0.0-20211027152503-ec76d7b8e923 // indirect
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240506104042-037f3cc74f2a // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/metalim/jsonmap v0.5.0 // indirect
	github.com/qmuntal/gltf v0.24.2 // indirect
	github.com/spyzhov/ajson v0.9.0 // indirect
	github.com/ugurcsen/gods-generic v0.10.4 // indirect
	github.com/veandco/go-sdl2 v0.4.40 // indirect
	gitlab.com/coaljoe-test/t2/rengine v0.0.0-20230720084033-aa352a0f8f93 // indirect
	golang.org/x/image v0.17.0 // indirect
	kristallos.ga/lib/debug v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/ider v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/pubsub v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/sr v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/xlog v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/rx/loaders/gltf_test v0.0.0-00010101000000-000000000000 // indirect
	kristallos.ga/rx/math v0.0.0-20230730105147-bf226ad5b775 // indirect
	kristallos.ga/rx/phys v0.0.0-20210204202831-c0aefa214cc6 // indirect
	kristallos.ga/rx/transform v0.0.0-20230730105147-bf226ad5b775 // indirect
	test/common v0.0.0 // indirect
	test/map_compiler/text v0.0.0-00010101000000-000000000000 // indirect
	test/object_renderer/render_backend/backend v0.0.0 // indirect
	test/object_renderer/render_backend/common v0.0.0 // indirect
	test/object_renderer/render_backend/rx_backend v0.0.0 // indirect
	test/rengine v0.0.0-00010101000000-000000000000 // indirect
)

replace gitlab.com/coaljoe-test/t2/map_compiler => ../map_compiler

replace test/object_renderer/render_backend/backend => ../object_renderer3/render_backend/backend

replace test/object_renderer/render_backend/common => ../object_renderer3/render_backend/common

replace test/object_renderer/render_backend/rx_backend => ../object_renderer3/render_backend/rx_backend

replace kristallos.ga/rx => ../../rx_upstream

//replace kristallos.ga/rx => ../../rx_gltf_test

replace kristallos.ga/rx/loaders/gltf_test => ../../rx_gltf_test/loaders/gltf_test

replace test/rengine => ../rengine_dyn

replace test/common => ../common

replace test/map_compiler/text => ../map_compiler/text
