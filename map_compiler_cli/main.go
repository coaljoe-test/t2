package main

import (
	"fmt"

	"kristallos.ga/rx"

	// Map Compiler base ?
	"gitlab.com/coaljoe-test/t2/map_compiler"
)

func main() {
	fmt.Println("main()")

	// XXX ?
	fmt.Println("main: init map_compiler...")

	map_compiler.Init(512, 512)

	//map_compiler.Test1()
	//map_compiler.Test2()

	//map_compiler.Test3()
	map_compiler.Test4()
	//panic(2)

	/*
	//map_compiler.LoadMapJson("../res/maps/testmap1.json")
	//map_compiler.LoadMapJson("../map_scene2.json")
	map_compiler.LoadMapSceneJson("../map_scene2.json")
	*/
	
	// XXX ?
	showWindow := true
	//showWindow := false
	_ = showWindow
	
	//if true {
	if showWindow {
		rxi := rx.Rxi()
		app := rxi.App
	
		for app.Step() {
			//println("step")
			dt := app.GetDt()
			_ = dt

			//uli.Update(dt)
			//b.Update(dt)

			app.Flip()
		}
	}
}
