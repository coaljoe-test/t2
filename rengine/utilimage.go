package rengine

import (
	"fmt"
	"os"
	"image"
	"image/png"
//	"image/color"
//	"math"
)

func LoadPng(path string) image.Image {
	fmt.Println("LoadPng path:", path)

	//_panic(3)

	f, err := os.Open(path)
    if err != nil {
        panic(err)
    }
    image, _, err := image.Decode(f)

    return image
}

func SavePng(im image.Image, path string) {
	fmt.Println("SavePng path:", path)

	//_panic(3)

	f, err := os.Create(path)
    if err != nil {
        panic(err)
    }
    err = png.Encode(f, im)
	if err != nil {
		panic(err)
	}
}
