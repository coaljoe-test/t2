package main

import (
	"fmt"
	"time"
	ren "gitlab.com/coaljoe-test/t2/rengine"
)

func main() {
	fmt.Println("main()")

	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	//im1 := ren.LoadPng("test_gray8_depth.png")
	//im2 := ren.LoadPng("test_gray8_depth.png")
	im1 := ren.LoadPng("/tmp/q1.png")
	im2 := ren.LoadPng("/tmp/q2.png")
	_ = im1
	_ = im2

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	//ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)

	t := time.Now()
	outIm := ren.MergeDepthImagesGray(im1, im2)
	fmt.Println("d:", time.Now().Sub(t))

	ren.SavePng(outIm, "/tmp/out.png")

	fmt.Println("exiting...")
}
