package main

import (
	"fmt"
//	"os"
	
	//glm "github.com/technohippy/go-glmatrix"
	//lm "github.com/xlab/linmath"
	. "github.com/go-gl/mathgl/mgl64"
	"github.com/gen2brain/raylib-go/raylib"
	
)

var _ = `
//------------------------------------------------------------------------------------
// Define custom functions required for the example
//------------------------------------------------------------------------------------
// Load custom render texture, create a writable depth texture buffer
RenderTexture2D LoadRenderTextureDepthTex(int width, int height)
{
    RenderTexture2D target = { 0 };

    target.id = rlLoadFramebuffer(width, height);   // Load an empty framebuffer

    if (target.id > 0)
    {
        rlEnableFramebuffer(target.id);

        // Create color texture (default to RGBA)
        target.texture.id = rlLoadTexture(0, width, height, PIXELFORMAT_UNCOMPRESSED_R8G8B8A8, 1);
        target.texture.width = width;
        target.texture.height = height;
        target.texture.format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
        target.texture.mipmaps = 1;

        // Create depth texture buffer (instead of raylib default renderbuffer)
        target.depth.id = rlLoadTextureDepth(width, height, false);
        target.depth.width = width;
        target.depth.height = height;
        target.depth.format = 19;       //DEPTH_COMPONENT_24BIT?
        target.depth.mipmaps = 1;

        // Attach color texture and depth texture to FBO
        rlFramebufferAttach(target.id, target.texture.id, RL_ATTACHMENT_COLOR_CHANNEL0, RL_ATTACHMENT_TEXTURE2D, 0);
        rlFramebufferAttach(target.id, target.depth.id, RL_ATTACHMENT_DEPTH, RL_ATTACHMENT_TEXTURE2D, 0);

        // Check if fbo is complete with attachments (valid)
        if (rlFramebufferComplete(target.id)) TRACELOG(LOG_INFO, "FBO: [ID %i] Framebuffer object created successfully", target.id);

        rlDisableFramebuffer();
    }
    else TRACELOG(LOG_WARNING, "FBO: Framebuffer object can not be created");

    return target;
}

// Unload render texture from GPU memory (VRAM)
void UnloadRenderTextureDepthTex(RenderTexture2D target)
{
    if (target.id > 0)
    {
        // Color texture attached to FBO is deleted
        rlUnloadTexture(target.texture.id);
        rlUnloadTexture(target.depth.id);

        // NOTE: Depth texture is automatically
        // queried and deleted before deleting framebuffer
        rlUnloadFramebuffer(target.id);
    }
}
`

// XXX 
// https://github.com/raysan5/raylib/wiki/Frequently-Asked-Questions#how-do-i-create-a-depth-texture
var _ = `
RenderTexture2D LoadRenderTextureWithDepthTexture(int width, int height)
{
    RenderTexture2D target = {0};

    target.id = rlLoadFramebuffer(width, height);   // Load an empty framebuffer

    if (target.id > 0)
    {
        rlEnableFramebuffer(target.id);

        // Create color texture (default to RGBA)
        target.texture.id = rlLoadTexture(NULL, width, height, PIXELFORMAT_UNCOMPRESSED_R8G8B8A8, 1);
        target.texture.width = width;
        target.texture.height = height;
        target.texture.format = PIXELFORMAT_UNCOMPRESSED_R8G8B8A8;
        target.texture.mipmaps = 1;

        // Create depth texture
        target.depth.id = rlLoadTextureDepth(width, height, false);
        target.depth.width = width;
        target.depth.height = height;
        target.depth.format = 19;       //DEPTH_COMPONENT_24BIT?
        target.depth.mipmaps = 1;

        // Attach color texture and depth texture to FBO
        rlFramebufferAttach(target.id, target.texture.id, RL_ATTACHMENT_COLOR_CHANNEL0, RL_ATTACHMENT_TEXTURE2D, 0);
        rlFramebufferAttach(target.id, target.depth.id, RL_ATTACHMENT_DEPTH, RL_ATTACHMENT_TEXTURE2D, 0);

        // Check if fbo is complete with attachments (valid)
        if (rlFramebufferComplete(target.id)) TRACELOG(LOG_INFO, "FBO: [ID %i] Framebuffer object created successfully", target.id);

        rlDisableFramebuffer();
    } 
    else TRACELOG(LOG_WARNING, "FBO: Framebuffer object can not be created");

    return target;
}
`


// XXX ?
func makeImage(camera rl.Camera) {
	fmt.Println("makeImage")

	w := int32(800)
	h := int32(450)
	
	target := rl.LoadRenderTexture(w, h)

	rl.BeginTextureMode(target)
	
	//rl.ClearBackground(rl.Black)
	rl.ClearBackground(rl.Green)

	rl.BeginMode3D(camera)

	//x := rl.NewVector3(0, 0, 0)
	//rl.DrawModel(model, x, 1.0, rl.White)

	rl.EndMode3D()

	rl.EndTextureMode()



	// XXX
	if true {
		tex := target.Texture
		image := rl.LoadImageFromTexture(tex)
		rl.ExportImage(*image, "/tmp/1.png")

		//fmt.Println("ret:", ret)
	}


	// XXX
	// Depth ?
	if true {
		tex := target.Depth
		image := rl.LoadImageFromTexture(tex)
		rl.ExportImage(*image, "/tmp/2.png")

		//fmt.Println("ret:", ret)
	}


	panic(2)
}

func main() {
	fmt.Println("main()")

	////  XXX init ?
	
	//
	// Viz XXX separate ?
	//
	rl.SetConfigFlags(rl.FlagVsyncHint)
	rl.InitWindow(800, 450, "")

	rl.SetTargetFPS(60)


	// Default / start cam position ?
	camPosition := rl.NewVector3(0.0, 10.0, 10.0)

	camera := rl.Camera3D{}
	camera.Position = camPosition
	camera.Target = rl.NewVector3(0.0, 0.0, 0.0)
	camera.Up = rl.NewVector3(0.0, 1.0, 0.0)
	//camera.Fovy = 45.0
	camera.Fovy = 20.0
	//camera.Projection = rl.CameraPerspective
	camera.Projection = rl.CameraOrthographic

	cubePosition := rl.NewVector3(0.0, 0.0, 0.0)
	_ = cubePosition

	//shiftVec := glm.NewVec3()
	shiftAmt := 0.2

	//rl.BeginDrawing()
	//rl.BeginMode3D(camera)
	//mesh := rl.GenMeshCube(1.0, 1.0, 8.0)
	//model := rl.LoadModelFromMesh(mesh)

	//path := "eiffel_tower.obj"
	//path := "zeiffel_tower.obj"
	//path := "../eiffel_tower.obj"
	//path := "../untitled.gltf"
	//path := "../untitled.glb"

	//_, err := os.Open(path)

	//if err != nil {
//		fmt.Println("no such path? path:", path)
//		panic(err)
//	}
	
	//model := rl.LoadModel(path)
	//_ = model
	//fmt.Println("model:", model)
	//if true { panic(2) }
	
	//rl.EndMode3D()
	//rl.EndDrawing()

	//
	// Test
	//
	if true {
		//sb.MoveTo(Vec3{10, 0, 0}, 1.0)
	}


	orbitalCam := false
	// Set an orbital camera mode
	//rl.SetCameraMode(camera, rl.CameraOrbital)
	//camMode := rl.CameraFree
	//camMode := rl.CameraCustom
	defaultCamMode := rl.CameraMode(0)
	// curCamMode ?
	camMode := defaultCamMode
	_ = camMode

	cubePos := Vec3{}
	_ = cubePos

	for !rl.WindowShouldClose() {
		// XXX input
		{
			// Toggle Camera
			if rl.IsKeyPressed(rl.KeySpace) {
				//rl.SetCameraMode(camera, 0)
				orbitalCam = !orbitalCam
				if orbitalCam {
					//rl.SetCameraMode(camera, rl.CameraOrbital)
					camMode = rl.CameraOrbital
				} else {
					//rl.SetCameraMode(camera, 0)
					//camMode = rl.CameraFree
					//camMode = rl.CameraCustom
					camMode = defaultCamMode
					//camera.Position = rl.NewVector3(0.0, 10.0, 10.0)
					camera.Position = camPosition
				}
			}
		
			newPos := Vec3{}
			//newPos := sb.Pos()
		
			if rl.IsKeyDown(rl.KeyRight) {
				newPos[0] += shiftAmt
			}
			if rl.IsKeyDown(rl.KeyLeft) {
				newPos[0] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyUp) {
				//b.pos[1] -= shiftAmt
				newPos[2] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyDown) {
				//b.pos[1] += shiftAmt
				newPos[2] += shiftAmt		
			}

			// XXX ?
			cubePos = newPos
		}

		//rl.UpdateCamera(&camera, camMode) // Update camera
		//rl.UpdateCamera(&camera, 0) // Update camera
	
		if orbitalCam {
			rl.UpdateCamera(&camera, camMode) // Update camera
		}
	
		rl.BeginDrawing()

		rl.ClearBackground(rl.RayWhite)

		rl.BeginMode3D(camera)

		//rl.DrawCube(cubePosition, 2.0, 2.0, 2.0, rl.Red)
		//rl.DrawCubeWires(cubePosition, 2.0, 2.0, 2.0, rl.Maroon)
		if true {
		//if false {
			x := rl.NewVector3(float32(cubePos[0]), float32(cubePos[1]), float32(cubePos[2]))
			rl.DrawCube(x, 2.0, 2.0, 2.0, rl.Red)
			rl.DrawCubeWires(x, 2.0, 2.0, 2.0, rl.Maroon)
		}

		if true {
		//if false {
			//x := rl.NewVector3(0, 0, 0)
			//rl.DrawModel(model, x, 1.0, rl.White)
			//rl.DrawModel(model, x, 1.0, rl.Blue)
			//rl.DrawModel(model, x, 4.0, rl.White)

			// .obj z-up
			//if true {
			//	rotAxis := rl.NewVector3(1, 0, 0)
			//	rotAngle := float32(-90.0)
			//	scale := rl.NewVector3(1, 1, 1)
			//	rl.DrawModelEx(model, x, rotAxis, rotAngle, scale, rl.Blue)
			//}
		}

		makeImage(camera)

		rl.DrawGrid(10, 1.0)

		rl.EndMode3D()


		//rl.DrawText("Congrats! You created your first window!", 190, 200, 20, rl.LightGray)

		rl.EndDrawing()
	}

	rl.CloseWindow()
}
