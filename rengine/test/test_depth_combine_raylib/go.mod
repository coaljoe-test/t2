module object_renderer

go 1.20

require (
	github.com/gen2brain/raylib-go/raylib v0.0.0-20230719211022-1083eace2049
	github.com/go-gl/mathgl v1.0.0
)

require golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
