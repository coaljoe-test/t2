package main

import (
	"fmt"
	ren "gitlab.com/coaljoe-test/t2/rengine"
)

func main() {
	fmt.Println("main()")

	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	im := ren.LoadPng("test_gray8_depth.png")
	_ = im

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)

	ren.SavePng(im, "/tmp/out.png")

	fmt.Println("exiting...")
}
