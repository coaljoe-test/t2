#version 140
in vec2 Position;

in vec2 vTexCoord;

out vec2 fTexCoord;

void main()
{
	fTexCoord = vTexCoord;
	
	gl_Position = vec4( Position.x, Position.y, 0, 1 );
}
