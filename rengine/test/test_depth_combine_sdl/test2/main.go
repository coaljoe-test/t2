package main

import (
	"fmt"
	//gl "github.com/chsc/gogl/gl33"
	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/veandco/go-sdl2/sdl"
	//"math"
	"os"
	"image"
	"image/draw"
	_ "image/png"
	"runtime"
	"time"
)

func createprogram() uint32 {
	// VERTEX SHADER
	vs := gl.CreateShader(gl.VERTEX_SHADER)
	vs_source, free := gl.Strs(vertexShaderSource + "\x00")
	gl.ShaderSource(vs, 1, vs_source, nil)
	free()
	gl.CompileShader(vs)
	
	var vs_status int32
	gl.GetShaderiv(vs, gl.COMPILE_STATUS, &vs_status)
	fmt.Printf("Compiled Vertex Shader: %v\n", vs_status)

	if vs_status != gl.TRUE {
		panic("vertex shader error")
	}

	// FRAGMENT SHADER
	fs := gl.CreateShader(gl.FRAGMENT_SHADER)
	fs_source, free := gl.Strs(fragmentShaderSource + "\x00")
	gl.ShaderSource(fs, 1, fs_source, nil)
	free()
	gl.CompileShader(fs)
	
	var fstatus int32
	gl.GetShaderiv(fs, gl.COMPILE_STATUS, &fstatus)
	fmt.Printf("Compiled Fragment Shader: %v\n", fstatus)
	
	if fstatus != gl.TRUE {
		panic("fragment shader error")
	}

	// CREATE PROGRAM
	program := gl.CreateProgram()
	gl.AttachShader(program, vs)
	gl.AttachShader(program, fs)
	
	/*
	fragoutstring, free := gl.Strs("outColor")
	gl.BindFragDataLocation(program, 0, &fragoutstring)
	free()
	*/
	
	//fragoutstring, free := gl.Strs("outColor")
	//gl.BindFragDataLocation(program, 0, &fragoutstring)
	//free()

	gl.LinkProgram(program)
	var linkstatus int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &linkstatus)
	fmt.Printf("Program Link: %v\n", linkstatus)

	if linkstatus != gl.TRUE {
		panic("link shader error ?")
	}


	return program
}

func newTexture(file string) (uint32, error) {
	imgFile, err := os.Open(file)
	if err != nil {
		return 0, fmt.Errorf("texture %q not found on disk: %v", file, err)
	}
	img, _, err := image.Decode(imgFile)
	if err != nil {
		return 0, err
	}

	rgba := image.NewRGBA(img.Bounds())
	if rgba.Stride != rgba.Rect.Size().X*4 {
		return 0, fmt.Errorf("unsupported stride")
	}
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	gl.TexImage2D(
		gl.TEXTURE_2D,
		0,
		gl.RGBA,
		int32(rgba.Rect.Size().X),
		int32(rgba.Rect.Size().Y),
		0,
		gl.RGBA,
		gl.UNSIGNED_BYTE,
		gl.Ptr(rgba.Pix))

	return texture, nil
}


//var UniScale gl.Int
var gVao uint32
var gTexture uint32

func main() {	
	var window *sdl.Window
	var context sdl.GLContext
	var event sdl.Event
	var running bool
	var err error
	runtime.LockOSThread()
	if err = sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		panic(err)
	}
	defer sdl.Quit()
	window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED,
		sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_OPENGL)
	if err != nil {
		panic(err)
	}
	defer window.Destroy()
	context, err = window.GLCreateContext()
	if err != nil {
		panic(err)
	}
	defer sdl.GLDeleteContext(context)

	gl.Init()
	gl.Viewport(0, 0, int32(winWidth), int32(winHeight))
	// OPENGL FLAGS
	gl.ClearColor(0.0, 0.1, 0.0, 1.0)
	gl.Enable(gl.DEPTH_TEST)
	gl.DepthFunc(gl.LESS)
	gl.Enable(gl.BLEND)
	gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
	
	// VERTEX ARRAY
	//var VertexArrayID gl.Uint
	gl.GenVertexArrays(1, &gVao)
	gl.BindVertexArray(gVao)

	//glEnableVertexAttribArray( gVertexPos2DLocation );
	
	// XXX
	//var texture uint32
	{
		// Load texture
		var err error
		gTexture, err = newTexture("tex.png")
		if err != nil {
			panic(err)
		}
	}
	//_ = texture

	// VERTEX BUFFER
	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(vertexData), gl.Ptr(vertexData), gl.STATIC_DRAW)
	
	var vboTexCoords uint32
	gl.GenBuffers(1, &vboTexCoords)
	gl.BindBuffer(gl.ARRAY_BUFFER, vboTexCoords)
	gl.BufferData(gl.ARRAY_BUFFER, 4*len(texCoordData), gl.Ptr(texCoordData), gl.STATIC_DRAW)
	
	var ibo uint32
	gl.GenBuffers(1, &ibo)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, 4*len(indexData), gl.Ptr(indexData), gl.STATIC_DRAW)
	
	
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)
	//gl.EnableVertexAttribArray(0)

	_ = `
	texCoordAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vertTexCoord\x00")))
	gl.EnableVertexAttribArray(texCoordAttrib)
	gl.VertexAttribPointerWithOffset(texCoordAttrib, 2, gl.FLOAT, false, 5*4, 3*4)
	`
	_ = `
    quadVBO[1] = glGenBuffers();
    glBindBuffer(GL_ARRAY_BUFFER, quadVBO[1]);
    glBufferData(GL_ARRAY_BUFFER, texcoords, GL_STATIC_DRAW);
    glVertexAttribPointer(shaderProgram.getLocs().get(LOC_VERTEX_TEXCOORD01.ShaderLocationInt), 2, GL_FLOAT,
            false, 0,0); //Texcoords
    `
    ///*
   	gl.BindBuffer(gl.ARRAY_BUFFER, vboTexCoords)
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 0, nil)
	//gl.EnableVertexAttribArray(1)
	//*/

	gl.EnableVertexAttribArray(0)
	gl.EnableVertexAttribArray(1)
	
	
	_ = `
	//Set vertex data
	glBindBuffer( GL_ARRAY_BUFFER, gVBO );
	glVertexAttribPointer( gVertexPos2DLocation, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), NULL );
	`
	//gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	//gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 0, nil)
	
	_ = `
	//Set index data and render
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, gIBO );
	glDrawElements( GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, NULL );
	`
	//gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ibo)
	

	//UNIFORM HOOK
	//unistring := gl.GLString("scaleMove")
	//UniScale = gl.GetUniformLocation(program, unistring)
	//fmt.Printf("Uniform Link: %v\n", UniScale+1)

	// GUESS WHAT
	program := createprogram()

	gl.UseProgram(program)

	running = true
	for running {
		for event = sdl.PollEvent(); event != nil; event =
			sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.MouseMotionEvent:
				fmt.Printf("[%dms]MouseMotion\tid:%d\tx:%d\ty:%d\txrel:%d\tyrel:%d\n", t.Timestamp, t.Which, t.X, t.Y, t.XRel, t.YRel)
			}
		}
		drawgl()
		window.GLSwap()
	}
}

func drawgl() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	
	//gl.DrawArrays(gl.TRIANGLES, gl.Int(0), gl.Sizei(len(vertexData)*4))

	_ = `
	glDrawElements( GL_TRIANGLE_FAN, 4, GL_UNSIGNED_INT, NULL );
	`
	//gl.DrawElements(gl.TRIANGLE_FAN, gl.Sizei(4), gl.UNSIGNED_INT, nil)

	gl.BindVertexArray(gVao)
	
	
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, gTexture)
	
	gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, nil)
	//gl.DrawArrays(gl.TRIANGLES, 0, int32(len(vertexData)/3))
	
	
	gl.BindVertexArray(0)


	time.Sleep(50 * time.Millisecond)

}

const (
	winTitle           = "OpenGL Shader"
	winWidth           = 512
	winHeight          = 512
	vertexShaderSource = `
#version 140
in vec2 Position;

in vec2 vTexCoord;

out vec2 fTexCoord;

void main()
{
	fTexCoord = vTexCoord;
	
	gl_Position = vec4( Position.x, Position.y, 0, 1 );
}
`
	fragmentShaderSource = `
#version 140

uniform sampler2D tex;

in vec2 fTexCoord;

out vec4 outColor;

void main()
{
	//outColor = vec4( 0.0, 1.0, 0.0, 1.0 );
	
	outColor = texture(tex, fTexCoord);
}
`
)

var vertexData = []float32{
						/*
						-0.5, -0.5,
						 0.5, -0.5,
						 0.5,  0.5,
						-0.5,  0.5,
						*/
					
						/*	
						// tr
						0.5, 0.5,
						// br
						0.5, -0.5,
						// bl
						-0.5, -0.5,
						// tl
						-0.5, 0.5,
						*/
						
						///*
						// tr
						0.5, 0.5, 0,
						// br
						0.5, -0.5, 0,
						// bl
						-0.5, -0.5, 0,
						// tl
						-0.5, 0.5,	0,
						//*/
						
						/*
		    			-0.5,  0.5, 0.0,
		                -0.5, -0.5, 0.0,
        		        0.5,  0.5, 0.0,
        		        0.5, -0.5, 0.0,
        		        */
}

// info
// https://arm-software.github.io/opengl-es-sdk-for-android/etc_texture.html
var texCoordData = []float32{
						/*			
						// tr
						1, -1,
						// br
						1, 0,
						// bl
						0, 0,
						// tl
						0, -1,
						*/
						
						///*			
						// tr
						1, 0,
						// br
						1, 1,
						// bl
						0, 1,
						// tl
						0, 0,
						//*/
						
						/*
						// tr
						0.5, -0.5,
						// br
						0.5, 0,
						// bl
						0, 0,
						// tl
						0, -0.5,
						*/
		
						/*				
						0.5, -0.5,
		               -0.5, -0.5,
 		                0.5,  0.5,
		                0.5, -0.5,
		                */
		                
		                /*
   						1.0, -1.0,
		               -1.0, -1.0,
 		                1.0,  1.0,
		                1.0, -1.0,
		                */
}

var indexData = []uint32{
					/*0, 1, 2, 3*/
					
					0, 1, 3,
					1, 2, 3,
					
					//0, 1, 2, 
					//0, 2, 3,
}

