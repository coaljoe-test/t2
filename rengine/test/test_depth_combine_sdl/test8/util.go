package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"github.com/go-gl/gl/v3.3-core/gl"
	"os"
	
	"github.com/disintegration/imaging"
)

// Get raw texture data
func GetTextureData(texture uint32, width, height int) interface{} {
	fmt.Println("GetTexureData texture:", texture, "width:", width, "height:", height)

	iw, ih := width, height

	var ret interface{}
	
	glcheck()

	//gl.Disable(gl.BLEND)
	//t.Bind()
	//gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	//switch t.format {
	/*
	case TextureFormatRGBA:
		fmt.Println("texture is rgba...")
		var raw_img []uint8 = make([]uint8, iw*ih*4)
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		ret = raw_img
	*/
	
	//case TextureFormatFloat32:
	/*
		fmt.Println("texture is float32...")
		var raw_img []float32 = make([]float32, iw*ih)
		//gl.GetTexImage(gl.TEXTURE_2D, 0, gl.FLOAT, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RED, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		glcheck()
		ret = raw_img
	*/
	///*
	//case TextureFormatDepth:
		fmt.Println("texture is depth...")
		var raw_img []uint32 = make([]uint32, iw*ih)
		//gl.GetTexImage(gl.TEXTURE_2D, 0, gl.FLOAT, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_INT, gl.Ptr(raw_img))
		ret = raw_img
	
	// default:
	// 	panic("unknown format")
	// */
	//}
	//t.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)
	//gl.Enable(gl.BLEND)
	
	glcheck()

	//pdump(t)
	//pp(2)
	
	return ret
}

// XXX Fetch depth texture data and as Image (depth16conv version)
// XXX fixme ?
func GetTextureDataDepth16ConvAsImage(texture uint32, width, height int, flipY bool) image.Image {
	fmt.Println("GetTextureDataDepth16ConvAsImage")
	//Log.Dbg("%F flipY:", flipY)

	//if t.format != TextureFormatDepth {
	//	panic("texture format is not depth")
	//}

	raw_img := GetTextureData(texture, width, height).([]uint32)
	glcheck()
	
	iw, ih := width, height
	
	im := image.NewGray16(image.Rect(0, 0, iw, ih))	
	//im.Pix = raw_img
	
	for y := 0; y < ih; y++ {
		for x := 0; x < iw; x++ {
			v := raw_img[y*iw + x]
			// Half-range 16-bit value
			var v16 uint16
			// Map range
			// to.start + (self - from.start) * (to.end - to.start) / (from.end - from.start)
			v2 := uint64(v) // XXX
			v16 = uint16(0 + (v2 - 0) * (0xffff - 0) / (0xffffffff - 0))
			//fmt.Println("v:", v)
			//fmt.Println("v16:", v16)
			//fmt.Println("z:", z)
			// XXX same as ?
			//  Lerp(0.0, float64(0xffff), float64(v)/float64(0xffffffff))
			// not tested
			//pp(v)
			c := color.Gray16{v16}
			im.SetGray16(x, y, c)
		}
	}
	
	if flipY {
		im = flipYImageGray16(im).(*image.Gray16)
	}
	
	return im
}




// Special method to save converted depth textures to
// 16-bit png files, fixme ?
func SaveDepth16Conv(texture uint32, width, height int, path string) {
	fmt.Println("SaveDepth16Conv path:", path)

	//if t.format != TextureFormatDepth {
	//	panic("texture format is not depht")
	//}

	//Log.Inf("Warning: experimental")
	//Log.Inf("Saving depth texture converted to 16 bit depth: ", path)
	
	im := GetTextureDataDepth16ConvAsImage(texture, width, height, false)
	
	//saveImage(im, path, false)
	saveImage(im, path, true)

	//pp(2)
}

// TODO: add jpeg support?
func saveImage(im image.Image, path string, flipY bool) {
	//Log.Dbg("%F im:", im, "path:", path, "flipY:", flipY)

	f, err := os.Create(path)
	if err != nil {
		fmt.Println("can't save image to path; path: ", path)
		panic(err)
	}
	defer f.Close()
	
	if flipY {
		if im.ColorModel() == color.Gray16Model {
			im = flipYImageGray16(im)
		} else {
			im = flipYImage(im)
		}
	}

	// disable compression
	//enc := &png.Encoder{CompressionLevel: -1}
	// Use fast compression
	enc := &png.Encoder{CompressionLevel: -2}
	enc.Encode(f, im)
}

//// Utils

// Flip rgba image
func flipYImage(im image.Image) image.Image {

	// Checks
	if im.ColorModel() == color.Gray16Model {
		panic("bad image color model")
	}
	
	im2 := imaging.FlipV(im)

	return im2
}

// Flip gray16 image
func flipYImageGray16(im image.Image) image.Image {

	// Checks
	if im.ColorModel() != color.Gray16Model {
		panic("bad image color model")
	}

	//im = color.Gray16Model.Convert(im)
	b := im.Bounds()
	//im2 := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	im2 := image.NewGray16(image.Rect(0, 0, b.Dx(), b.Dy()))
	for y := 0; y < b.Dy(); y++ {
		for x := 0; x < b.Dx(); x++ {
			c := im.(*image.Gray16).Gray16At(x, y)
			im2.SetGray16(x, b.Dy()-1-y, c)
		}
	}
	//draw.Draw(im2, im2.Bounds(), im, b.Min, draw.Src)

	// Debug
	if false {
		f, err := os.Create("/tmp/x_16.png")
		if err != nil {
			panic(err)
		}
		defer f.Close()
		png.Encode(f, im2)
		//png.Encode(f, im)
	}

	return im2
}

// XXX
func glcheck() {
	//return

	
	if err := gl.GetError(); err != gl.NO_ERROR {
		switch err {
		case gl.INVALID_ENUM:
			fmt.Println("OpenGL Error: GL_INVALID_ENUM")
		case gl.INVALID_VALUE:
			fmt.Println("OpenGL Error: GL_INVALID_VALUE")
		case gl.INVALID_OPERATION:
			fmt.Println("OpenGL Error: GL_INVALID_OPERATION")
		case gl.STACK_OVERFLOW:
			fmt.Println("OpenGL Error: GL_STACK_OVERFLOW")
		case gl.STACK_UNDERFLOW:
			fmt.Println("OpenGL Error: GL_STACK_UNDERFLOW")
		case gl.OUT_OF_MEMORY:
			fmt.Println("OpenGL Error: GL_OUT_OF_MEMORY")
		}
	
		panic(err)
	}
}
