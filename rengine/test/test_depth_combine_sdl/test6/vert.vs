#version 140

// Uniform ?
uniform vec2 shift;

in vec2 Position;

in vec2 vTexCoord;


out vec2 fTexCoord;


void main()
{
	fTexCoord = vTexCoord;
	
	//gl_Position = vec4( Position.x - .5, Position.y, 0, 1 );
	gl_Position = vec4( Position.x + shift.x, Position.y + shift.y, 0, 1 );
}


