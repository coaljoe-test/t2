module opengl3

go 1.21.0

require (
	github.com/chsc/gogl v0.0.0-20131111203533-c411acc846b6
	github.com/veandco/go-sdl2 v0.4.35
)
