// author: Jacky Boen

package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"os"
	"time"
	"math/rand"
)

var winTitle string = "Go-SDL2 Texture"
var winWidth, winHeight int32 = 1024, 1024
//var imageName string = "../../assets/test.png"
//var imageName string = "test.png"
var imageName string = "out1.png"

func main() {
	fmt.Println("main()")

	var window *sdl.Window
	var renderer *sdl.Renderer
	var texture *sdl.Texture
	var src, dst sdl.Rect
	var err error

	window, err = sdl.CreateWindow(winTitle, sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		winWidth, winHeight, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create window: %s\n", err)
		return
	}
	defer window.Destroy()

	renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create renderer: %s\n", err)
		return
	}
	defer renderer.Destroy()

	image, err := img.Load(imageName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		return
	}
	defer image.Free()

	texture, err = renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		return
	}
	defer texture.Destroy()

	src = sdl.Rect{0, 0, 512, 512}
	dst = sdl.Rect{100, 50, 512, 512}

	if false {
		t := time.Now()
		renderer.Clear()
		renderer.SetDrawColor(255, 0, 0, 255)
		renderer.FillRect(&sdl.Rect{0, 0, int32(winWidth), int32(winHeight)})
		renderer.Copy(texture, &src, &dst)
		renderer.Present()

		fmt.Println("d:", time.Now().Sub(t))
	}

	if true {
		n := 100
		fmt.Printf("n=%d\n", n)

		// XXX need to clear ?
		renderer.Clear()

		t := time.Now()
		fmt.Println("t start:", t)
		
		for i := 0; i < n; i++ {
			//renderer.Copy(texture, &src, &dst)

			xsrc := sdl.Rect{
				//int32(rand.Intn(100)),
				//int32(rand.Intn(100)),
				0, 0,
				256, 256}
			xdst := sdl.Rect{
				int32(rand.Intn(512)),
				int32(rand.Intn(512)),
				256, 256}
	
			renderer.Copy(texture, &xsrc, &xdst)
		}

		renderer.Present()

		fmt.Println("t end:", time.Now())
		fmt.Println("d1:", time.Now().Sub(t))
	}

	if true {
	for {
		sdl.PollEvent()
		sdl.Delay(2000)
	}
	}
}
