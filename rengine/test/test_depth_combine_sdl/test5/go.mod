module test5

go 1.21.0

require (
	github.com/disintegration/imaging v1.6.2
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6
	github.com/veandco/go-sdl2 v0.4.35
)

require golang.org/x/image v0.0.0-20191009234506-e7c1f5e7dbb8 // indirect
