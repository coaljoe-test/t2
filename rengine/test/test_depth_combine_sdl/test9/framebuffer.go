package main

import (
	"fmt"

	//"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/gl/v3.3-core/gl"

)

type Framebuffer struct {
	width     int
	height    int
	modeDepth bool     // is a depth frame buffer
	onlyColor bool     // always false
	//tex       *Texture // gl texture
	tex       uint32 // gl texture

	// gl
	fbo       uint32
	rbo_depth uint32 // depth render buffer object
}

//func (f *Framebuffer) Tex() *Texture { return f.tex }

func NewFramebuffer(width, height int, modeDepth bool) *Framebuffer {
	f := &Framebuffer{
		//tex:       nil,
		width:     width,
		height:    height,
		modeDepth: modeDepth,
		onlyColor: false,
	}
	f.create()
	return f
}

func NewEmptyTexture(width, height int, depth bool) uint32 {
	fmt.Println("NewEmptyTexture width:", width, "height:", height, "depth:", depth)

	var texture uint32
	gl.GenTextures(1, &texture)
	gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	gl.PixelStorei(gl.UNPACK_ALIGNMENT, 1)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	if !depth {
		gl.TexImage2D(
			gl.TEXTURE_2D,
			0,
			gl.RGBA8,
			int32(width),
			int32(height),
			0,
			gl.RGBA,
			gl.UNSIGNED_BYTE,
			nil)
	} else {
		gl.TexImage2D(
			gl.TEXTURE_2D,
			0,
			gl.DEPTH_COMPONENT24,
			int32(width),
			int32(height),
			0,
			gl.DEPTH_COMPONENT,
			gl.FLOAT,
			nil)
	}

	gl.BindTexture(gl.TEXTURE_2D, 0)

	return texture
}

func (f *Framebuffer) create() {

	//f.tex = CreateTexture(f.width, f.height, f.modeDepth)
	//f.tex = NewEmptyTexture(f.width, f.height, TextureFormatFloat32)
	if f.modeDepth {
		//f.tex = NewEmptyTexture(f.width, f.height, TextureFormatDepth)
		f.tex = NewEmptyTexture(f.width, f.height, true)
	} else {
		//f.tex = NewEmptyTexture(f.width, f.height, TextureFormatRGBA)
		f.tex = NewEmptyTexture(f.width, f.height, false)
	}
	glcheck()
	
	// XXX Bind texture
	//f.tex.Bind()
	gl.BindTexture(gl.TEXTURE_2D, f.tex)

	// tune the texture
	// XXX fixme: use t.tex object ?
	if f.modeDepth {
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
	} else {
		// need for fxaa
		//gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR)
		gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR)
	}

	// XXX fixme ?
	//f.tex.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)

	// create FBO
	gl.GenFramebuffers(1, &f.fbo)
	//rt.fbo.Bind()
	gl.BindFramebuffer(gl.FRAMEBUFFER, f.fbo)
	glcheck()

	if f.onlyColor {
		gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
			gl.TEXTURE_2D, f.tex, 0)
		glcheck()
	} else {
		// depth buffer
		gl.GenRenderbuffers(1, &f.rbo_depth)
		//rbo_depth.Bind()
		gl.BindRenderbuffer(gl.RENDERBUFFER, f.rbo_depth)
		glcheck()
		gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT24,
			int32(f.width), int32(f.height))
		//gl.RenderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT32,
		//	int32(f.width), int32(f.height))
		glcheck()
		gl.FramebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, f.rbo_depth)
		glcheck()

		// attach the texture
		if !f.modeDepth {
			gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0,
				gl.TEXTURE_2D, f.tex, 0)
		} else {
			gl.DrawBuffer(gl.NONE)
			gl.ReadBuffer(gl.NONE)
			gl.FramebufferTexture2D(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT,
				gl.TEXTURE_2D, f.tex, 0)
		}
	}
	glcheck()

	// check status
	status := gl.CheckFramebufferStatus(gl.FRAMEBUFFER)
	if status != gl.FRAMEBUFFER_COMPLETE {
		panic("Bad FBO, status: " + fmt.Sprintf("%d", status))
	}
	glcheck()

	// unbind
	//rt.fbo.Unbind()
	//rbo_depth.Unbind()
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
	glcheck()
}

func (f *Framebuffer) Bind() {
	gl.BindFramebuffer(gl.FRAMEBUFFER, f.fbo)
}

func (f *Framebuffer) Unbind() {
	gl.BindFramebuffer(gl.FRAMEBUFFER, 0)
}
