module opengl

go 1.21.0

require (
	github.com/go-gl/gl v0.0.0-20211210172815-726fda9656d6
	github.com/veandco/go-sdl2 v0.4.35
)
