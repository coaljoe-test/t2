package main

import (
	"fmt"
	"image"
	"image/color"
//	"image/png"
	"github.com/go-gl/gl/v3.3-core/gl"
//	"os"
)

// Get raw texture data
func GetTextureData(texture uint32, width, height int) interface{} {
	fmt.Println("GetTexureData texture:", texture, "width:", width, "height:", height)

	iw, ih := width, height

	var ret interface{}
	
	glcheck()

	//gl.Disable(gl.BLEND)
	//t.Bind()
	//gl.ActiveTexture(gl.TEXTURE0)
	gl.BindTexture(gl.TEXTURE_2D, texture)
	//switch t.format {
	/*
	case TextureFormatRGBA:
		fmt.Println("texture is rgba...")
		var raw_img []uint8 = make([]uint8, iw*ih*4)
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		ret = raw_img
	*/
	
	//case TextureFormatFloat32:
	/*
		fmt.Println("texture is float32...")
		var raw_img []float32 = make([]float32, iw*ih)
		//gl.GetTexImage(gl.TEXTURE_2D, 0, gl.FLOAT, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.RED, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		glcheck()
		ret = raw_img
	*/
	///*
	//case TextureFormatDepth:
		fmt.Println("texture is depth...")
		var raw_img []uint32 = make([]uint32, iw*ih)
		//gl.GetTexImage(gl.TEXTURE_2D, 0, gl.FLOAT, gl.UNSIGNED_BYTE, gl.Ptr(raw_img))
		gl.GetTexImage(gl.TEXTURE_2D, 0, gl.DEPTH_COMPONENT, gl.UNSIGNED_INT, gl.Ptr(raw_img))
		ret = raw_img
	
	// default:
	// 	panic("unknown format")
	// */
	//}
	//t.Unbind()
	gl.BindTexture(gl.TEXTURE_2D, 0)
	//gl.Enable(gl.BLEND)
	
	glcheck()

	//pdump(t)
	//pp(2)
	
	return ret
}

// XXX Fetch depth texture data and as Image (depth16conv version)
// XXX fixme ?
func GetTextureDataDepth16ConvAsImage(texture uint32, width, height int, flipY bool) image.Image {
	fmt.Println("GetTextureDataDepth16ConvAsImage")
	//Log.Dbg("%F flipY:", flipY)

	//if t.format != TextureFormatDepth {
	//	panic("texture format is not depth")
	//}

	raw_img := GetTextureData(texture, width, height).([]uint32)
	glcheck()
	
	iw, ih := width, height
	
	im := image.NewGray16(image.Rect(0, 0, iw, ih))	
	//im.Pix = raw_img
	
	for y := 0; y < ih; y++ {
		for x := 0; x < iw; x++ {
			v := raw_img[y*iw + x]
			// Half-range 16-bit value
			var v16 uint16
			// Map range
			// to.start + (self - from.start) * (to.end - to.start) / (from.end - from.start)
			v2 := uint64(v) // XXX
			v16 = uint16(0 + (v2 - 0) * (0xffff - 0) / (0xffffffff - 0))
			//fmt.Println("v:", v)
			//fmt.Println("v16:", v16)
			//fmt.Println("z:", z)
			// XXX same as ?
			//  Lerp(0.0, float64(0xffff), float64(v)/float64(0xffffffff))
			// not tested
			//pp(v)
			c := color.Gray16{v16}
			im.SetGray16(x, y, c)
		}
	}
	
	if flipY {
		im = flipYImageGray16(im).(*image.Gray16)
	}
	
	return im
}




// Special method to save converted depth textures to
// 16-bit png files, fixme ?
func SaveDepth16Conv(texture uint32, width, height int, path string) {
	fmt.Println("SaveDepth16Conv path:", path)

	//if t.format != TextureFormatDepth {
	//	panic("texture format is not depht")
	//}

	//Log.Inf("Warning: experimental")
	//Log.Inf("Saving depth texture converted to 16 bit depth: ", path)
	
	im := GetTextureDataDepth16ConvAsImage(texture, width, height, false)
	
	//saveImage(im, path, false)
	saveImage(im, path, true)

	//pp(2)
}
