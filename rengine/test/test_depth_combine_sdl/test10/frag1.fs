#version 140

uniform sampler2D tex;

in vec2 fTexCoord;

out vec4 outColor;
//out float outDepth;

void main()
{
	//outColor = vec4( 0.0, 1.0, 0.0, 1.0 );
	
	outColor = texture(tex, fTexCoord);
	
	// XXX ?
	outColor.a = 1.0;
}
