package main

import (
	"fmt"
	"github.com/go-gl/gl/v3.3-core/gl"
)

// XXX
func glcheck() {
	//return

	
	if err := gl.GetError(); err != gl.NO_ERROR {
		switch err {
		case gl.INVALID_ENUM:
			fmt.Println("OpenGL Error: GL_INVALID_ENUM")
		case gl.INVALID_VALUE:
			fmt.Println("OpenGL Error: GL_INVALID_VALUE")
		case gl.INVALID_OPERATION:
			fmt.Println("OpenGL Error: GL_INVALID_OPERATION")
		case gl.STACK_OVERFLOW:
			fmt.Println("OpenGL Error: GL_STACK_OVERFLOW")
		case gl.STACK_UNDERFLOW:
			fmt.Println("OpenGL Error: GL_STACK_UNDERFLOW")
		case gl.OUT_OF_MEMORY:
			fmt.Println("OpenGL Error: GL_OUT_OF_MEMORY")
		}
	
		panic(err)
	}
}
