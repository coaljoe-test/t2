package rengine

import (
	"fmt"
//	"os"
	"image"
//_	"image/png"
	"image/color"
	"math"
)

// ? Gray 8 ?
// in-place ?
func MulImageDataGray(im image.Image, v float64) {
	fmt.Println("MulImageDataGray v:", v)

	cm := im.ColorModel()
	fmt.Println("cm: ", cm)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	imGray := im.(*image.Gray)

	b := im.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c := imGray.GrayAt(x, y)

			//fmt.Println("c:", c)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			 
			oldV := c.Y
			oldFloatV := float64(oldV)/255.0
			// clamp ?
			newFloatV := math.Min(oldFloatV * v, 1.0)
			newV := int(newFloatV*255.0)
			_ = newV

			//fmt.Println("oldFloatV:", oldFloatV)
			//fmt.Println("newFloatV:", newFloatV)
			//fmt.Println("newV:", newV)

			// clamp ?
			newV2 := int(math.Min(float64(oldV) * v, 255.0))
			//fmt.Println("newV2:", newV2)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV2)}
				imGray.SetGray(x, y, newC)
			}
		}
	}
}

// ? Gray 8 ?
// Combine depths ?
func MergeDepthImagesGray(inIm1, inIm2 image.Image) image.Image {
	fmt.Println("DepthMergeImageGray")

	cm1 := inIm1.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	im1 := inIm1.(*image.Gray)
	im2 := inIm2.(*image.Gray)

	out := image.NewGray(im1.Bounds())

	b := im1.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := im1.GrayAt(x, y)
			c2 := im2.GrayAt(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			if c2.Y > c1.Y {
				newV = c2.Y
			}

			
			
			// clamp ?
			//fmt.Println("newV2:", newV)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV)}
				out.SetGray(x, y, newC)
			}
		}
	}

	return out
}

var _ = `
// ? Gray 8 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
func SWDepthMergeImages(bgDepth, bgColor, objDepth, objColor image.Image) image.Image {
	fmt.Println("SWDepthMergeImages")

	cm1 := inIm1.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	im1 := inIm1.(*image.Gray)
	im2 := inIm2.(*image.Gray)

	out := image.NewGray(im1.Bounds())

	b := im1.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := im1.GrayAt(x, y)
			c2 := im2.GrayAt(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			if c2.Y > c1.Y {
				newV = c2.Y
			}

			
			
			// clamp ?
			//fmt.Println("newV2:", newV)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV)}
				out.SetGray(x, y, newC)
			}
		}
	}

	return out
}
`
