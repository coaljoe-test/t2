package main

import (
	"fmt"
	"os"

	"github.com/diamondburned/gotk4/pkg/gtk/v4"
	"github.com/diamondburned/gotk4/pkg/gio/v2"
)

func main() {
	fmt.Println("main()")

	initCtx()

	sv := NewSceneViewer()
	_ = sv

	// XXX
	sv.LoadTiles("test_scene/tiles")

	svv := NewSceneViewerView(sv)
	_ = svv
	// ?
	sv.View = svv

	// XXX ?
	//sv.Build()

	//svv.Build()

	sv.Spawn()

	app := gtk.NewApplication("com.github.diamondburned.gotk4-examples.gtk4.simple", gio.ApplicationFlagsNone)
	app.ConnectActivate(func() { activate(app) })

	if code := app.Run(os.Args); code > 0 {
		os.Exit(code)
	}

	fmt.Println("exiting...")
}

func activate(app *gtk.Application) {
	win := gtk.NewApplicationWindow(app)
	win.SetTitle("gotk4 Example")
	//win.SetChild(gtk.NewLabel("Hello from Go!"))
	//win.SetDefaultSize(400, 300)
	//win.SetDefaultSize(800, 600)
	win.SetDefaultSize(1024, 800)
	win.Show()

	// XXX ?
	view := ctx.SceneViewer.View
	view.Win = win
	//view.AppWin = win

	// XXX ?
	view.MakeGui()
	view.Spawn()
}
