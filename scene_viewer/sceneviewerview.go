package main

import (
	"fmt"

	"github.com/diamondburned/gotk4/pkg/gtk/v4"
)

type SceneViewerView struct {
	// link
	m *SceneViewer

	// ?
	//Win *gtk.Window
	Win *gtk.ApplicationWindow
	
	// ?
	ScrolledArea *gtk.ScrolledWindow

	// ?
	Grid *gtk.Grid
	
	
	// ?
	box1  *gtk.Box
	box2  *gtk.Box
	box3  *gtk.Box
	button1 *gtk.Button
	button2 *gtk.Button
	button3 *gtk.Button

	

	Pictures [][]*gtk.Picture

	// ?
	Dirty bool
}

func NewSceneViewerView(m *SceneViewer) *SceneViewerView {

	fmt.Println("NewSceneViwerView")

	v := &SceneViewerView{
		m: m,
	}
	
	// XXX
	v.initPictures()

	return v
}

func (v *SceneViewerView) initPictures() {

	fmt.Println("_ initPictures")

	w := v.m.ResTilesX
	h := v.m.ResTilesY
	
	tileW := v.m.TileSize
	tileH := v.m.TileSize
	_ = tileW
	_ = tileH

	//// Init ?
	v.Pictures = make([][]*gtk.Picture, w)
	
	for i := range v.Pictures {
		v.Pictures[i] = make([]*gtk.Picture, h)	
	}
	
	//dump(mr.Tiles)
	
	//panic(2)
	
	//dump(mr.Tiles)
}

func (v *SceneViewerView) makePicture(path string) *gtk.Picture {

	fmt.Println("_ makePicture path:", path)

	//x2 := gtk.NewPicture()
	//x2 := gtk.NewPictureForFilename("test_scene/tiles/tile1.png")
	x2 := gtk.NewPictureForFilename(path)
	x2.SetAlternativeText("tile")
	//x2.SetCanShrink(false)
	x2.SetSizeRequest(512, 512)
	//x2.SetVExpand(false)
	//x2.SetHExpand(false)
	//x2.SetVAlign(gtk.AlignBaseline)
	//x2.SetHAlign(gtk.AlignBaseline)
	x2.SetVAlign(gtk.AlignStart)
	x2.SetHAlign(gtk.AlignStart)
	//x2.SetVAlign(gtk.AlignCenter)
	//x2.SetHAlign(gtk.AlignCenter)

	return x2
}

// Make/load pictures
func (v *SceneViewerView) makePictures() {

	fmt.Println("_ makePictures")

	//w := 8
	//h := 8
	w := v.m.ResTilesX
	h := v.m.ResTilesY

	//format := "png"
	format := "avif"
	fmt.Println("format:", format)

	path := "test_scene/tiles1"
	//path := "test_scene/tiles2"
	fmt.Println("path:", path)
	
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			fmt.Println("->")
			xpath := path + "/" + fmt.Sprintf("tile_%04d,%04d.%s", x, y, format)
			fmt.Println("xpath:", xpath)
			
			pic := v.makePicture(xpath)
			
			// XXX
			v.Pictures[x][y] = pic
		}
	}
	
	fmt.Println("done makePictures")
}

func (v *SceneViewerView) makePictureGrid() {

	fmt.Println("_ makePictureGrid")
	
	v.Grid = gtk.NewGrid()
	v.Grid.SetRowHomogeneous(true)
	v.Grid.SetRowSpacing(3)
	v.Grid.SetColumnHomogeneous(true)
	v.Grid.SetColumnSpacing(3)
	
	// XXX
	//win.SetChild(v.Grid)
	v.ScrolledArea.SetChild(v.Grid)
	
	
	/*
	pic := v.Pictures[0][0]
	
	v.Grid.Attach(pic, 0, 0, 1, 1)
	*/
	
	w := v.m.ResTilesX
	h := v.m.ResTilesY
	
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Println("->")
			
			pic := v.Pictures[x][y]
			
			// XXX
			overlay := gtk.NewOverlay()
			overlay.SetChild(pic)
			
			//s := "test_label"
			s := ""
			s += fmt.Sprintf("\n<span font=\"35\" color=\"#FFFF00\">(%d,%d)</span>", x, y)
			
			label := gtk.NewLabel("")
			//label.SetText(s)
			label.SetMarkup(s)
			
			overlay.AddOverlay(label)
			
			v.Grid.Attach(overlay, x, y, 1, 1)
			
			//v.Grid.Attach(pic, x, y, 1, 1)
		}
	}
}

func (v *SceneViewerView) MakeGui() {

	fmt.Println("_ MakeGui")

	//sv.Win.SetChild(gtk.NewLabel("Hello from Go!"))
	//sv.AppWin.SetChild(gtk.NewLabel("Hello from Go!"))
	
	//v.box1 = gtk.NewBox(gtk.OrientationHorizontal, 5)
	v.box1 = gtk.NewBox(gtk.OrientationVertical, 5)
	v.Win.SetChild(v.box1)
	
	// XXX
	//v.box2 = gtk.NewBox(gtk.OrientationHorizontal, 5)
	v.box2 = gtk.NewBox(gtk.OrientationVertical, 5)
	v.box1.Append(v.box2)
	
	v.box3 = gtk.NewBox(gtk.OrientationHorizontal, 5)
	//v.box3 = gtk.NewBox(gtk.OrientationVertical, 5)
	v.box2.Append(v.box3)
	
	v.button1 = gtk.NewButton()
	v.button1.SetLabel("Button 1")
	v.button1.SetHExpand(false)
	v.button1.SetVExpand(false)
	//v.box2.Append(v.button1)
	v.box3.Append(v.button1)
	
	v.button2 = gtk.NewButton()
	v.button2.SetLabel("Toggle tiles")
	v.button2.SetHExpand(false)
	v.button2.SetVExpand(false)
	v.box3.Append(v.button2)

	// XXX
	v.button2.ConnectClicked(func() {
		v.m.ToggleDrawTiles()
	})
	
	v.button3 = gtk.NewButton()
	v.button3.SetLabel("Toggle tile numbers")
	v.button3.SetHExpand(false)
	v.button3.SetVExpand(false)
	v.box3.Append(v.button3)
	
	// XXX
	v.button3.ConnectClicked(func() {
		v.m.ToggleDrawTileNumbers()
	})
	
	if true {
		//x := gtk.NewLabel("Hello from Go!")
		x := gtk.NewScrolledWindow()
		//x.SetHasFrame(true)
		x.SetPolicy(gtk.PolicyAlways, gtk.PolicyAlways)
		x.SetHExpand(true)
		x.SetVExpand(true)		
			
		//v.Win.SetChild(x)
		v.box1.Append(x)
		
		// XXX
		v.ScrolledArea = x

		//x.SetChild(gtk.NewLabel("Hello from Go!"))

		fmt.Println("load file...")

		//x2 := gtk.NewPicture()
		//x2 := gtk.NewPictureForFilename("test_scene/tiles/tile1.png")
		x2 := gtk.NewPictureForFilename("test_scene/tiles/tile1.avif")
		//x2 := gtk.NewPictureForFilename("test_scene/tiles/tile1.jp2")

		fmt.Println("done load file...")

		//path := "test_scene/tiles1/tile_0000,0000.avif"
		//x2 := gtk.NewPictureForFilename(path)
		x2.SetAlternativeText("tile")
		//x2.SetCanShrink(false)
		x2.SetSizeRequest(512, 512)
		//x2.SetVExpand(false)
		//x2.SetHExpand(false)
		//x2.SetVAlign(gtk.AlignBaseline)
		//x2.SetHAlign(gtk.AlignBaseline)
		x2.SetVAlign(gtk.AlignStart)
		x2.SetHAlign(gtk.AlignStart)
		//x2.SetVAlign(gtk.AlignCenter)
		//x2.SetHAlign(gtk.AlignCenter)

		x.SetChild(x2)

		//panic(2)
	}
	
	if true {
	//if false {
		v.makePictures()
		
		// XXX
		v.makePictureGrid()
	}

	// XXX ?
	v.Build()
}

// Update tile numbers
func (v *SceneViewerView) setShowTileNumbers(xv bool) {

	fmt.Println("_ setShowTileNumbers xv:", xv)

	w := v.m.ResTilesX
	h := v.m.ResTilesY
	
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			overlay := v.Grid.ChildAt(x, y).(*gtk.Overlay)
			
			ch := overlay.Child()
			_ = ch
			
			//fmt.Println("ch:", ch)
			//fmt.Printf("T ch: %T\n", ch)

			ch1 := overlay.LastChild().(*gtk.Label)

			//fmt.Println("ch1:", ch1)
			//fmt.Printf("T ch1: %T\n", ch1)
			
			//panic(2)
		
			if xv == true {
				ch1.SetVisible(true)
			} else {
				ch1.SetVisible(false)
			}
		}
	}
}

func (v *SceneViewerView) Build() {

	fmt.Println("_ Build")
	
	if v.m.DrawTiles {
		v.Grid.SetRowSpacing(3)
		v.Grid.SetColumnSpacing(3)
	} else {
		v.Grid.SetRowSpacing(0)
		v.Grid.SetColumnSpacing(0)
	}
	
	// XXX
	v.setShowTileNumbers(v.m.DrawTileNumbers)
}

func (v *SceneViewerView) Spawn() {
}
