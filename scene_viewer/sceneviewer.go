package main

import (
	"fmt"
	"image"
)

type SceneViewer struct {
	TileSize int
	// Resolution in tiles ?
	ResTilesX int
	ResTilesY int

	Tiles [][]*image.NRGBA
	// paths ?
	//Tiles [][]string
	
	// Option ?
	DrawTiles bool
	// Option ?
	DrawTileNumbers bool

	// ?
	View *SceneViewerView
}

func NewSceneViewer() *SceneViewer {
	sv := &SceneViewer{
		TileSize: 512,
		//ResTilesX: 4,
		//ResTilesY: 4,
		//ResTilesX: 5,
		//ResTilesY: 5,
		ResTilesX: 8,
		ResTilesY: 8,
		DrawTiles: true,
		DrawTileNumbers: false,
	}

	// XXX
	sv.initTiles()

	ctx.SceneViewer = sv

	return sv
}

func (sv *SceneViewer) initTiles() {

	fmt.Println("_ initTiles")

	w := sv.ResTilesX
	h := sv.ResTilesY
	
	//tileW := 512
	//tileH := 512
	
	tileW := sv.TileSize
	tileH := sv.TileSize

	//// Init ?
	sv.Tiles = make([][]*image.NRGBA, w)
	
	for i := range sv.Tiles {
		sv.Tiles[i] = make([]*image.NRGBA, h)	
	}
	
	//dump(mr.Tiles)
	
	//panic(2)
	
	////

	for y := 0; y < w; y++ {
		for x := 0; x < h; x++ {
			fmt.Println("-> x:", x, "y:", y)
			
			rect := image.Rect(0, 0, tileW, tileH)
			im := image.NewNRGBA(rect)
			sv.Tiles[x][y] = im
		}
	}

	//dump(mr.Tiles)
}

func (sv *SceneViewer) LoadTiles(path string) {
	// XXX
	xpath := path + "/tile1.png"
	xtile := LoadPng(xpath)
	_ = xtile

}

func (sv *SceneViewer) ToggleDrawTiles() {

	fmt.Println("_ ToggleDrawTiles")

	sv.DrawTiles = !sv.DrawTiles
	
	// XXX
	sv.View.Build()
}

func (sv *SceneViewer) ToggleDrawTileNumbers() {

	fmt.Println("_ ToggleDrawTileNumbers")

	sv.DrawTileNumbers = !sv.DrawTileNumbers
	
	// XXX
	sv.View.Build()
}

func (sv *SceneViewer) Build() {
}

// Should this init view ?
func (sv *SceneViewer) Spawn() {

	if sv.View != nil {
		sv.View.Spawn()
	}
}
