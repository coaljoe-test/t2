package text

import (
	"golang.org/x/image/font"
	"golang.org/x/image/font/basicfont"
	"golang.org/x/image/math/fixed"
	"image"
	"image/color"
)

// https://stackoverflow.com/questions/38299930/how-to-add-a-simple-text-label-to-an-image-in-go

func AddText(img *image.NRGBA, x, y int, label string, color color.Color) {
	//col := color.NRGBA{200, 100, 0, 255}
	//point := fixed.Point26_6{fixed.I(x), fixed.I(y)}
	point := fixed.Point26_6{fixed.I(x), fixed.I(y+13)}
	//point := fixed.Point26_6{fixed.I(x), fixed.I(y+11)}

	d := &font.Drawer{
		Dst:  img,
		Src:  image.NewUniform(color),
		Face: basicfont.Face7x13,
		Dot:  point,
	}
	d.DrawString(label)
}