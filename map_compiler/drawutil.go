package map_compiler

import (
	"fmt"
	"image"
	"image/draw"
	"image/color"
	_draw "golang.org/x/image/draw"
)

var _ = `
func blitImage(dst, src *image.NRGBA, offsetX, offsetY int) {
	fmt.Println("blitImage ... offsetX:", offsetX, "offsetY:", offsetY)

	tileSize := 512
	r := image.Rect(0, 0, tileSize, tileSize)
	fmt.Println("r:", r)
	
	// ??
	p := image.Pt(offsetX, offsetY)
	//draw.Draw(dst, src.Bounds(), src, 
	//	src.Bounds().Min.Sub(p), draw.Src)
	draw.Draw(dst, src.Bounds(), src, 
		src.Bounds().Min.Sub(p), draw.Over)
}
`

// not tested
func blitImage(dst, src *image.NRGBA, offsetX, offsetY int) {
	fmt.Println("blitImage ... offsetX:", offsetX, "offsetY:", offsetY)

	b := src.Bounds()
	fmt.Println("b:", b)
	fmt.Println("b.min:", b.Min)

	//panic(2)
	
	/*
	tileSize := 512
	r := image.Rect(0, 0, tileSize, tileSize)
	fmt.Println("r:", r)
	*/
	
	dp := image.Pt(offsetX, offsetY)
	
	//_draw.Copy(dst, dp, src, image.Pt(0, 0), draw.Over, nil)
	_draw.Copy(dst, dp, src, b, draw.Over, nil)
}

// XXX doesn't accept size but coords...
// non-filled (?)
func drawRectangle(img draw.Image, x1, y1, x2, y2 int, color color.Color) {
    for i:= x1; i<x2; i++ {
        img.Set(i, y1, color)
        img.Set(i, y2, color)
    }

    for i:= y1; i<=y2; i++ {
        img.Set(x1, i, color)
        img.Set(x2, i, color)
    }
}

/*
func drawRectangle2(img draw.Image, x1, y1, x2, y2 int, color color.Color) {
    thickness := 2
    for t:=0; t<thickness; t++ {
        // draw horizontal lines
        for x := x1; x<= x2; x++ {
            img.Set(x, y1+t, color)
            img.Set(x, y2-t, color)
        }
        // draw vertical lines
        for y := y1; y <= y2; y++ {
            img.Set(x1+t, y, color)
            img.Set(x2-t, y, color)
        }
    }
}
*/