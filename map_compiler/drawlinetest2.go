package map_compiler

import (
	"fmt"
	"image"
	"image/color"
	//"image/draw"
	//"math"
	//"slices"
	
	"github.com/StephaneBunel/bresenham"
)

// Get TileIds from pixel coords
func getTileIdsFromPxCoords(x, y, tileSize int) (int, int) {
	tileIdX := x/tileSize
	tileIdY := y/tileSize
	
	return tileIdX, tileIdY
}

// Get tile pixel coords from pixel coords
// global pixel coords
// pixel version ?
// return: 0..tileSize
func getTileCoordsFromPxCoords(x, y, tileSize int) (int, int) {
	// Tile pos
	tileX := x % tileSize
	tileY := y % tileSize

	// Check ?
	if tileX > tileSize || tileY > tileSize {
		panic("error")
	}
	
	return tileX, tileY
}


type CustomPlotter struct {
	tiles [][]*image.NRGBA
	tileSize int
	//Set(x int,  y int, c color.Color)
}

//func NewCustomPlotter(tiles [][]*image.NRGBA) *CustomPlotter {
func NewCustomPlotter() *CustomPlotter {
	p := &CustomPlotter{
		//tiles: tiles,
	}

	return p
}

// Basically its just a custom bresenham line function
// that draws at specific tiles/tilesIds
// and draws at points offsets at that tiles
func (p *CustomPlotter) Set(x int,  y int, c color.Color) {
	fmt.Println("CustomPlotter Set x:", x, "y:", y, "c:", c)

	tileIdX, tileIdY := getTileIdsFromPxCoords(x, y, p.tileSize)

	fmt.Println("tileIdX:", tileIdX)
	fmt.Println("tileIdY:", tileIdY)

	im := p.tiles[tileIdX][tileIdY]

	// 0..tileSize
	tileX, tileY := getTileCoordsFromPxCoords(x, y, p.tileSize)

	fmt.Println("tileX:", tileX)
	fmt.Println("tileY:", tileY)

	//im.Set(x, y, c)
	im.Set(tileX, tileY, c)
}

func (p *CustomPlotter) DrawLine(x1, y1, x2, y2 int, col color.Color) {
	bresenham.Bresenham(p, x1, y1, x2, y2, col)
}

func (p *CustomPlotter) DrawLine2(x1, y1, x2, y2 int,
	col color.Color, tiles [][]*image.NRGBA, tileSize int) {
	
	fmt.Println("CustomPlotter DrawLine2",
		"x1:", x1, "y1:", y1, "x2:", x2, "y2:", y2,
		"col:", col, "tiles: ...", "tileSize:", tileSize)

	// Set vars / data
	p.tiles = tiles
	p.tileSize = tileSize
	
	bresenham.Bresenham(p, x1, y1, x2, y2, col)
}


// ? Draw segmented line ?
func DrawSegmentedLine3(x1, y1, x2, y2 int,
	tiles [][]*image.NRGBA, tileSize int) {
	
	fmt.Println("DrawSegmentedLine3 x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)

	DrawSegmentedLine3Color(x1, y1, x2, y2,
		tiles, tileSize, [3]int{255, 255, 255})
}

// ? Draw segmented line ?
func DrawSegmentedLine3Color(x1, y1, x2, y2 int,
	tiles [][]*image.NRGBA, tileSize int, c [3]int) {
	
	fmt.Println(
		"DrawSegmentedLine3Color x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize, "c:", c)

	//p := NewCustomPlotter(tiles)
	p := NewCustomPlotter()
	_ = p

	//c := color.NRGBA{255, 255, 255, 255}
	//c := color.NRGBA{uint8(r), uint8(g), uint8(b), 255}
	xc := color.NRGBA{uint8(c[0]), uint8(c[1]), uint8(c[2]), 255}

	p.DrawLine2(x1, y1, x2, y2, xc, tiles, tileSize)
}
