package map_compiler

import (
	"fmt"

	"kristallos.ga/rx"

	c "test/common"
	"test/object_renderer/render_backend/rx_backend"
)

type MapCompiler struct {
	// ?
	Map *Map
	MapLoader *MapLoader
	MapSaver *MapSaver
}

func newMapCompiler() *MapCompiler {
	mc := &MapCompiler{
		// ?
		Map: NewMap(),
		MapLoader: NewMapLoader(),
		MapSaver: NewMapSaver(),
	}

	return mc
}

func (mc *MapCompiler) init() {

	fmt.Println("MapCompiler init")
}

func Init(resX, resY int) {
	fmt.Println("map_compiler: Init")

	//Test4()
	
	fmt.Println("resX:", resX)
	fmt.Println("resY:", resY)

	conf := rx.NewDefaultConf()

	conf.DebugDraw = true

	// ??
	//rxi := rx.NewRx(nil)
	rxi := rx.NewRx(conf)
	rxi.Init(resX, resY)

	// XXX
	initCtx()

	fmt.Println("init backend...")

	xb := rx_backend.NewBackend(rxi)
	xb.Init()

	// XXX
	ctx.renderBackend = xb

	// XXX
	rc := NewRenderCache()
	ctx.renderCache = rc
	
	// XXX fixme
	// move ?
	//Test4()

	// XXX
	mapCompiler := newMapCompiler()
	mapCompiler.init()
	ctx.mapCompiler = mapCompiler

	// XXX prepare temp
	if true {
		c.PrepareTmpDir()
	}
		
	fmt.Println("done map_compiler init")
}
