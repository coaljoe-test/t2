package map_compiler

import (
	"fmt"
	//"slices"
)

// Util

// Get all scene nodes ?
func MdGetSceneNodes(s *MapDataScene) []*MapDataNode {

	fmt.Println("MdGetSceneNodes s:", s)

	return s.Nodes
}

func MdGetSceneNodesWithType(s *MapDataScene, typeString string) []*MapDataNode {

	fmt.Println("MdGetSceneNodesWithType s:", s, "typeString:", typeString)

	ret := make([]*MapDataNode, 0)

	if typeString != "mesh" &&
		typeString != "scene" {

		panic(fmt.Sprintf("unknown typesString: %s", typeString))
	}

	for i, node := range s.Nodes {
		fmt.Println("-> i:", i)

		fmt.Println("nodeType:", node.NodeType)
	
		//if node.NodeType == "scene" {
		if node.NodeType == typeString {
			ret = append(ret, node)
		}
	}

	return ret
}

// Return nil if there's no mesh ?
// Returns mesh object
func MdGetMeshForSceneNode(s *MapDataScene, n *MapDataNode) *MapDataMesh {

	fmt.Println("MdGetMeshForSceneNode s:", s, "n:", n)
	
	// Mesh/obj id
	meshId := *n.MeshId
	
	if !MdHasMeshWithId(s, meshId) {
		return nil
	}

	// Mesh obj/rec ?
	meshObj	:= MdGetMeshById(s, meshId)
	
	return meshObj
}

// Return nil if there's no meshgen ?
func MdGetMeshGenForSceneNode(s *MapDataScene, n *MapDataNode) *MapDataMeshGen {

	fmt.Println("MdGetMeshGenForSceneNode s:", s, "n:", n)
	
	// Mesh/obj id
	meshId := *n.MeshId
	
	if !MdHasMeshGenWithId(s, meshId) {
		return nil
	}

	// Mesh obj/rec ?
	meshGenObj := MdGetMeshGenById(s, meshId)
	
	return meshGenObj
}


// SceneHasMeshWithId ?
func MdHasMeshWithId(s *MapDataScene, meshId uuid) bool {

	fmt.Println("MdHasMeshWithId s:", s, "meshId:", meshId)
	
	for _, x := range s.Meshes {
		if x.Mesh == nil {
			continue
		}
		if x.Id == meshId {
			return true
		}
	}
	
	return false
}

func MdHasMeshGenWithId(s *MapDataScene, meshId uuid) bool {

	fmt.Println("MdHasMeshGenWithId s:", s, "meshId:", meshId)
	
	for _, x := range s.Meshes {
		if x.MeshGen == nil {
			continue
		}
		if x.Id == meshId {
			return true
		}
	}
	
	return false
}

// Get mesh object ? (MeshDataMesh)
func MdGetMeshById(s *MapDataScene, meshId uuid) *MapDataMesh {

	fmt.Println("MdGetMeshById s:", s, "meshId:", meshId)
	
	for _, x := range s.Meshes {
		if x.Mesh == nil {
			continue
		}
		if x.Id == meshId {
			return x
		}
	}
	
	pp("no such mesh. meshId:", meshId)
	return nil
}

// Get mesh record (string) ?
// Returns mesh path
func MdGetMeshPathById(s *MapDataScene, meshId uuid) *string {

	fmt.Println("MdGetMeshById s:", s, "meshId:", meshId)

	meshData := MdGetMeshById(s, meshId)

	return 	meshData.Mesh
}

// Returns mesh gen object / record
func MdGetMeshGenById(s *MapDataScene, meshId uuid) *MapDataMeshGen {

	fmt.Println("MdGetMeshGenById s:", s, "meshId:", meshId)
	
	for _, x := range s.Meshes {
		if x.MeshGen == nil {
			continue
		}
		if x.Id == meshId {
			return x.MeshGen
		}
	}
	
	pp("no such mesh (MeshGen). meshId:", meshId)
	return nil
}

func MdSceneNodeHasMesh(s *MapDataScene, n *MapDataNode) bool {

	fmt.Println("MdSceneNodeHasMesh s:", s, "n:", n)
	
	if n.MeshId == nil {
		pp("MeshId is nil")
	}
	
	return MdHasMeshWithId(s, *n.MeshId)
}

func MdSceneNodeHasMeshGen(s *MapDataScene, n *MapDataNode) bool {

	fmt.Println("MdSceneNodeHasMeshGen s:", s, "n:", n)
	
	if n.MeshId == nil {
		pp("MeshId is nil")
	}
	
	return MdHasMeshGenWithId(s, *n.MeshId)
}
