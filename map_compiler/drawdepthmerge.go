package map_compiler

import (
	"fmt"
	"image"
	//"image/draw"

	ren "test/rengine"
)

// ? Gray 8 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
// invertDepth - invert the depth textures / values
func SWDepthMergeImages(bgDepth,
	bgColor, objDepth, objColor image.Image, invertDepth bool) (
	image.Image, image.Image) {
 
	fmt.Println("drawdepthmerge SWDepthMergeImages")

	out, outDepth := ren.SWDepthMergeImages(bgDepth,
		bgColor, objDepth, objColor, invertDepth)

	return out, outDepth
}


// ? Gray 16 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
// invertDepth - invert the depth textures / values
func SWDepthMergeImages16(bgDepth,
	bgColor, objDepth, objColor image.Image, invertDepth bool) (
	image.Image, image.Image) {
 
	fmt.Println("drawdepthmerge SWDepthMergeImages16")

	out, outDepth := ren.SWDepthMergeImages16(bgDepth,
		bgColor, objDepth, objColor, invertDepth)

	return out, outDepth
}
