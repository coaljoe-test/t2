package map_compiler

var _ = `

import (
	"fmt"
	"math"
	
	rl "github.com/gen2brain/raylib-go/raylib"
)

type Line struct {
	Start rl.Vector2
	End   rl.Vector2
}

// https://gist.github.com/joetifa2003/867bbf4aa2ec05027e12f5e388172626
// also:
// http://walter.bislins.ch/blog/index.asp?page=Schnittpunkt+zweier+Geraden+berechnen+%28JavaScript%29
func lineIntersect(l1, l2 Line) (rl.Vector2, bool) {
	det := (l1.End.X-l1.Start.X)*(l2.End.Y-l2.Start.Y) - (l1.End.Y-l1.Start.Y)*(l2.End.X-l2.Start.X)

	if det == 0 {
		return rl.Vector2{}, false
	}

	t := ((l2.Start.X-l1.Start.X)*(l2.End.Y-l2.Start.Y) - (l2.Start.Y-l1.Start.Y)*(l2.End.X-l2.Start.X)) / det
	u := -((l1.Start.X-l1.End.X)*(l1.Start.Y-l2.Start.Y) - (l1.Start.Y-l1.End.Y)*(l1.Start.X-l2.Start.X)) / det

	if t >= 0 && t <= 1 && u >= 0 && u <= 1 {
		// Intersection point
		intersectionX := l1.Start.X + t*(l1.End.X-l1.Start.X)
		intersectionY := l1.Start.Y + t*(l1.End.Y-l1.Start.Y)
		return rl.Vector2{intersectionX, intersectionY}, true
	}

	// Lines do not intersect within the given segments
	return rl.Vector2{}, false
}
`
