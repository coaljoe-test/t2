package map_compiler

import (
	"fmt"
	//"os"
	//_path "path"
	"encoding/json"
	
	"github.com/metalim/jsonmap"
	//v4 "github.com/fossoreslp/go-uuid-v4"
)

type MapSaver struct {

}

func NewMapSaver() *MapSaver {
	ms := &MapSaver{}

	return ms
}

// Load map scene from file ?
func (ms *MapSaver) SaveScene(s *Scene, path string) {

	fmt.Println("MapSaver SaveScene s:", s, "path:", path)

	m := ms.encodeScene(s)

	fmt.Println("m:", m)
	fmt.Println()
	
	data, err := json.Marshal(&m)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(data))

	fmt.Println()

	data2, err2 := json.MarshalIndent(&m, "", "    ")
	if err2 != nil {
		panic(err2)
	}
	fmt.Println(string(data2))

	//pp(2)
}

// Encode map scene
func (ms *MapSaver) encodeScene(s *Scene) *jsonmap.Map {

	fmt.Println("_ encodeScene s:", s)

	m := jsonmap.New()

	{
		meshesRec := make([]*jsonmap.Map, 0)

		for _, mesh := range s.Meshes {
			m2 := ms.encodeMesh(mesh)
			_ = m2

			meshesRec = append(meshesRec, m2)
		}

		m.Set("meshes", meshesRec)
	}

	nodesRec := make([]*jsonmap.Map, 0)

	for _, node := range s.Nodes {
		m2 := ms.encodeSceneNode(node)
		_ = m2

		nodesRec = append(nodesRec, m2)
	}

	m.Set("nodes", nodesRec)

	fmt.Println("m:", m)

	return m
}

// Encode scene node
func (ms *MapSaver) encodeSceneNode(n *SceneNode) *jsonmap.Map {

	fmt.Println("_ encodeSceneNode n:", n)

	m := jsonmap.New()
	m.Set("_id", n.Id)
	m.Set("_type", "scene_node")

	m.Set("type", n.NodeType)

	// Mesh ?
	//m.Set("mesh_id", n.MeshId)
	if n.Mesh != nil {
		meshId := n.Mesh.Id
		fmt.Println("meshId:", meshId)
		m.Set("mesh_id", meshId)
	} else {
		fmt.Println("mesh is nil")
		//pp(2)
	}

	fmt.Println("m:", m)

	//pp(2)

	return m
}

// Encode mesh ?
func (ms *MapSaver) encodeMesh(mesh *Mesh) *jsonmap.Map {

	fmt.Println("_ encodeMesh mesh:", mesh)

	m := jsonmap.New()
	m.Set("_id", mesh.Id)
	m.Set("_type", "mesh")

	//if mesh.HasMesh() {
	if mesh.Mesh != nil {
		m.Set("mesh", *mesh.Mesh)
	//} else if mesh.HasMeshGen() {
	} else if mesh.MeshGen != nil {

	} else {
		panic("unknown mesh type (mesh is nil; meshGen is nil)")
	}

	fmt.Println("m:", m)

	//pp(2)

	return m
}