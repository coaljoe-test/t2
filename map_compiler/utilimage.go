package map_compiler

import (
	"fmt"
	"os"
	"image"
	"image/png"
//	"image/color"
//	"math"
	"bytes"
	"bufio"
)

func LoadPng(path string) image.Image {
	fmt.Println("LoadPng path:", path)

	//_panic(3)

	f, err := os.Open(path)
    if err != nil {
        panic(err)
    }
    image, _, err := image.Decode(f)

    return image
}

func SavePng(im image.Image, path string) {
	fmt.Println("SavePng path:", path)

	//_panic(3)

	f, err := os.Create(path)
    if err != nil {
        panic(err)
    }
    err = png.Encode(f, im)
	if err != nil {
		panic(err)
	}
}

// Save png to bytes data
//func SavePngToBytes(im image.Image, buf *bytes.Buffer) {
func SavePngToBytes(im image.Image) []byte {
	fmt.Println("SavePngToBytes")

	//_panic(3)

	var b bytes.Buffer

	buf := bufio.NewWriter(&b)

    err := png.Encode(buf, im)
	if err != nil {
		panic(err)
	}

	return b.Bytes()
}

