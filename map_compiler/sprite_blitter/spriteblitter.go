package sprite_blitter

import (
	"image"

	ren "gitlab.com/coaljoe-test/t2/rengine"
)

type SpriteBlitter struct {
}

// Get sprite depth at
func (sp *SpriteBlitter) GetSpriteDepthAtPos(s *Sprite, pos [3]float64) {
	panic("not implemented")
}

// Make sprite depth image at pos
func (sp *SpriteBlitter) MakeSpriteImageDepthAtPos(s *Sprite, pos [3]float64) *image.NRGBA {
	panic("not implemented")
}

// Make sprite depth image at pos (SW version)
func (sp *SpriteBlitter) MakeSpriteImageDepthAtPosSW(s *Sprite, pos [3]float64) *image.NRGBA {
	panic("not implemented")

	/*
	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	im := ren.LoadPng("test_gray8_depth.png")
	_ = im

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)

	ren.SavePng(im, "/tmp/out.png")

	*/
}