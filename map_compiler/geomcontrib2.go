package map_compiler

// https://gamedev.stackexchange.com/a/111106

var _ = `
func rectToLines(rect rl.Rectangle) [4]Line {
	return [4]Line{
		{Start: rl.Vector2{X: rect.X, Y: rect.Y + rect.Height}, End: rl.Vector2{X: rect.X, Y: rect.Y}},                           // left
		{Start: rl.Vector2{X: rect.X + rect.Width, Y: rect.Y}, End: rl.Vector2{X: rect.X + rect.Width, Y: rect.Y + rect.Height}}, // right

		{Start: rl.Vector2{X: rect.X, Y: rect.Y}, End: rl.Vector2{X: rect.X + rect.Width, Y: rect.Y}},                             // top
		{Start: rl.Vector2{X: rect.X + rect.Width, Y: rect.Y + rect.Height}, End: rl.Vector2{X: rect.X, Y: rect.Y + rect.Height}}, // bottom
	}
}
`
