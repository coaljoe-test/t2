package map_compiler

import (
	"fmt"
	"os"
	"image"

	"test/object_renderer/render_backend/backend"
	c "test/object_renderer/render_backend/common"
	//"test/object_renderer/render_backend/rx_backend"

	//"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"

	
	//ren "gitlab.com/coaljoe-test/t2/rengine"
)

type ObjectRenderer struct {

}

func NewObjectRenderer() *ObjectRenderer {
	fmt.Println("NewObjectRenderer")

	or := &ObjectRenderer{}

	return or
}

func (r *ObjectRenderer) getRenderBackend() backend.RenderBackendI {

	fmt.Println("RenderObject getRenderBackend")

	fmt.Println("get renderBackend...")
	
	b := ctx.renderBackend

	fmt.Println(b)

	return b
}

func (r *ObjectRenderer) RenderObject(path string, modeDepth bool) image.Image {
	fmt.Println("ObjectRenderer RenderObject path:", path, "modeDepth:", modeDepth)

	return r.doRenderObject(path, modeDepth)
}

func (r *ObjectRenderer) RenderObjectEx(path string,
	modeDepth bool, rot *[3]float32) image.Image {
	
	fmt.Println("ObjectRenderer RenderObjectEx path:", path,
		"modeDepth:", modeDepth, "rot:", rot)

	return r.doRenderObjectEx("data", path, modeDepth, rot)
}

func (r *ObjectRenderer) doRenderObject(path string, modeDepth bool) image.Image {
	fmt.Println("ObjectRenderer doRenderObject path:", path, "modeDepth:", modeDepth)

	return r.doRenderObjectEx("data", path, modeDepth, nil)
}

//// Gen meshes

// render with default rot ?
// render with default scale ?
func (r *ObjectRenderer) RenderObjectGen(meshGen *MapDataMeshGen, modeDepth bool) image.Image {
	fmt.Println("ObjectRenderer RenderObjectGen meshGen:", meshGen, "modeDepth:", modeDepth)

	// XXX ?
	dump(meshGen)
	//panic(2)

	// XXX
	return r.doRenderObjectEx3Gen(meshGen, modeDepth, nil, nil)
}

// XXX rename to RenderObjectGenEx ?
// color = optional ?
func (r *ObjectRenderer) RenderObjectExGen(meshGen *MapDataMeshGen, modeDepth bool,
	rot *[3]float32, scale *[3]float32) image.Image {
	
	fmt.Println(
		"ObjectRenderer RenderObjectExGen meshGen:", meshGen,
		"modeDepth:", modeDepth, "rot:", rot, "scale:", scale)

	return r.doRenderObjectEx3Gen(meshGen, modeDepth, rot, scale)
}

// XXX test generate from mesh gen data / record
// XXX and render ?
func (r *ObjectRenderer) doRenderObjectEx3Gen(meshGen *MapDataMeshGen, modeDepth bool,
	rot *[3]float32, scale *[3]float32) image.Image {

	fmt.Println(
		"ObjectRenderer doRenderObjectEx3Gen meshGen:", meshGen, "modeDepth:", modeDepth,
		"rot:", rot, "scale:", scale)

		/*
		b := ctx.renderBackend

		sizeX := meshGen.Size[0]
		sizeY := meshGen.Size[1]
		sizeZ := meshGen.Size[2]

		// md
		meshData := b.GenCubeMeshData(sizeX, sizeY, sizeZ)
		
		im := b.RenderObjectMeshData(meshData)
		_ = im
		*/
	
		//panic(2)
		
		// XXX fixme ?

		b := r.getRenderBackend()
		//b := ctx.renderBackend
		
		var md *c.MeshData
		
		//md = &c.MeshData{}
		sizeX := meshGen.Size[0]
		sizeY := meshGen.Size[1]
		sizeZ := meshGen.Size[2]
		md = b.GenCubeMeshData(sizeX, sizeY, sizeZ)

		// XXX ?
		//c := Vec3{1, 0, 0}
		//c := [3]int{255, 0, 0}
		c := meshGen.Color
		md.Color = c

		// ?
		//pdump(md)
		
		return r.doRenderObjectEx("mesh_data", md, modeDepth, nil)
		
		//panic(2)
}


// XXX
// mesh data based renderer ?
// for generated meshes ?
// it doesn't take a path as parameter
func (r *ObjectRenderer) doRenderObjectEx2Gen(meshData *c.MeshData, modeDepth bool, rot *[3]float32) image.Image {
	fmt.Println("ObjectRenderer doRenderObjectEx2Gen meshData:", meshData, "modeDepth:", modeDepth,
		"rot:", rot)

		/*
		b := ctx.renderBackend
		
		im := b.RenderObjectMeshData(meshData)
		_ = im
		*/
	
		panic(2)
}

//func (r *ObjectRenderer) doRenderObjectEx2(meshList []*rx.MeshNode, modeDepth bool, rot *[3]float32) image.Image {
//	fmt.Println("ObjectRenderer doRenderObjectEx2 meshList:", meshList, "modeDepth:", modeDepth,
//		"rot:", rot)
//
//		panic(2)
//}

// XXX out to /tmp/renderer
// XXX        /tmp/rendered/[object_id]
//func (r *ObjectRenderer) doRenderObjectEx(path string, modeDepth bool, rot *[3]float32) image.Image {
//	fmt.Println("ObjectRenderer doRenderObjectEx path:", path, "modeDepth:", modeDepth,
// modeString:
// data = gltf path/data path (normal)
// mesh_data = MeshData (rx) (gen/mesh_gen)
func (r *ObjectRenderer) doRenderObjectEx(modeString string,
	data interface{}, modeDepth bool, rot *[3]float32) image.Image {
	
	fmt.Println("ObjectRenderer doRenderObjectEx modeString:", modeString,
		"data:", data, "modeDepth:", modeDepth, "rot:", rot)

	// XXX this should be in backend ?

	var _ = `

	// XXX
	{

		conf := rx.NewTestFwConf()
		//conf.ResX = 1280
		//conf.ResY = 800
		//conf.ResX = 800
		//conf.ResY = 600
		//conf.ResX = 8192
		//conf.ResY = 8192
		//conf.ResX = 10000
		//conf.ResY = 10000
		//conf.ResX = 2048
		//conf.ResY = 2048

		conf.ResX = 512
		conf.ResY = 512

		//panic(3)
	

		//app := rx.TestFwInit()
		app := rx.TestFwInitConf(conf)
		_ = app
		sce := rx.TestFwCreateDefaultScene()
		_ = sce
	
		// XXX disable debug ?
		xconf := rx.GetConf()
		_ = xconf
		//xconf.DebugDraw = false

		// ?
	
		/*
		// Iso cam
		cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		//fmt.Println(cn.Camera.Fov())
		//panic(2)
		cn.Camera.SetFov(10)


		gc := newGameCamera()
		Ctx.GameCamera = gc
		gc.setup(cn, Vec3{60 ,0, 45}, 40.0)
		*/

		rxi := rx.Rxi()
		cn := rxi.Camera
		//cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		mulV := 5.0
		cn.SetPos(Vec3{12.24745*mulV, -12.24745*mulV, 10.0*mulV})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		// XXX
		fmt.Println(cn.Camera.Fov())
		//panic(2)
		//cn.Camera.SetFov(10)
		//cn.Camera.SetFov(2)
		//cn.Camera.SetFov(4)
		//cn.Camera.SetFov(5)
		//cn.Camera.SetFov(150)

		cn.Camera.SetZnear(0.001)
		cn.Camera.SetZfar(100000.0)
		cn.Camera.SetFov(150)

		////

		// Zoom

		{
			if true {
				nativeResX := 512
				nativeResY := 512
				nativeZoom := 1.0
				_ = nativeResX
				_ = nativeResY
				_ = nativeZoom
	
				zoomV := 1.0

				scale := float64(conf.ResX)/float64(nativeResX)
				scaleZ := 1.0/scale

			
				fmt.Println("scale:", scale)
				fmt.Println("scaleZ:", scaleZ)

				//zoomV = scaleZ
				zoomV = scale
		
				cn.Camera.SetZoom(zoomV)

				//panic(2)
			}

		}
	
		////

		l1 := sce.GetNodeByName("light1")
		//l1.SetPosZ(10)
		l1.SetPos(Vec3{-2, -15, 2})

	}
	
	`

	////

	var b backend.RenderBackendI

	//fmt.Println("x rx_backend init...")
	//xb := rx_backend.NewBackend()
	//xb.Init()
	fmt.Println("get renderBackend...")
	xb := ctx.renderBackend
	b = xb

	fmt.Println(b)

	//path := "../untitled.gltf"
	//path := "../untitled.gltf"
	//path := "../testdata/tree1/tree1_no_mat.glb"
	//path := os.Args[1]
	//path := flag.Arg(0)
	//fmt.Println("path:", path)

	// data buf
	var dataBuf []byte
	_ = dataBuf
	var md *c.MeshData

	if modeString == "data" {
		path := data.(string)
		fmt.Println("path:", path)
	
		if path == "" {
			panic("path is empty")
		}

		fmt.Println("... path:", path)
		//panic(2)
	
		var err error
		dataBuf, err = os.ReadFile(path)
			
		if err != nil {
			panic(err)
		}
	} else if modeString == "mesh_data" {
		md = data.(*c.MeshData)

		// XXX
		//pdump(md)
	} else {
		panic("unknown modeString")
	}



	t := c.NewTransform()

	if rot != nil {
		rotV := *rot
		panic(rotV)
	}

	//t.SetScale([3]float64{0.5, 0.5, 0.5})
	//t.SetScale([3]float64{2.0, 2.0, 2.0})
	//t.SetScale([3]float64{0.1, 0.1, 0.1})

	//t.SetRot([3]float64{0.0, 0.0, 45.0})

	//t.SetPos([3]float64{10.0, 0.0, 0.0})
	
	var im image.Image
	var imDepth image.Image
	
	// XXX
	outPath := GetTmpDir() + "/out1.png"
	outPath2 := GetTmpDir() + "/out2.png"

	if modeString == "data" {
		xpath := data.(string)
		fmt.Println("xpath:", xpath)
		//panic(2)

		//im = b.RenderObject(dataBuf, t)
		im = b.RenderObject(xpath, t)
		_ = im

		//imDepth = b.RenderObjectDepth(dataBuf, t)
		imDepth = b.RenderObjectDepth(xpath, t)
		_ = imDepth

		//b.RenderObjectToPath(dataBuf, t, outPath)
		b.RenderObjectToPath(xpath, t, outPath)

		//b.RenderObjectDepthToPath(dataBuf, t, outPath2)
		b.RenderObjectDepthToPath(xpath, t, outPath2)
	} else if modeString == "mesh_data" {
		im = b.RenderObjectMeshData(md, t)
		_ = im

		imDepth = b.RenderObjectMeshDataDepth(md, t)
		_ = imDepth

		b.RenderObjectMeshDataToPath(md, t, outPath)

		b.RenderObjectMeshDataDepthToPath(md, t, outPath2)
	} else {
		panic("unknown modeString")
	}

	if !modeDepth {
		return im
	} else {
		return imDepth
	}
}
