package map_compiler

import (
	"fmt"
	//"slices"

	"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"
	_math "kristallos.ga/rx/math"
)

// Util

// Apply modifiers ?
func MdFlatternScene(s *MapDataScene) {

	fmt.Println("MdFlatternScene s:", s)

	panic("not implemented")
}

// Apply modifiers ?
func MdApplyModifiers(s *MapDataScene) {

	fmt.Println("MdApplyModifiers s:", s)

	panic("not implemented")
}

func MdGetSceneBoundingBox(s *MapDataScene) rx.AABB {

	fmt.Println("MdGetSceneBoundingBox s:", s)

	//panic("not implemented")

	// XXX
	node := s.Nodes[0]
	fmt.Println()
	fmt.Println("node:")
	dump(node)

	var ret rx.AABB

	if MdSceneNodeHasMesh(s, node) {
		mesh := MdGetMeshForSceneNode(s, node)
		//fmt.Println()
		//pdump(mesh)
		path := *mesh.Mesh
		// XXX
		xpath := "../" + path
		fmt.Println("xpath:", xpath)
		bbox := ctx.renderBackend.GetObjectBoundingBox(xpath)
		fmt.Println("bbox:", bbox)
		// min
		min := _math.Vec3{bbox[0], bbox[1], bbox[2]}
		// max
		max := _math.Vec3{bbox[3], bbox[4], bbox[5]}
		ret = rx.NewAABBFromPoints(min, max)
		//pp(2)
	} else if MdSceneNodeHasMeshGen(s, node) {
		meshGen := MdGetMeshGenForSceneNode(s, node)
		//fmt.Println()
		//pdump(meshGen)

		if meshGen.Type == "cube" {
			size := meshGen.Size
			// min
			min := _math.Vec3{0, 0, 0}
			// max
			max := _math.Vec3{size[0], size[1], size[2]}
			ret = rx.NewAABBFromPoints(min, max)
		} else {
			pp("unknown type:", meshGen.Type)
		}
		
	} else {
		pp("no mesh or meshGen")
	}

	fmt.Println()
	fmt.Println("ret:", ret)

	return ret
}

// ?
func MdGetMapBoundingBox(m *MapData) rx.AABB {

	fmt.Println("MdGetMapBoundingBox m:", m)

	panic("not implemented")
}
