package map_compiler

import (
	"fmt"

	"test/object_renderer/render_backend/backend"
)

// ?
var ctx *Context

type Context struct {
	renderBackend backend.RenderBackendI
	renderCache *RenderCache

	// ?
	mapCompiler *MapCompiler
}

func initCtx() {
	fmt.Println("initCtx")

	ctx = &Context{}
}
