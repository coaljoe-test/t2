package map_compiler

import (
	"fmt"
	"image"
	"image/draw"
	//"image/png"
	//"image/color"
	"math"
)

// XXX it doesn't pad images ?
// offsetXY = tile offset
// Ideologically similar to imagemagick splice (?)
// Tile offset can't be bigger than tile size
// returns sx -> sy -> [images / tiles]
// Split image onto some sort of a grid (with an offsets, if they're presented)
// TODO: it can also return regions
// (like drawing rects, an array for each image)
// position at which tile(s) should be drawn
// maybe it should be a special function
func SplitImageIntoSegments(im *image.NRGBA,
	offsetX, offsetY int, tileSize int) (int, int, []*image.NRGBA) {
	
	fmt.Println("SplitImageIntoSegments im: (isnil)",
		im == nil, "offsetX:", offsetX,
		"offsetY:", offsetY, "tileSize:", tileSize)

	//panic("not imlemented")

	if offsetX > tileSize {
		panic("bad offset x?")
	}

	if offsetY > tileSize {
		panic("bad offset y?")
	}
	
	images := make([]*image.NRGBA, 0)

	b := im.Bounds()
	imW := b.Max.X
	imH := b.Max.Y
	
	fmt.Println("imW:", imW)
	fmt.Println("imH:", imH)
	
	if tileSize > imW  || tileSize > imH {
		panic("error")
	}
	// XXX TODO test with overflow / offset ?

	overflowX := imW - tileSize

	fmt.Println("overflowX:", overflowX)

	overflowY := imH - tileSize

	fmt.Println("overflowY:", overflowY)
	
	if overflowX <= 0 && overflowY <= 0 {
		fmt.Println("return...")
		// Then just draw
		//panic(2)
		images = append(images, im)
		
		return 1, 1, images
	}

	var newSx int
	var newSy int

	if overflowX <= 0 {
		newSx = 1
	} else {
		//newSx = imW / tileSize
		newSx = int(math.Ceil((float64(imW)+float64(offsetX)) / float64(tileSize)))
		//newSx := int(float64(overfloatX))
		//_ = newSx
	}
          
	if overflowY <= 0 {
		newSy = 1
	} else {
		//newSy = imH / tileSize
		newSy = int(math.Ceil((float64(imH)+float64(offsetY)) / float64(tileSize)))
	}

	fmt.Println("newSx:", newSx)
	fmt.Println("newSy:", newSy)
	
	// New image size ?
	// with padding / new tiles ?
	// after resize (new size) ?
	newImW := newSx * tileSize
	newImH := newSy * tileSize
	
	fmt.Println("newImW:", newImW)
	fmt.Println("newImH:", newImH)
	
	var subImageRect image.Rectangle
	_ = subImageRect

	// yoffset...
	//yShift := 0
	yShift := offsetY
	
	for y := 0; y < newSy-1; y++ {
		//yShift += (y*tileSize) + offsetY
		yShift += tileSize

		for x := 0; x < newSx-1; x++ {
			fmt.Println("-> x:", x, "y:", y)
		
			var subImage *image.NRGBA
			_ = subImage

			// Rect ?
			var px, py int
			var sx, sy int

			//// Else

			// Just copy tile
			
			// Create coord
				
			//px = (x*tileSize) + offsetX
			//py = (y*tileSize) + offsetY
			//py = offsetY
			px = (x*tileSize)
			py = (y*tileSize)

			sx = tileSize
			sy = tileSize
		
		
			var _ = `
			// Get subimage 1
			///if offsetX > 0 {
			if offsetX > 0 || offsetY > 0 {
				// First...
				// tl
				//if x == 0 {
				if y == 0 && x == 0 {
					//px := (x*tileSize) + offsetX
					//py := (y*tileSize) + offsetY
					px = offsetX
					py = offsetY
					
					sx = tileSize - offsetX
					sy = tileSize - offsetY			
				}
				// Last...
				// tr
				//if x == newSx-1 {
				if y == 0 && x == newSx-1 {
					px = x*tileSize
					//py = (y*tileSize) + offsetY
					py = offsetY
					
					sx = offsetX
					sy = tileSize - offsetY
				}
			}
			`

			fmt.Printf("p: %v, %v\n", px, py)
			fmt.Printf("s: %v, %v\n", sx, sy)
				
			//r := image.Rect(px, py, tileSize, tileSize)
			//r := image.Rect(px, py, sx, sy)

			xpy := py + yShift
			_ = xpy
			//r := image.Rect(px, xpy, sx, sy)
			//r := image.Rectangle{px, xpy, sx, sy}
			// x0, y0, x1, y1
			//r := image.Rect(px, py, px+(sx-px), py+(sy-py))
			r := image.Rect(px, py, px+sx, py+sy)
			//r := image.Rect(px, xpy, px+(sx-px), py+(sy-xpy))
			//r := image.Rect(px, xpy, px, xpy).Add(image.Pt(sx, sy))
			fmt.Println("r:", r)
			fmt.Println("size r:", r.Size())
			
			// Cut coord
			
			/*
			if true {
				if offsetX > 0 || offsetY > 0 {
					// Min top
					minY := offsetY
					minX := offsetX
				
					fmt.Println("Cut coord...")
					// First...
					// tl
					
					// Top row
					if y == 0 {
						if py > minY {
							py = minY
						}
					}
					
					newR := image.Rect(px, py, px+sx, py+sy)
					fmt.Println("newR:", newR)
					fmt.Println("size newR:", newR.Size())
				}
			}
			*/
			
			if true {
				if offsetX > 0 || offsetY > 0 {
					// A frame ?
							
					// Min left (x) coord
					minX := offsetX
					// Max right (x) coord
					maxX := newImW - offsetX
					// Min top (y) coord
					minY := offsetY
					// Max bottom (y) coord
					maxY := newImH - offsetY
				
					fmt.Println("minX:", minX)
					fmt.Println("maxX:", maxX)
					fmt.Println("minY:", minY)
					fmt.Println("maxY:", maxY)
				
					fmt.Println("Cut coord...")

					if px < minX {
						px = minX
					}
					if px > maxX {
						px = maxX
					}
					if py < minY {
						py = minY
					}
					if py > maxY {
						py = maxY
					}
					
					// Change size
					if true {
						// Valid points/regions
						// Top left
						// Left
						// Top
					
						// Top row
						if y == 0 {
							//pp(2)
							sy = tileSize - offsetY
							// Top left (tl)
							if x == 0 {
								sx = tileSize - offsetX
							}
						} else {
							//pp(2)
							// Left
							if x == 0 {
								sx = tileSize - offsetX
							}
						}
					}
					
					newR := image.Rect(px, py, px+sx, py+sy)
					fmt.Println("newR:", newR)
					fmt.Println("size newR:", newR.Size())
					
					// XXX
					r = newR
				}
			}
			
			//panic(2)
			
			fmt.Println("x r:", r)
			fmt.Println("x size r:", r.Size())

			subImage = im.SubImage(r).(*image.NRGBA)
			
			b2 := subImage.Bounds()
			fmt.Println("b2:", b2)
			fmt.Println("x b2:", b2.Size())
			
			if b2.Size().X <= 0 {
				panic("subImage size is zero")
			}
			
			// XXX
			SavePng(subImage, GetTmpDir() + "/x.png")

			// XXX
			images = append(images, subImage)
		}
	}

	return newSx, newSy, images
}

// Just pad the images / tiles
func SplitImageIntoSegmentsPadded(im *image.NRGBA,
	offsetX, offsetY int, tileSize int) (int, int, []*image.NRGBA) {

	fmt.Println("SplitImageIntoSegmentsPadded im: (isnil)",
		im == nil,"offsetX:", offsetX,
		"offsetY:", offsetY, "tileSize:", tileSize)
	
	//panic("not implemented")
	
	sx, sy, images := SplitImageIntoSegments(im, offsetX, offsetY, tileSize)
	
	fmt.Println("sx:", sx)
	fmt.Println("sy:", sy)
	
	// Just return images
	if offsetX == 0 && offsetY == 0 {
		fmt.Println("return...")
		return sx, sy, images
	}
	
	for y := 0; y < sy-1; y++ {
		for x := 0; x < sx-1; x++ {
			fmt.Println("-> x:", x, "y:", y)
			
			// Tile index ?
			tileId := (y * sy) + x
			fmt.Println("tileId:", tileId)

			tile := images[tileId]
			//fmt.Println("tile:", tile)
			fmt.Println("tile size:", tile.Bounds().Size())
			fmt.Println("tile bounds:", tile.Bounds())
			
			// New tile image
			newImage := image.NewNRGBA(image.Rectangle{
				Max: image.Point{X: tileSize, Y: tileSize}})
			
			// Expand tiles ?
			updated := false

			// Top row
			if y == 0 {
				// Top left (tl)
				//if y == 0 && x == 0 {
				if x == 0 {
					b := tile.Bounds()
					fmt.Println("b:", b)
					fmt.Println("x b:", b.Size())
					newTile := newImage

					// XXX draw at the offset
					//draw.Draw(newTile, tile.Bounds(), tile, 
					//	image.Point{offsetX, offsetY}, draw.Src)

					//draw.Draw(newTile, newTile.Bounds(), tile, 
					//	image.Point{0, 0}, draw.Src)

					//p := image.Pt(0, 20)
					//p := image.Pt(0, 0)
					//p := image.Pt(0, 5)
					p := image.Pt(offsetX, offsetY)

					//draw.Draw(newTile, newTile.Bounds(), tile, 
					//	tile.Bounds().Min.Add(p), draw.Src)
					// ??
					//draw.Draw(newTile, newTile.Bounds(), tile, 
					//	tile.Bounds().Min.Sub(p), draw.Src)
					draw.Draw(newTile, newTile.Bounds(), tile, 
						tile.Bounds().Min.Sub(p), draw.Src)

					// Draws at top-left
					//draw.Draw(newTile, newTile.Bounds(), tile, 
					//	tile.Bounds().Min, draw.Src)
					
					// XXX
					SavePng(tile, GetTmpDir() + "/tile_image.png")
					SavePng(newTile, GetTmpDir() + "/x1.png")
						
					// XXX
					updated = true
					
					//panic(2)
				} else {
					// Top
					
					p := image.Pt(0, offsetY)
					draw.Draw(newImage, newImage.Bounds(), tile, 
						tile.Bounds().Min.Sub(p), draw.Src)
					
					SavePng(tile, GetTmpDir() + "/tile_image.png")
					SavePng(newImage, GetTmpDir() + "/x2.png")
					
					// XXX
					updated = true
					//panic(2)
				}
			} else {
				// Left
				if x == 0 {
					p := image.Pt(offsetX, 0)
					draw.Draw(newImage, newImage.Bounds(), tile, 
						tile.Bounds().Min.Sub(p), draw.Src)
					
					SavePng(tile, GetTmpDir() + "/tile_image.png")
					SavePng(newImage, GetTmpDir() + "/x3.png")
					
					// XXX
					updated = true					
					//panic(2)
				}
			}
			
			// Finally
			if updated {
				fmt.Println("updating tile image...")
				// Replace tile image ?
				*tile = *newImage
				//pp(2)
			}
		}
	}
	
	return sx, sy, images
}

// Draw/blit on tiled image/tiles
// Can split in multiple segments
// x = absolute position (like on map)
// y = absolute position (like on map)
func DrawOnTiles2(im *image.NRGBA, x, y int, tiles [][]*image.NRGBA, tileSize int) {
	fmt.Println("DrawOnTiles2 im: ...", "x:", x, "y:", y,
		"tiles: ...", "tileSize:", tileSize)

	tileIdX := int(float64(x) / float64(tileSize))
	tileIdY := int(float64(y) / float64(tileSize))
	_ = tileIdX
	_ = tileIdY
	
	fmt.Println("tileIdX:", tileIdX)
	fmt.Println("tileIdY:", tileIdY)
	
	//tileIm := mr.getTile(tileIdX, tileIdY)
	//_ = tileIm

	tileIm := tiles[tileIdX][tileIdY]
	_ = tileIm

	b := im.Bounds()
	imW := b.Max.X
	imH := b.Max.Y

	fmt.Println("imW:", imW)
	fmt.Println("imH:", imH)
	
	// XXX test
	if imW > tileSize || imH > tileSize {
		panic("bad image size")
	}

	// Tile offsets

	tileOffsetX := x%tileSize

	fmt.Println("tileOffsetX:", tileOffsetX)

	tileOffsetY := y%tileSize

	fmt.Println("tileOffsetY:", tileOffsetY)

	if tileOffsetX <= 0 && tileOffsetY <= 0 {
		// Then just draw
		
		//sx, sy, images := SplitImageIntoSegments
		
		//panic(2)
	}
	
	//panic(2)
	
	sx, sy, images := SplitImageIntoSegmentsPadded(im, tileOffsetX, tileOffsetY, tileSize)
			
	for y := 0; y < sy; y++ {
		for x := 0; x < sx; x++ {
			fmt.Println("-> x:", x, "y:", y)
			
			ztileIdX := tileIdX + x
			ztileIdY := tileIdY + y
			
			fmt.Println("ztileIdX:", ztileIdX)
			fmt.Println("ztileIdY:", ztileIdY)
			
			// Current tile ?
			//ztile := mr.getTile(ztileIdX, ztileIdY)
			ztile := tiles[ztileIdX][ztileIdY]
			_ = ztile
			
			// Image index ?
			imageId := (y * sx) + x
			
			fmt.Println("imageId:", imageId)
			
			curImage := images[imageId]
			
			//blitImage(ztile, curImage)
			//blitImage(ztile, curImage, 0, 0)
			blitImage(ztile, curImage, tileOffsetX, tileOffsetY)
			//SavePng(ztile, GetTmpDir() + "/x_blit1.png")
			//blitImage(ztile, curImage, tileOffsetX+10, tileOffsetY)
			//SavePng(ztile, GetTmpDir() + "/x_blit2.png")
			
			//pp(2)
		}
	}
}

// Draw/blit on tiled image/tiles
// Can split in multiple segments
func DrawOnTiles(im *image.NRGBA, x, y int, tiles [][]*image.NRGBA, tileSize int) {
	fmt.Println("DrawOnTiles im:", im, "x:", x, "y:", y,
		"tiles:", tiles, "tileSize:", tileSize)

	tileIdX := int(float64(x) / float64(tileSize))
	tileIdY := int(float64(y) / float64(tileSize))
	_ = tileIdX
	_ = tileIdY
	
	fmt.Println("tileIdX:", tileIdX)
	fmt.Println("tileIdY:", tileIdY)
	
	//tileIm := mr.getTile(tileIdX, tileIdY)
	//_ = tileIm

	tileIm := tiles[x][y]
	_ = tileIm

	b := im.Bounds()
	imW := b.Max.X
	imH := b.Max.Y

	fmt.Println("imW:", imW)
	fmt.Println("imH:", imH)

	overflowX := imW - tileSize

	fmt.Println("overflowX:", overflowX)

	overflowY := imH - tileSize

	fmt.Println("overflowY:", overflowY)

	if overflowX <= 0 && overflowY <= 0 {
		// Then just draw
		panic(2)
	}

	// Number of x splits / splits over x ?
	xSplits := imW / tileSize
	fmt.Println("num xsplits:", xSplits)
	ySplits := imH / tileSize
	fmt.Println("num ysplits:", ySplits)
	
	// XXX We should split it many times over x ?
	if overflowX > 0 {
		// Draw subimage
		tile1 := tileIm
		_ = tile1

		r := image.Rect(0, 0, tileSize, tileSize)
		fmt.Println("r:", r)
		
		// Draw subimage
		// Right
		tile2 := tiles[x+1][y]
		_ = tile2

		r2 := image.Rect(0, 0, tileSize, tileSize)
		fmt.Println("r2:", r2)
	}
	
}
