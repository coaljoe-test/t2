package map_compiler

import (
	"fmt"
	"image"
)

func Test4() {
	fmt.Println("Test4")

	if false {
	//if true {
		mr := NewMapRenderer()
	
		// XXX
		// Single-tile mode ?
		//mr.SetNumTiles(1, 1)
		mr.SetNumTiles(4, 4)
		mr.SetTileSize(64)
	
		fmt.Println("...", len(mr.Tiles))
		fmt.Println("...", len(mr.Tiles[0]))

		//pp(2)
	
		//im4 := DrawSegmentedLineTest3(0, 0, 192, 192, 64)
		//SavePng(im4, GetTmpDir() + "/line_out3.png")

		//DrawSegmentedLine3(0, 0, 32, 32, mr.Tiles, 64)
		
		//DrawSegmentedLine3(0, 0, 192, 192, mr.Tiles, 64)
		DrawSegmentedLine3Color(0, 0, 192, 192, mr.Tiles, 64, [3]int{0, 0, 255})

		mr.SaveTiles(GetTmpDir() + "/cache4")

		// XXX
		mr.SaveTilesAsImage(GetTmpDir() + "/cache4_image.png")
		// draw_borders = true
		// draw_names = true
		//mr.SaveTilesAsImageEx(GetTmpDir() + "/cache4_image1.png", true, true)
	}

	//panic(2)

	//testLine()
	
	//SplitLineIntoTileSegments2(0, 0, 128, 0, 64)
	//SplitLineIntoTileSegments2(0, 0, 68, 0, 64)
	//im3 := DrawSegmentedLineTest2(0, 0, 68, 0, 64)
	//im3 := DrawSegmentedLineTest2(0, 0, 128, 0, 64)
	//im3 := DrawSegmentedLineTest2(0, 0, 128, 128, 64)
	im3 := DrawSegmentedLineTest2(0, 0, 192, 192, 64)
	SavePng(im3, GetTmpDir() +"/line_out2.png")
	
	//panic(2)

	//SplitLineIntoTileSegments(0, 0, 128, 0, 64)
	//SplitLineIntoTileSegments(0, 0, 127, 0, 64)
	//SplitLineIntoTileSegments(0, 0, 129, 0, 64)
	//SplitLineIntoTileSegments(128, 0, 0, 0, 64)
	//SplitLineIntoTileSegments(0, 0, 128, 128, 64)
	//SplitLineIntoTileSegments(0, 0, 128, 100, 64)

	//im := DrawSegmentedLineTest(0, 0, 128, 100, 64)
	//im := DrawSegmentedLineTest(0, 0, 128, 128, 64)
	//im := DrawSegmentedLineTest(0, 0, 150, 0, 64)
	//im := DrawSegmentedLineTest(10, 0, 128, 0, 64)
	//im := DrawSegmentedLineTest(0, 0, 128, 100, 64)
	im := DrawSegmentedLineTest(0, 0, 150, 100, 64)
	SavePng(im, GetTmpDir() +"/line_out.png")

	//panic(2)
	
	//s := LoadMapSceneJson("../map_scene6.json")
	//s := LoadMapSceneJson("../map_scene7.json")
	//s := LoadMapSceneJson("../map_scene8.json")
	//s := LoadMapSceneJson("../map_scene9.json")
	//s := LoadMapSceneJson("../map_scene10.json")
	//s := LoadMapSceneJson("../map_scene11.json")
	//s := LoadMapSceneJson("../map_scene12.json")
	s := LoadMapSceneJson("../map_scene13.json")
	//s := LoadMapSceneJson("../map_scene14.json")
	//s := LoadMapSceneJson("../map_scene15.json")
	//s := LoadMapSceneJson("../map_scene16_empty.json")
	//s := LoadMapSceneJson("../map_scene18.json")


	fmt.Println("s:", s)


	ml := ctx.mapCompiler.MapLoader
	s2 := ml.LoadScene("../map_scene13.json")
	
	fmt.Println("s2:", s2)

	fmt.Println()
	fmt.Println("s2:")
	dump(s2)

	//pp(2)

	if true {
	
		ms := ctx.mapCompiler.MapSaver
		ms.SaveScene(s2, GetTmpDir() +"/out.json")

		// XXX
		//newNode := NewSceneNode()
		newNode := NewMeshSceneNode()
		//newNode := NewMeshGenSceneNode()
		//newNode.Pos = float64{10, 0, 0}
		s2.AddNode(newNode)

		// XXX
		//mesh := s2.AddNewMeshWithPath("res1/test.obj")
		mesh2 := NewMesh()
		path := "res1/test.obj"
		mesh2.Mesh = &path
		s2.AddMesh(mesh2)

		// XXX ?
		//newNode.SetMesh(mesh)
		newNode.SetMesh(mesh2)

		//pdump(newNode)
		
		
		ms.SaveScene(s2, GetTmpDir() +"/out2.json")

		pp(2)
	}


	if true {
	//if false {
		fmt.Println("...")
		fmt.Println("...")
	
		s1 := MapLoaderApplyModifiersJson("../map_scene17_editor1.json")
		
		fmt.Println("s1:", s1)
		
		pp(2)
	}

	//if true {
	if false {
		fmt.Println("")
	
		aabb := MdGetSceneBoundingBox(s)
		
		fmt.Println("aabb:", aabb)
		
		//pp(2)
	}

	mr := NewMapRenderer()
	//mr.SetMapData(s)
	// ?
	// SetSceneData ?
	mr.SetScene(s)

	// XXX ?
	if true {
		//draw3dGrid([3]float64{0, 0, 0}, 20, 20, 1.0, [3]int{255, 255, 255}, true)
	}
	
	// XXX
	// Single-tile mode ?
	mr.SetNumTiles(1, 1)
	
	mr.SaveTiles(GetTmpDir() + "/cache2")
	
	if false {
	im2 := LoadPng(GetTmpDir() + "/test_image.png").(*image.NRGBA)
	
	//sx, sy, images := SplitImageIntoSegments(im2, 0, 0, 256)
	//sx, sy, images := SplitImageIntoSegments(im2, 0, 0, 24)
	sx, sy, images := SplitImageIntoSegmentsPadded(im2, 12, 12, 24)
	//sx, sy, images := SplitImageIntoSegments(im2, 12, 12, 24)
	_ = sx
	_ = sy
	_ = images

	// XXX
	for i, x := range images {
		fmt.Println("save image i:", i)

		outPath := fmt.Sprintf(GetTmpDir() + "/out_image%02d.png", i)
		SavePng(x, outPath)
	}
	}
	
	//panic(2)

	//s := md.Scenes[0]
	//mr.RenderScene(s)

	//panic(2)

	//mr.EvaluateBounds()
	mr.EvaluateSceneBounds(s)
	mr.EvaluateSceneBounds2(s)

	//// XXX
	//mr.Test1(s)
	//mr.Test2(s)


	if true {
		//draw3dGrid([3]float64{0, 0, 0}, 20, 20, 1.0, [3]int{255, 255, 255}, true)
	}
	
	mr.RenderAllObjectsToCache(s)

	if true {
		//draw3dGrid([3]float64{0, 0, 0}, 20, 20, 1.0, [3]int{255, 255, 255}, true)
	}
	
	mr.SaveCache(GetTmpDir() +"/cache1")
	
	// XXX
	mr.ClearCache()
	
	mr.RenderScene(s)

	if true {
		//draw3dGrid([3]float64{0, 0, 0}, 20, 20, 1.0, [3]int{255, 255, 255}, true)
	}
	
	mr.SaveTiles(GetTmpDir() + "/cache3")

	//panic(2)
}
