package map_compiler

import (
	"fmt"
	"bytes"

	//v4 "github.com/fossoreslp/go-uuid-v4"
	//"github.com/geange/gods-generic/sets/linkedhashset"
	//"github.com/geange/gods-generic/utils"
	//"github.com/geange/gods-generic/maps/linkedhashmap"
	//"github.com/zijiren233/gencontainer/lhashmap"
	"github.com/ugurcsen/gods-generic/maps/linkedhashmap"
)

// Cache for rendered objects ?
type RenderCache struct {
	// XXX a cache ?
	//RenderedObjects *linkedhashset.Set[uuid]
	//RenderedObjectsCache *linkedhashset.Set[uuid]
	//RenderedObjects linkedhashset.Set[v4.UUID]
	//RenderedObjects linkedhashmap.New[uuid, *RenderedObjectData]
	//RenderedObjectsCache *linkedhashmap.Map[uuid, *RenderedObjectData]
	RenderedObjectsCache *linkedhashmap.Map[RenderedObjectsCacheKey,
		*RenderedObjectData]
	//RenderedObjectsCache lhashmap.New[uuid, *RenderedObjectData]
}

func NewRenderCache() *RenderCache {
	fmt.Println("NewRenderCache")

	compF := func(a, b uuid) int {
		abv, _ := a.MarshalBinary()
		bbv, _ := b.MarshalBinary()
		//return bytes.Compare(bytes(a), bytes(b))
		return bytes.Compare(abv, bbv)
	}
	_ = compF

	rc := &RenderCache{
		//RenderedObjects: linkedhashset.New[uuid],
		//RenderedObjects: linkedhashset.NewWith[uuid](utils.ByteComparator),
		//RenderedObjects: linkedhashset.NewWith[uuid](bytes.Compare),
		//RenderedObjects: linkedhashset.NewWith[uuid](compF),
		//RenderedObjectsCache: linkedhashset.NewWith[uuid](compF),
		//RenderedObjects: linkedhashset.New[v4.UUID],
		//RenderedObjects: linkedhashmap.New[uuid, *RenderedObjectData](),
		//RenderedObjectsCache: linkedhashmap.New[uuid, *RenderedObjectData](),
		RenderedObjectsCache: linkedhashmap.New[RenderedObjectsCacheKey,
			*RenderedObjectData](),
	}

	return rc
}

func (rc *RenderCache) GetKeysWithUuid(uuid_ uuid) []RenderedObjectsCacheKey {
	panic("not implemented")
}

func (rc *RenderCache) HasObjectWithKey(k RenderedObjectsCacheKey) bool {
	fmt.Println("RenderCache HasObjectWithKey k:", k)
	
	_, ok := rc.RenderedObjectsCache.Get(k)
	
	return ok
}

func (rc *RenderCache) GetObjectByKey(k RenderedObjectsCacheKey) *RenderedObjectData {
	fmt.Println("RenderCache GetObjectByKey k:", k)
	
	if !rc.HasObjectWithKey(k) {
		pp("no such object; k:", k)
	}
	
	obj, _ := rc.RenderedObjectsCache.Get(k)
	
	return obj
}


func (rc *RenderCache) Clear() {
	fmt.Println("RenderCache Clear")
	
	rc.RenderedObjectsCache.Clear()
}

