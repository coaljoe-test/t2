package map_compiler

import (
	"fmt"
	"os"
	
	"github.com/spyzhov/ajson"
	v4 "github.com/fossoreslp/go-uuid-v4"
)

// XXX Apply modifiers on json level ?
func MapLoaderApplyModifiersJson(path string) string {

	fmt.Println("MapLoaderApplyModifiersJson path:", path)

	//panic("not implemented")

	data, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	//fmt.Println("data:")
	//fmt.Println(string(data))

	// Root
	json, _ := ajson.Unmarshal(data)

	//fmt.Println("json:")
	//fmt.Println(json)
	
	//nodes := json.MustKey("nodes").MustArray()

	def := applyModifiersJson(json)
	_ = def

	// XXX
	//generateMeshes(def)

	//return def
	//return defS
	// XXX
	return ""
}

func applyModifiersJson(def *ajson.Node) *ajson.Node {

	fmt.Println("applyModifiersJson def:", def)

	//panic("not implemented")

	modifiers := def.MustKey("editor").MustKey("modifiers").MustArray()
	nodes := def.MustKey("nodes").MustArray()

	for i, xdef := range modifiers {
		fmt.Println("-> i:", i)
		fmt.Printf("%#v\n", xdef)

		//xpath := sDef.MustString()

		if xdef.MustKey("type").MustString() == "array" {
			// Array modifier
			
			nodeId := xdef.MustKey("node_id").MustString()
			fmt.Println("nodeId:", nodeId)

			amount := int(xdef.MustKey("amount").MustNumeric())
			fmt.Println("amount:", amount)

			enabled := xdef.MustKey("enabled").MustBool()
			fmt.Println("enabled:", enabled)

			var nodeRec *ajson.Node

			for j, zdef := range nodes {
				_ = j
				znodeId := zdef.MustKey("_id").MustString()
				if znodeId == nodeId {
					nodeRec = zdef
					break
				}
			}

			fmt.Println("nodeRec:", nodeRec)

			if nodeRec == nil {
				pp("nodeRec is nil")
			}

			// XXX clone node
			newNodeRec := nodeRec.Clone()

			// XXX modify node
			newNodeRec.AppendObject("__cloned", ajson.BoolNode("", true))
			newNodeRec.AppendObject("__cloned_from_id", ajson.StringNode("", nodeId))
			// XXX add new id
			// ...
			id, err := v4.New()
			if err != nil {
				panic(err)
			}
			err2 := newNodeRec.MustKey("_id").SetString(id.String())

			if err2 != nil {
				panic(err2)
			}
			// XXX update position
			// newPos = ...
			

			fmt.Println("x newNodeRec:", newNodeRec)
		}
	}

	panic(2)
}
