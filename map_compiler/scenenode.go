package map_compiler

import (
	"fmt"
	//"math"

	v4 "github.com/fossoreslp/go-uuid-v4"

	"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"
	_math "kristallos.ga/rx/math"
)

// ?
// Mesh component/record ?
type Mesh struct {
	// Object / mesh id ?
	Id uuid
	// XXX path ?
	//Mesh string
	// Optional ?
	Mesh *string
	//Mesh string
	
	// Optional ?
	MeshGen *MeshGen
}

func NewMesh() *Mesh {
	id, _ := v4.New()
	return &Mesh{
		Id: id,	
	}
}

// ?
// MeshGen component/record ?
type MeshGen struct {
	Type string
	Size [3]float64

	// Optional ?
	Texture *string
	// Optional ?
	Color *[3]int
	// Optional ?
	Unshaded *bool
}

func NewMeshGen() *MeshGen {
	return &MeshGen{}
}

type SceneNode struct {
	// link ?
	Scene *Scene

	// _id ?
	Id uuid
	// _type ?
	//Type string
	
	Name string
	Description string
	NodeType string

	Pos [3]float64
	Rot [3]float64
	// Default = 1.0,1.0,1.0
	Scale [3]float64

	// optional ?
	//Mesh *Mesh

	// optional ?
	//MeshGen *MeshGen

	// optional ?
	// link (?)
	// link to scene.Meshes mesh/object/record
	// Note: mesh-sharing
	Mesh *Mesh

	// XXX Data
	//
	// ?
	//Node *MapDataNode
	//NodeData *MapDataNode

	// ?
	//MapDataMesh *MapDataMesh
	// ?
	// Mesh record from map data ?
	//Mesh *MapDataMesh
	//MeshData *MapDataMesh

	// ? MeshGen record from map data ?
	//MeshGen *MapDataMeshGen
	//MeshGenData *MapDataMeshGen
}

func NewSceneNode() *SceneNode {
	id, _ := v4.New()
	n := &SceneNode{
		Id: id,
		Scale: [3]float64{1.0, 1.0, 1.0},
	}

	return n
}

// ?
func NewMeshSceneNode() *SceneNode {
	n := NewSceneNode()
	n.NodeType = "mesh"

	// XXX ?
	//n.Mesh = NewMesh()

	return n
}

// ?
func NewMeshGenSceneNode() *SceneNode {
	n := NewSceneNode()
	n.NodeType = "mesh_gen"

	// XXX ?
	//n.MeshGen = NewMeshGen()
	
	return n
}

func  (s *SceneNode) HasMesh() bool {
	//ret := MdSceneNodeHasMesh(
	//return s.Mesh != nil
	return s.Mesh.Mesh != nil
}

func  (s *SceneNode) HasMeshGen() bool {
	//return s.MeshGen != nil
	return s.Mesh.MeshGen != nil
}

func  (s *SceneNode) SetMesh(mesh *Mesh) {
	s.Mesh = mesh
}

func  (s *SceneNode) GetBoundingBox() rx.AABB {

	fmt.Println("SceneNode GetBoundingBox")

	//panic("not implemented")

	// XXX
	node := s
	fmt.Println()
	fmt.Println("node:")
	dump(node)

	var ret rx.AABB

	if s.HasMesh() {
		mesh := s.Mesh
		//fmt.Println()
		//pdump(mesh)
		path := *mesh.Mesh
		// XXX
		xpath := "../" + path
		fmt.Println("xpath:", xpath)
		bbox := ctx.renderBackend.GetObjectBoundingBox(xpath)
		fmt.Println("bbox:", bbox)
		// min
		min := _math.Vec3{bbox[0], bbox[1], bbox[2]}
		// max
		max := _math.Vec3{bbox[3], bbox[4], bbox[5]}
		ret = rx.NewAABBFromPoints(min, max)
		//pp(2)
	} else if s.HasMeshGen() {
		//meshGen := s.MeshGen
		meshGen := s.Mesh.MeshGen
		//fmt.Println()
		//pdump(meshGen)

		if meshGen.Type == "cube" {
			size := meshGen.Size
			// min
			min := _math.Vec3{0, 0, 0}
			// max
			max := _math.Vec3{size[0], size[1], size[2]}
			ret = rx.NewAABBFromPoints(min, max)
		} else {
			pp("unknown type:", meshGen.Type)
		}
		
	} else {
		pp("no mesh or meshGen")
	}

	fmt.Println()
	fmt.Println("ret:", ret)

	return ret

}

var _ = `
func  (s *SceneNode) GetBoundingBox() rx.AABB {

	fmt.Println("SceneNode GetBoundingBox")

	//panic("not implemented")

	// XXX
	node := s.Nodes[0]
	fmt.Println()
	fmt.Println("node:")
	dump(node)

	var ret rx.AABB

	if MdSceneNodeHasMesh(s, node) {
		mesh := MdGetMeshForSceneNode(s, node)
		//fmt.Println()
		//pdump(mesh)
		path := *mesh.Mesh
		// XXX
		xpath := "../" + path
		fmt.Println("xpath:", xpath)
		bbox := ctx.renderBackend.GetObjectBoundingBox(xpath)
		fmt.Println("bbox:", bbox)
		// min
		min := _math.Vec3{bbox[0], bbox[1], bbox[2]}
		// max
		max := _math.Vec3{bbox[3], bbox[4], bbox[5]}
		ret = rx.NewAABBFromPoints(min, max)
		//pp(2)
	} else if MdSceneNodeHasMeshGen(s, node) {
		meshGen := MdGetMeshGenForSceneNode(s, node)
		//fmt.Println()
		//pdump(meshGen)

		if meshGen.Type == "cube" {
			size := meshGen.Size
			// min
			min := _math.Vec3{0, 0, 0}
			// max
			max := _math.Vec3{size[0], size[1], size[2]}
			ret = rx.NewAABBFromPoints(min, max)
		} else {
			pp("unknown type:", meshGen.Type)
		}
		
	} else {
		pp("no mesh or meshGen")
	}

	fmt.Println()
	fmt.Println("ret:", ret)

	return ret

}
`
