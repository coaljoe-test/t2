package map_compiler

import (
	"fmt"
	//"os"
	//"image"

	//"test/object_renderer/render_backend/backend"
	//c "test/object_renderer/render_backend/common"
	//"test/object_renderer/render_backend/rx_backend"

	//"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"

	
	//ren "gitlab.com/coaljoe-test/t2/rengine"
)

// XXX test
func RenderNode(mn *MapDataNode) {
	fmt.Println("RenderNode mn:", mn)

	
}

var _ = `
// Util ?
func RenderObject(defPath string) {
	fmt.Println("RenderObject defPath:", defPath)

	path := "../testdata/tree1/tree1_no_mat.glb"
	doRenderObject(path, false)

}


func doRenderObject(path string, modeDepth bool) image.Image {
	fmt.Println("doRenderObject path:", path, "modeDepth:", modeDepth)

	return doRenderObjectEx(path, modeDepth, nil)
}
`

var _ = `
// XXX out to /tmp/renderer
// XXX        /tmp/rendered/[object_id]
func doRenderObjectEx(path string, modeDepth bool, rot *[3]float32) image.Image {
	fmt.Println("doRenderObjectEx path:", path, "modeDepth:", modeDepth,
		"rot:", rot)

	// XXX this should be in backend ?

	// XXX
	{

		conf := rx.NewTestFwConf()
		//conf.ResX = 1280
		//conf.ResY = 800
		//conf.ResX = 800
		//conf.ResY = 600
		//conf.ResX = 8192
		//conf.ResY = 8192
		//conf.ResX = 10000
		//conf.ResY = 10000
		//conf.ResX = 2048
		//conf.ResY = 2048

		conf.ResX = 512
		conf.ResY = 512

		//panic(3)
	

		//app := rx.TestFwInit()
		app := rx.TestFwInitConf(conf)
		_ = app
		sce := rx.TestFwCreateDefaultScene()
		_ = sce
	
		// XXX disable debug ?
		xconf := rx.GetConf()
		_ = xconf
		//xconf.DebugDraw = false

		// ?
	
		/*
		// Iso cam
		cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		//fmt.Println(cn.Camera.Fov())
		//panic(2)
		cn.Camera.SetFov(10)


		gc := newGameCamera()
		Ctx.GameCamera = gc
		gc.setup(cn, Vec3{60 ,0, 45}, 40.0)
		*/

		rxi := rx.Rxi()
		cn := rxi.Camera
		//cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		mulV := 5.0
		cn.SetPos(Vec3{12.24745*mulV, -12.24745*mulV, 10.0*mulV})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		// XXX
		fmt.Println(cn.Camera.Fov())
		//panic(2)
		//cn.Camera.SetFov(10)
		//cn.Camera.SetFov(2)
		//cn.Camera.SetFov(4)
		//cn.Camera.SetFov(5)
		//cn.Camera.SetFov(150)

		cn.Camera.SetZnear(0.001)
		cn.Camera.SetZfar(100000.0)
		cn.Camera.SetFov(150)

		////

		// Zoom

		{
			if true {
				nativeResX := 512
				nativeResY := 512
				nativeZoom := 1.0
				_ = nativeResX
				_ = nativeResY
				_ = nativeZoom
	
				zoomV := 1.0

				scale := float64(conf.ResX)/float64(nativeResX)
				scaleZ := 1.0/scale

			
				fmt.Println("scale:", scale)
				fmt.Println("scaleZ:", scaleZ)

				//zoomV = scaleZ
				zoomV = scale
		
				cn.Camera.SetZoom(zoomV)

				//panic(2)
			}

		}
	
		////

		l1 := sce.GetNodeByName("light1")
		//l1.SetPosZ(10)
		l1.SetPos(Vec3{-2, -15, 2})

	}

	////

	var b backend.RenderBackendI

	fmt.Println("rx_backend init...")
	xb := rx_backend.NewBackend()
	xb.Init()
	fmt.Println("done init")
	b = xb

	fmt.Println(b)

	//path := "../untitled.gltf"
	//path := "../untitled.gltf"
	//path := "../testdata/tree1/tree1_no_mat.glb"
	//path := os.Args[1]
	//path := flag.Arg(0)
	fmt.Println("path:", path)
	data, err := os.ReadFile(path)


	if err != nil {
		panic(err)
	}


	t := c.NewTransform()

	if rot != nil {
		rotV := *rot
		panic(rotV)
	}

	//t.SetScale([3]float64{0.5, 0.5, 0.5})
	//t.SetScale([3]float64{2.0, 2.0, 2.0})
	//t.SetScale([3]float64{0.1, 0.1, 0.1})

	//t.SetRot([3]float64{0.0, 0.0, 45.0})

	//t.SetPos([3]float64{10.0, 0.0, 0.0})


	im := b.RenderObject(data, t)
	_ = im

	imDepth := b.RenderObjectDepth(data, t)
	_ = imDepth

	outPath := GetTmpDir() + "/out1.png"

	b.RenderObjectFile(data, t, outPath)


	outPath2 := GetTmpDir() + "/out2.png"

	b.RenderObjectDepthFile(data, t, outPath2)

	if !modeDepth {
		return im
	} else {
		return imDepth
	}
}
`
