package map_compiler

import (
	//"fmt"
)

// 2d
func drawLine(x1, y1, x2, y2 int, c [3]int) {
	// Across tiles ?
	panic("not implemented")
}

// 2d
func drawHLine(x, y, l int, c [3]int) {
        drawLine(x, y, x+l, y, c)
}

// 2d
func drawVLine(x, y, l int, c [3]int) {
        drawLine(x, y, x, y+l, c)
}

// 2d
func drawCross(x, y, size int, c [3]int) {
        drawHLine(x-size/2, y, size, c)
        drawVLine(x, y-size/2, size, c)
}
