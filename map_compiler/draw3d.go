package map_compiler

import (
	//"fmt"
)

var _ = `
// In world space
// via backend ?
func draw3dLine(p1, p2 [3]float64, color [3]int) {
	b := ctx.renderBackend

	b.Draw3dLine(p1, p2, color)
}

// In world space
// via backend ?
func draw3dGrid(pos [3]float64, w, h int, cellSize float64, color [3]int, drawOrigin bool) {
	b := ctx.renderBackend

	b.Draw3dGrid(pos, w, h, cellSize, color, drawOrigin)
}
`

// In world space
func Xdraw3dLine(p1, p2 [3]float64, color [3]int) {

	// View space to World space
	// World space to View space

	/*	
	// camera ?
	cameraNode := ?
	cameraMatrix := ?

	// Main camera ?
	mainCamera := ?
	*/
}

// In world space
func XdrawLineWS(p1, p2 [3]float64, color [3]int) {

	// View space to World space
	// World space to View space

	/*	
	// camera ?
	cameraNode := ?
	cameraMatrix := ?

	// Main camera ?
	mainCamera := ?
	*/
}

// In world space
// top-left corner of map ?
// map's origin ?
// world space origin ?
func XdrawWorldOrigin(color [3]int) {

	// View space to World space
	// World space to View space

	/*	
	// camera ?
	cameraNode := ?
	cameraMatrix := ?

	// Main camera ?
	mainCamera := ?
	*/

	//drawCross()
}

func XdrawCrossWS(pos [3]float64, size float64, color [3]int) {
	panic(2)	
}

// XXX
// util ?
func worldToScreen(px, py, pz float64) (int, int, float64) {
	
	//camera.GetWorldToScreen(pos Vec3)
	panic(2)
}

// util ?
func screenToWorld(x, y int) (float64, float64, float64) {
	// ?

	// Using this currently
	// maybe this can be done with some isometric code

	// raypick_planes.go
	var _ = `
		c := rxi.Camera
		//c.SetPos(Vec3{0, 0, 5})
		fmt.Println(c.Camera.Fov())
		fmt.Println(c.Camera.Zoom())
		//panic(2)
		println(c)
		//rayLenght := 10.0
		rayLenght := 1000.0
		//rayLenght := 100.0
		rayDir := c.Camera.CreateRay()
		ray := rayDir
		//rayDir_norm := rayDir.Norm()
		//rayStart := c.WorldPos()
		rayStart := ray.Origin
		//rayStart := Vec3{0, 0, 0}
		rayEnd := ray.Origin.Add(ray.Direction.MulScalar(rayLenght))
		//rayEnd := rayStart.Add(rayDir.MulScalar(rayLenght))
		//rayEnd := rayDir.Add(rayStart)
		//rayEnd := Vec3Zero
		//rayEnd_z := rayStart.Add(rayDir_norm.MulScalar(rayLenght))
		//rayEnd[2] = rayEnd_z[2]
		//rayEnd := c.Pos().Add(rayDir.MulScalar(rayLenght))
		//rayEnd := rayDir.MulScalar(100)
		//rayEnd.SetZ(0)
		//rayEnd := rayStart.Mul(rayDir.MulScalar(rayLenght))
		//fmt.Println(c.Pos())
		//fmt.Println(rayDir)
		fmt.Println(rayLenght, rayDir)
		fmt.Println("start:", rayStart, "\nend:  ", rayEnd)
		fmt.Println("cpos:", c.WorldPos())
		fmt.Println("crot:", c.Rot())

		// Place the object at ray's end
		//sce.GetNode("box").SetPos(rayEnd)
		//sce.GetNode("box").MoveByVec(Vec3{0, 0, -5})
		//sce.GetNode("box").SetPos(ray.Origin)
		//sce.GetNode("box").MoveByVec(Vec3{0, 0, -10})
		//sce.GetNode("box").SetPos(Vec3{4, 46, 0})
		//fmt.Println("box pos:", sce.GetNode("box").Pos())
		//fmt.Println("box pos:", sce.GetNodeByName("box").Pos())
		
		// Lock X axis
		lockAxis := true
		_ = lockAxis

		//selRay := NewRay(rayStart, rayEnd)
		selRay := NewRay(ray.Origin, ray.Direction)
		// Do scene rayquery
		rc := rx.NewRaycaster()
		rc.TestRayScene(selRay)
		//hitInfo := rc.TestRayGround(selRay)
		
		xyPlane := NewPlane(Vec3{0, 0, 1}, 0)
		yzPlane := NewPlane(Vec3{1, 0, 0}, 0)
		_ = xyPlane
		_ = yzPlane

		plane := xyPlane
		
		if true && lockAxis {
			//fmt.Println("ray.Origin:", ray.Origin)
			planeNormal := ray.Direction.DirectionTo(ray.Origin)
			//planeNormal := ray.Origin.DirectionTo(ray.Direction)
			//planeNormal := Vec3Zero.DirectionTo(ray.Origin)
			//planeNormal := Vec3Zero.DirectionTo(cn.Pos())
			//planeNormal := cn.Pos().DirectionTo(Vec3Zero)
			//planeNormal = planeNormal.Negate()
			planeNormal.SetX(0)
			//panic(planeNormal)
			plane.Normal = planeNormal
		}
		
		hitInfo := rx.TestRayPlane(selRay, plane)
		

		
		cubePos := Vec3Zero

		if hitInfo.Hit {
			//sce.GetNodeByName("box#2").SetPos(hitInfo.Position)
			if !lockAxis {
				cubePos = hitInfo.Position
			} else {
				xpos := hitInfo.Position
				xpos.SetY(0)
				xpos.SetZ(0)
				cubePos = xpos
			}
		}
	`

	panic(2)
}

// ?
func screenToWorldRayCast(px, py, pz float64) (int, int, float64) {
	// ?
	
	panic(2)
}