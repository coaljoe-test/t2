package map_compiler

import (
	"fmt"
	"os"
	//_path "path"
	
	"github.com/spyzhov/ajson"
	v4 "github.com/fossoreslp/go-uuid-v4"
)

type MapLoader struct {

}

func NewMapLoader() *MapLoader {
	ml := &MapLoader{}

	return ml
}

// Load map scene from file ?
//func (ml *MapLoader) LoadMapScene(path string) *Scene {
func (ml *MapLoader) LoadScene(path string) *Scene {

	//fmt.Println("MapLoader LoadMapScene path:", path)
	fmt.Println("MapLoader LoadScene path:", path)

	data, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	//fmt.Println("data:")
	//fmt.Println(string(data))

	// Root
	json, _ := ajson.Unmarshal(data)

	//fmt.Println("json:")
	//fmt.Println(json)
	
	//nodes := json.MustKey("nodes").MustArray()

	//def := parseScene(json)

	// XXX
	//generateMeshes(def)

	//scene := NewScene()

	//scene := ml.loadSceneFromMapDataScene(def)
	//scene := ml.decodeSceneFromMapDataScene(def)

	scene := ml.decodeScene(json)

	return scene
}

var _ = `
// Load map scene from map data ?
// load/parse/decode ?
//func (ml *MapLoader) loadSceneFromMapDataScene(def *MapDataScene) *Scene {
func (ml *MapLoader) decodeSceneFromMapDataScene(def *MapDataScene) *Scene {

	//fmt.Println("MapLoader loadSceneFromMapDataScene def:", def)
	fmt.Println("MapLoader decodeSceneFromMapDataScene def:", def)

	scene := NewScene()

	for _, nodeDef := range def.Nodes {
		//n := NewSceneNode()
		n := ml.decodeSceneNodeFromMapDataNode(def, nodeDef)
		scene.AddNode(n)
	}

	return scene
}
`

var _ = `
// Load scene node from map data ?
//func (ml *MapLoader) decodeSceneNodeFromMapDataNode(def *MapDataNode) *SceneNode {
func (ml *MapLoader) decodeSceneNodeFromMapDataNode(
	sceneDef *MapDataScene,def *MapDataNode) *SceneNode {

	//fmt.Println("MapLoader decodeSceneNodeFromMapDataNode def:", def)
	fmt.Println("MapLoader decodeSceneNodeFromMapDataNode sceneDef:", sceneDef, "def:", def)

	node := NewSceneNode()

	// XXX
	node.Node = def // ?

	//if MdSceneNodeHasMesh(s, node) {
	if MdSceneNodeHasMesh(sceneDef, def) {
		//mesh := MdGetMeshForSceneNode(s, node)
		mesh := MdGetMeshForSceneNode(sceneDef, def)

		// XXX ?
		node.Mesh = mesh
	}
	//node.Mesh = def.Mesh
	//node.MeshGen = def.MeshGen

	return node
}
`

// XXX
// returns ok, mesh (?)
func (ml *MapLoader) lookupMeshForSceneNode(s *Scene, nodeRec *ajson.Node) (bool, *Mesh) {

	fmt.Println("_ lookupMeshForSceneNode s:", s, "nodeRec:", nodeRec)

	meshId := nodeRec.MustKey("mesh_id").MustString()
	
	fmt.Println("meshId:", meshId)

	//panic(2)

	// Scan meshes
	for i, mesh := range s.Meshes {
		fmt.Println("-> i:", i)

		if meshId == mesh.Id.String() {
			fmt.Println("have...")
		
			return true, mesh
		}
	}

	return false, nil
}

//func parseScene(def *ajson.Node) *MapDataScene {
func (ml *MapLoader) decodeScene(def *ajson.Node) *Scene {

	fmt.Println("_ decodeScene def:", def)

	//s := NewMapDataScene()
	s := NewScene()

	// XXX
	
	meshesRec := def.MustKey("meshes").MustArray()

	fmt.Println("meshesRec:", meshesRec)
	
	fmt.Println("meshes...")

	for i, meshRec := range meshesRec {
		fmt.Println("-> i:", i)

		// Mesh rec. / mesh obj
		//ok, meshRec := parseMesh(mesh)
		ok, mesh := ml.decodeMesh(meshRec)
		if !ok {
			fmt.Println("skip")
			continue
		}
		
		s.Meshes = append(s.Meshes, mesh)
	}

	//pdump(s.Meshes)

	// XXX

	//nodes := def.MustArray()
	//nodes := def
	nodesRec := def.MustKey("nodes").MustArray()

	fmt.Println("nodesRec:", nodesRec)

	// XXX
	
	fmt.Println("nodes...")
	
	for i, nodeRec := range nodesRec {
		fmt.Println("-> i:", i)

		//ok, mn := parseNode(node)
		ok, mn := ml.decodeNode(nodeRec)
		if !ok {
			fmt.Println("skip")
			continue
		}

		// XXX
		ok, mesh := ml.lookupMeshForSceneNode(s, nodeRec)
		if ok {
			mn.Mesh = mesh
		}

		//pdump(mn)
		
		s.Nodes = append(s.Nodes, mn)
	}

	/*
	// XXX lookup meshes / links ?
	// Extra ?
	{
		fmt.Println("lookup nodes...")
	
		for i, node := s.Nodes {
			fmt.Println("-> i:", i)

			mesh := ...
			getMeshById(...)
			meshId := ?
		}
	}
	*/

	return s
}

// TODO: add checks ?
//func parseNode(def *ajson.Node) (bool, *MapDataNode) {
func (ml *MapLoader) decodeNode(def *ajson.Node) (bool, *SceneNode) {

	fmt.Println("_ decodeNode def:", def)

	if !def.HasKey("_id") {
		return false, nil
	}

	//mn := NewMapDataNode()
	mn := NewSceneNode()
	s := def.MustKey("_id").MustString()
	id, err := v4.Parse(s)
	if err != nil {
		panic(err)
	}
	mn.Id = id
	//mn.Type = def.MustKey("_type").MustString()
	mn.Name = def.MustKey("name").MustString()
	mn.NodeType = def.MustKey("node_type").MustString()
	posX := def.MustKey("pos").MustArray()[0].MustNumeric()
	posY := def.MustKey("pos").MustArray()[1].MustNumeric()
	posZ := def.MustKey("pos").MustArray()[2].MustNumeric()
	mn.Pos[0] = posX
	mn.Pos[1] = posY
	mn.Pos[2] = posZ

	// XXX optional
	if def.HasKey("rot") {
		rotX := def.MustKey("rot").MustArray()[0].MustNumeric()
		rotY := def.MustKey("rot").MustArray()[1].MustNumeric()
		rotZ := def.MustKey("rot").MustArray()[2].MustNumeric()
		mn.Rot[0] = rotX
		mn.Rot[1] = rotY
		mn.Rot[2] = rotZ
	}

	/*
	if def.HasKey("mesh") {
		v := def.MustKey("mesh").MustString()
		mn.Mesh = &v
	}
	*/

	var _ = `
	if def.HasKey("mesh_id") {
		//v := def.MustKey("mesh_id").MustString()
		//mn.MeshId = &v
		s := def.MustKey("mesh_id").MustString()
		id, err := v4.Parse(s)
		if err != nil {
			panic(err)
		}
		mn.MeshId = &id
	}
	`

	/*
	// XXX
	if def.HasKey("mesh_gen") {

			// XXX a check ?
			if def.HasKey("mesh") {
				panic("both mesh and mesh_gen keys are presented in a node")
			}

			//xdef := def.MustKey("mesh_gen").MustObject()
			xdef := def.MustKey("mesh_gen")

			rec := &MapDataMeshGen{}
			rec.Type = xdef.MustKey("type").MustString()

			sizeX := xdef.MustKey("size").MustArray()[0].MustNumeric()
			sizeY := xdef.MustKey("size").MustArray()[1].MustNumeric()
			sizeZ := xdef.MustKey("size").MustArray()[2].MustNumeric()
			rec.Size[0] = sizeX
			rec.Size[1] = sizeY
			rec.Size[2] = sizeZ

			mn.MeshGen = rec
	}
	*/	

	return true, mn
}

// TODO: add checks ?
//func parseMesh(def *ajson.Node) (bool, *MapDataMesh) {
func (ml *MapLoader) decodeMesh(def *ajson.Node) (bool, *Mesh) {

	fmt.Println("_ parseMesh def:", def)

	if !def.HasKey("_id") {
		return false, nil
	}

	//mesh := &MapDataMesh{}
	mesh := &Mesh{}
	s := def.MustKey("_id").MustString()
	id, err := v4.Parse(s)
	if err != nil {
		panic(err)
	}
	mesh.Id = id

	if def.HasKey("mesh") {
		v := def.MustKey("mesh").MustString()
		mesh.Mesh = &v
	}
	
	// XXX
	if def.HasKey("mesh_gen") {

		// XXX a check ?
		if def.HasKey("mesh") {
			panic("both mesh and mesh_gen keys are presented in a mesh")
		}

		//xdef := def.MustKey("mesh_gen").MustObject()
		xdef := def.MustKey("mesh_gen")

		//rec := &MapDataMeshGen{}
		rec := &MeshGen{}
		rec.Type = xdef.MustKey("type").MustString()

		sizeX := xdef.MustKey("size").MustArray()[0].MustNumeric()
		sizeY := xdef.MustKey("size").MustArray()[1].MustNumeric()
		sizeZ := xdef.MustKey("size").MustArray()[2].MustNumeric()
		rec.Size[0] = sizeX
		rec.Size[1] = sizeY
		rec.Size[2] = sizeZ

		// XXX optional
		if xdef.HasKey("texture") {
			v := xdef.MustKey("texture").MustString()
			rec.Texture = &v
		}
		
		// XXX optional
		if xdef.HasKey("color") {
			a := xdef.MustKey("color").MustArray()
			colorR := int(a[0].MustNumeric())
			colorG := int(a[1].MustNumeric())
			colorB := int(a[2].MustNumeric())
			v := [3]int{colorR, colorG, colorB}
			rec.Color = &v
		}

		// XXX optional
		if xdef.HasKey("unshaded") {
			v := xdef.MustKey("unshaded").MustBool()
			rec.Unshaded = &v
		}

		mesh.MeshGen = rec
	}	

	return true, mesh
}


