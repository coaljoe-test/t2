package map_compiler

import (
	"fmt"
	"image"
	"strconv"
)

// Cache data ?
// Can you cache oriented / transformed objects ?
// Cache for objects ?
// a cache record
type RenderedObjectData struct {
	// Technically not needed
	// a copy ?
	//uuid uuid
	uuid *uuid
	// Optional
	name *string
	rot [3]float64
	// ?
	// default = 1.0,1.0,1.0
	scale [3]float64
	im image.Image
	// GetTextureDataDepth16ConvAsImage
	// (*image.Gray16)
	// Depth16Conv
	// 16 bit converted depth image
	imDepth image.Image
}

// Creates new rec with default values ?
func NewRenderedObjectData() *RenderedObjectData {
	rec := &RenderedObjectData{
		// Default value ?
		scale: [3]float64{1, 1, 1},
	}
	
	return rec
}

// a cache key
type RenderedObjectsCacheKey struct {
	// Node id ?
	// Object id ?
	uuid uuid
	rot [3]float64
	// ?
	// default = 1.0,1.0,1.0
	scale [3]float64
}

// Creates new empty key with default values ?
func NewRenderedObjectsCacheKey() RenderedObjectsCacheKey {
	k := RenderedObjectsCacheKey{
		// Default value ?
		scale: [3]float64{1, 1, 1},
	}
	
	return k
}

func (k *RenderedObjectsCacheKey) ToString() string {
	/*
	s := fmt.Sprintf("%v_%f,%f,%f_%f,%f,%f",
		k.uuid, k.rot[0], k.rot[1], k.rot[2],
		k.scale[0], k.scale[1], k.scale[2])
	*/
	s := fmt.Sprintf("%v_%s,%s,%s_%s,%s,%s",
		k.uuid,
		strconv.FormatFloat(k.rot[0], 'f', -1, 64),
		strconv.FormatFloat(k.rot[1], 'f', -1, 64),
		strconv.FormatFloat(k.rot[2], 'f', -1, 64),
		strconv.FormatFloat(k.scale[0], 'f', -1, 64),
		strconv.FormatFloat(k.scale[1], 'f', -1, 64),
		strconv.FormatFloat(k.scale[2], 'f', -1, 64))
	
	return s
}

//func (k *RenderedObjectsCacheKey) String() string {
func (k RenderedObjectsCacheKey) String() string {
	//return k.ToString()
	s := fmt.Sprintf("Key<uuid=%v rot=%s,%s,%s scale=%s,%s,%s>",
		k.uuid,
		strconv.FormatFloat(k.rot[0], 'f', -1, 64),
		strconv.FormatFloat(k.rot[1], 'f', -1, 64),
		strconv.FormatFloat(k.rot[2], 'f', -1, 64),
		strconv.FormatFloat(k.scale[0], 'f', -1, 64),
		strconv.FormatFloat(k.scale[1], 'f', -1, 64),
		strconv.FormatFloat(k.scale[2], 'f', -1, 64))
		
	return s
}

