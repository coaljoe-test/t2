package map_compiler

import (
	"fmt"
	"math"
	"slices"

	//"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"
	//_math "kristallos.ga/rx/math"
)

type Scene struct {
	Nodes []*SceneNode
	Meshes []*Mesh
}

func NewScene() *Scene {
	s := &Scene{
		Nodes: make([]*SceneNode, 0),
		Meshes: make([]*Mesh, 0),
	}

	return s
}

func  (s *Scene) HasNode(n *SceneNode) bool {
	return slices.Contains(s.Nodes, n)
}

func (s *Scene) AddNode(n *SceneNode) {
	if s.HasNode(n) {
		pp("already have node. n:", n)
	}
	s.Nodes = append(s.Nodes, n)

	// XXX
	n.Scene = s
}

func (s *Scene) RemoveNode(n *SceneNode) {
	panic("not implemented")
}

func  (s *Scene) HasMesh(mesh *Mesh) bool {
	return slices.Contains(s.Meshes, mesh)
}

func (s *Scene) AddMesh(mesh *Mesh) {
	if s.HasMesh(mesh) {
		pp("already have mesh. mesh:", mesh)
	}
	s.Meshes = append(s.Meshes, mesh)
}

func (s *Scene) RemoveMesh(mesh *Mesh) {
	panic("not implemented")
}

// XXX ?
func (s *Scene) LoadMesh(path string) {

}

// XXX ?
func (s *Scene) AddNewMeshWithPath(path string) *Mesh {
	mesh := NewMesh()
	mesh.Mesh = &path

	s.AddMesh(mesh)

	return mesh
}

func (s *Scene) EvaluateBounds() {

	fmt.Println("Scene EvaluateSceneBounds")

	minX := math.MaxFloat32
	maxX := -math.MaxFloat32

	minY := math.MaxFloat32
	maxY := -math.MaxFloat32

	//for i, n := range mr.md.Nodes {
	for i, n := range s.Nodes {
		fmt.Println("-> i:", i)

		x := n.Pos[0]
		y := n.Pos[1]

		if x > maxX {
			maxX = x
		}
		if x < minX {
			minX = x
		}
		if y > maxY {
			maxY = y
		}
		if y < minY {
			minY = y
		}
	}

	fmt.Println("minX:", minX)
	fmt.Println("maxX:", maxX)
	fmt.Println("minY:", maxY)
	fmt.Println("maxY:", maxY)
}

func (s *Scene) EvaluateBounds2() {
	panic("not implemented")
}


/*
func  (s *Scene) GetBoundingBox() rx.AABB {

	fmt.Println("Scene GetBoundingBox")

	//panic("not implemented")

	// XXX
	node := s.Nodes[0]
	fmt.Println()
	fmt.Println("node:")
	dump(node)

	var ret rx.AABB

	if node.HasMesh() {
		mesh := MdGetMeshForSceneNode(s, node)
		//fmt.Println()
		//pdump(mesh)
		path := *mesh.Mesh
		// XXX
		xpath := "../" + path
		fmt.Println("xpath:", xpath)
		bbox := ctx.renderBackend.GetObjectBoundingBox(xpath)
		fmt.Println("bbox:", bbox)
		// min
		min := _math.Vec3{bbox[0], bbox[1], bbox[2]}
		// max
		max := _math.Vec3{bbox[3], bbox[4], bbox[5]}
		ret = rx.NewAABBFromPoints(min, max)
		//pp(2)
	} else if node.HasMeshGen() {
		meshGen := MdGetMeshGenForSceneNode(s, node)
		//fmt.Println()
		//pdump(meshGen)

		if meshGen.Type == "cube" {
			size := meshGen.Size
			// min
			min := _math.Vec3{0, 0, 0}
			// max
			max := _math.Vec3{size[0], size[1], size[2]}
			ret = rx.NewAABBFromPoints(min, max)
		} else {
			pp("unknown type:", meshGen.Type)
		}
		
	} else {
		pp("no mesh or meshGen")
	}

	fmt.Println()
	fmt.Println("ret:", ret)

	return ret

}
*/
