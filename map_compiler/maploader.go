package map_compiler

import (
	"fmt"
	"os"
	_path "path"
	
	"github.com/spyzhov/ajson"
	v4 "github.com/fossoreslp/go-uuid-v4"
)

func LoadMapJson(path string) *MapData {
	fmt.Println("LoadMapJson path:", path)

	data, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	baseDir := _path.Dir(path)

	//fmt.Println("data:")
	//fmt.Println(string(data))

	// Root
	json, _ := ajson.Unmarshal(data)

	//fmt.Println("json:")
	//fmt.Println(json)
	
	//nodes := json.MustKey("nodes").MustArray()
	scenes := json.MustKey("scenes").MustArray()

	//fmt.Println("nodes:", nodes)
	fmt.Println("scenes:", scenes)

	scenesObj := json.MustKey("scenes")
	_ = scenesObj

	// XXX

	md := NewMapData()

	fmt.Println("load scenes...")

	for i, sDef := range scenes {
		fmt.Println("-> i:", i)
		fmt.Printf("%#v\n", sDef)

		//s := parseScene(sDef)
		xpath := sDef.MustString()
		xpath2 := baseDir + "/" + xpath
		s := LoadMapSceneJson(xpath2)
		
		md.Scenes = append(md.Scenes, s)
	}

	return md
}

// Load map scene from file ?
func LoadMapSceneJson(path string) *MapDataScene {
	fmt.Println("LoadMapSceneJson path:", path)

	data, err := os.ReadFile(path)

	if err != nil {
		panic(err)
	}

	//fmt.Println("data:")
	//fmt.Println(string(data))

	// Root
	json, _ := ajson.Unmarshal(data)

	//fmt.Println("json:")
	//fmt.Println(json)
	
	//nodes := json.MustKey("nodes").MustArray()

	def := parseScene(json)

	// XXX
	//generateMeshes(def)

	return def
}

func parseScene(def *ajson.Node) *MapDataScene {
	fmt.Println("parseScene def:", def)

	s := NewMapDataScene()

	//nodes := def.MustArray()
	//nodes := def
	nodes := def.MustKey("nodes").MustArray()

	fmt.Println("nodes:", nodes)

	// XXX
	
	fmt.Println("nodes...")
	
	for i, node := range nodes {
		fmt.Println("-> i:", i)

		ok, mn := parseNode(node)
		if !ok {
			fmt.Println("skip")
			continue
		}
		
		s.Nodes = append(s.Nodes, mn)
	}
	
	// XXX
	
	meshes := def.MustKey("meshes").MustArray()

	fmt.Println("meshes:", meshes)
	
	fmt.Println("meshes...")

	for i, mesh := range meshes {
		fmt.Println("-> i:", i)

		// Mesh rec. / mesh obj
		ok, meshRec := parseMesh(mesh)
		if !ok {
			fmt.Println("skip")
			continue
		}
		
		s.Meshes = append(s.Meshes, meshRec)
	}

	return s
}

// TODO: add checks ?
func parseNode(def *ajson.Node) (bool, *MapDataNode) {
	fmt.Println("parseNode def:", def)

	if !def.HasKey("_id") {
		return false, nil
	}

	mn := NewMapDataNode()
	s := def.MustKey("_id").MustString()
	id, err := v4.Parse(s)
	if err != nil {
		panic(err)
	}
	mn.Id = id
	mn.Type = def.MustKey("_type").MustString()
	mn.Name = def.MustKey("name").MustString()
	mn.NodeType = def.MustKey("node_type").MustString()
	posX := def.MustKey("pos").MustArray()[0].MustNumeric()
	posY := def.MustKey("pos").MustArray()[1].MustNumeric()
	posZ := def.MustKey("pos").MustArray()[2].MustNumeric()
	mn.Pos[0] = posX
	mn.Pos[1] = posY
	mn.Pos[2] = posZ

	// XXX optional
	if def.HasKey("rot") {
		rotX := def.MustKey("rot").MustArray()[0].MustNumeric()
		rotY := def.MustKey("rot").MustArray()[1].MustNumeric()
		rotZ := def.MustKey("rot").MustArray()[2].MustNumeric()
		mn.Rot[0] = rotX
		mn.Rot[1] = rotY
		mn.Rot[2] = rotZ
	}

	/*
	if def.HasKey("mesh") {
		v := def.MustKey("mesh").MustString()
		mn.Mesh = &v
	}
	*/
	
	if def.HasKey("mesh_id") {
		//v := def.MustKey("mesh_id").MustString()
		//mn.MeshId = &v
		s := def.MustKey("mesh_id").MustString()
		id, err := v4.Parse(s)
		if err != nil {
			panic(err)
		}
		mn.MeshId = &id
	}

	/*
	// XXX
	if def.HasKey("mesh_gen") {

			// XXX a check ?
			if def.HasKey("mesh") {
				panic("both mesh and mesh_gen keys are presented in a node")
			}

			//xdef := def.MustKey("mesh_gen").MustObject()
			xdef := def.MustKey("mesh_gen")

			rec := &MapDataMeshGen{}
			rec.Type = xdef.MustKey("type").MustString()

			sizeX := xdef.MustKey("size").MustArray()[0].MustNumeric()
			sizeY := xdef.MustKey("size").MustArray()[1].MustNumeric()
			sizeZ := xdef.MustKey("size").MustArray()[2].MustNumeric()
			rec.Size[0] = sizeX
			rec.Size[1] = sizeY
			rec.Size[2] = sizeZ

			mn.MeshGen = rec
	}
	*/	

	return true, mn
}

// TODO: add checks ?
func parseMesh(def *ajson.Node) (bool, *MapDataMesh) {
	fmt.Println("parseMesh def:", def)

	if !def.HasKey("_id") {
		return false, nil
	}

	mesh := &MapDataMesh{}
	s := def.MustKey("_id").MustString()
	id, err := v4.Parse(s)
	if err != nil {
		panic(err)
	}
	mesh.Id = id

	if def.HasKey("mesh") {
		v := def.MustKey("mesh").MustString()
		mesh.Mesh = &v
	}
	
	// XXX
	if def.HasKey("mesh_gen") {

		// XXX a check ?
		if def.HasKey("mesh") {
			panic("both mesh and mesh_gen keys are presented in a mesh")
		}

		//xdef := def.MustKey("mesh_gen").MustObject()
		xdef := def.MustKey("mesh_gen")

		rec := &MapDataMeshGen{}
		rec.Type = xdef.MustKey("type").MustString()

		sizeX := xdef.MustKey("size").MustArray()[0].MustNumeric()
		sizeY := xdef.MustKey("size").MustArray()[1].MustNumeric()
		sizeZ := xdef.MustKey("size").MustArray()[2].MustNumeric()
		rec.Size[0] = sizeX
		rec.Size[1] = sizeY
		rec.Size[2] = sizeZ

		// XXX optional
		if xdef.HasKey("texture") {
			v := xdef.MustKey("texture").MustString()
			rec.Texture = &v
		}
		
		// XXX optional
		if xdef.HasKey("color") {
			a := xdef.MustKey("color").MustArray()
			colorR := int(a[0].MustNumeric())
			colorG := int(a[1].MustNumeric())
			colorB := int(a[2].MustNumeric())
			v := [3]int{colorR, colorG, colorB}
			rec.Color = &v
		}

		// XXX optional
		if xdef.HasKey("unshaded") {
			v := xdef.MustKey("unshaded").MustBool()
			rec.Unshaded = &v
		}

		mesh.MeshGen = rec
	}	

	return true, mesh
}

//func parseNode(pa

//func makeNode(

// Post load ?
// Generate meshes for scene ?
// This should happend after the Init ?
/*
func generateMeshes(s *MapDataScene) {
	fmt.Println("generateMeshes")

	meshNodes := MdGetSceneNodesWithType(s, "mesh")

	for i, mn := range meshNodes {
		fmt.Println("-> i:", i)

		fmt.Println("mn:", mn)
		
		dump(mn)

		if mn.MeshGen != nil {
			pp(2)

			// Generate mesh, with rx_backend ?
		}
	}

	pp(2)
}
*/
