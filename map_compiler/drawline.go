package map_compiler

import (
	"cmp"
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"
	"slices"
	
	"github.com/StephaneBunel/bresenham"
)

type Line struct {
	p1 image.Point
	p2 image.Point
}

// LenSq
func pointMagnitudeSq(p image.Point) int {
	return p.X*p.X + p.Y*p.Y
}

// Split / reconstruct / expand path into multiple lines (array)
// with p1 - p2 coords
func pathToLines(pathPoints []image.Point) []Line {
	res := make([]Line, 0)
	for i := 0; i < len(pathPoints)-1; i++ {
		p1 := pathPoints[i]
		p2 := pathPoints[i+1]
		l := Line{p1, p2}
		
		res = append(res, l)
	}

	return res
}

// Test join / proccess path lines
// Sorts min -> max
func JoinLinesTest(lines []Line) []Line {
	fmt.Println("JoinLinesTest lines:", lines)

	// Path split points
	// Path points ?
	pathPoints := make([]image.Point, 0)
	uniqPathPoints := make([]image.Point, 0)

	for _, x := range lines {
		pathPoints = append(pathPoints, x.p1)
		pathPoints = append(pathPoints, x.p2)
	}

	fmt.Println("pathPoints:", pathPoints)
	
	/*
	for _, x := range lines {
		slices.Contains(x, uniqPathPoints)
		pathPoints = append(pathPoints, x.p1)
		pathPoints = append(pathPoints, x.p2)
	}
	*/

	uniqPathPoints = slices.Clone(pathPoints)
	//slices.Sort(uniqPathPoints)
	// Sort by magnitude ?
	slices.SortFunc(uniqPathPoints, func(a, b image.Point) int {
		v1 := pointMagnitudeSq(a)
		v2 := pointMagnitudeSq(b)
		return cmp.Compare(v1, v2)
	})
	fmt.Println("uniqPathPoints:", uniqPathPoints)
	fmt.Println("uniqPathPoints:", uniqPathPoints)

	z := slices.Clone(uniqPathPoints)
	fmt.Println("z:", z)
	z = slices.CompactFunc(z, func(a, b image.Point) bool {
		return a == b
	})
	fmt.Println("z:", z)
	
	//uniqPathPoints = slices.Compact(pathPoints)
	uniqPathPoints = slices.Compact(uniqPathPoints)
	
	fmt.Println("uniqPathPoints:", uniqPathPoints)

	procPath := pathToLines(uniqPathPoints)
	fmt.Println("procPath:", procPath)

	//pp(2)

	//return nil
	return procPath
}

// ? Split line into rectangular tile segments ?
// Mostly it just returns continuous line segments,
// that can be drawn (to/as) a straight line
// -> []Line (line segments)
func SplitLineIntoTileSegments(x1, y1, x2, y2 int, tileSize int) (int, int, []Line) {

	fmt.Println("SplitLineIntoTileSegments x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)

	ret := make([]Line, 0)
	_ = ret

	// module length over x coord ?
	lengthXCoord := math.Abs(float64(x2) - float64(x1))
	lengthYCoord := math.Abs(float64(y2) - float64(y1))

	fmt.Println("lengthXCoord:", lengthXCoord)
	fmt.Println("lengthYCoord:", lengthYCoord)

	numSplitsX := int(math.Ceil(float64(lengthXCoord) / float64(tileSize)))
	numSplitsY := int(math.Ceil(float64(lengthYCoord) / float64(tileSize)))

	fmt.Println("numSplitsX:", numSplitsX)
	fmt.Println("numSplitsY:", numSplitsY)

	sx := numSplitsX
	sy := numSplitsY
	_ = sx
	_ = sy

	// X angle
	//xAngle := AngleBetweenPoints(0, 0, x2, y2)
	xAngle := AngleBetweenPoints(x1, y1, x2, y2)
	fmt.Println("xAngle:", xAngle)
	a := Radians(xAngle)

	fmt.Println("tileSize:", tileSize)
	xB := tileSize
	fmt.Println("xB:", xB)
	// Hypot ?
	xC := float64(xB) / math.Cos(a)
	fmt.Println("xC:", xC)
	xA := math.Sqrt(math.Pow(float64(xC), 2) - math.Pow(float64(xB), 2))
	fmt.Println("xA:", xA)
		
	//xCPoint := 
	//fmt.Println("xCPoint:", xCPoint)

	// XXX lerp line
	totalLineLength := DistanceTo(x1, y1, x2, y2)
	// Lerp line length
	lineLengthEqualSegments := DistanceTo(x1, y1, x2, y2)
	
	fmt.Println("totalLineLength:", totalLineLength)
	fmt.Println("lineLengthEqualSegments:", lineLengthEqualSegments)
	

	// XXX
	segmentLength := xC
	fmt.Println("segmentLength:", segmentLength)
	testNumSegments := totalLineLength / segmentLength
	fmt.Println("testNumSegments:", testNumSegments)
	testNumSegmentsi := int(testNumSegments)
	fmt.Println("testNumSegmentsi:", testNumSegmentsi)
	
	// XXX
	// 0..tileSize
	// Final line (rest)
	// Final line segment
	lineRestLength := totalLineLength - (segmentLength * float64(testNumSegmentsi))
	fmt.Println("lineRestLength:", lineRestLength)
	
	// Rest - final segment
	drawRest := false
	_ = drawRest
	if lineRestLength > 0 {
		drawRest = true
	}
	
	fmt.Println("drawRest:", drawRest)
	
	//pp(2)

	// XXX
	// 0..tileSize
	xOffset := x1 % tileSize
	fmt.Println("xOffset:", xOffset)
	// 0..tileSize
	yOffset := y1 % tileSize
	fmt.Println("yOffset:", yOffset)

	xSegmentLength := segmentLength
	fmt.Println("xSegmentLength:", xSegmentLength)
	//xB2 := tileSize
	//fmt.Println("xB2:", xB2)
	xA2 := tileSize
	fmt.Println("xA2:", xA2)
	// Hypot ?
	//xC2 := float64(xB2) / math.Cos(a)
	xC2 := float64(xA2) / math.Sin(a)
	fmt.Println("xC2:", xC2)
	xB2 := math.Sqrt(math.Pow(float64(xC2), 2) - math.Pow(float64(xA2), 2))
	fmt.Println("xB2:", xB2)
	ySegmentLength := xC2
	fmt.Println("ySegmentLength:", ySegmentLength)

	//pp(2)

	// ?
	lerpStep := 1.0/numSplitsX
	fmt.Println("lerpStep:", lerpStep)

	if true {
	fmt.Println("cycle points...")
	
	//prevX := int(0)
	//prevY := int(0)
	prevX := x1
	prevY := y1
	for i := 0; i < testNumSegmentsi; i++ {
		fmt.Println("-> i:", i)

		px := prevX
		py := prevY

		a := Radians(xAngle)
		//d := segmentLength
		//d := segmentLength - float64(xOffset)
		d := xSegmentLength - float64(xOffset)
		px1 := int(math.Round(float64(px) + (d * math.Cos(a))))
		py1 := int(math.Round(float64(py) + (d * math.Sin(a))))

		fmt.Println("px:", px)
		fmt.Println("py:", py)
		fmt.Println("px1:", px1)
		fmt.Println("py1:", py1)

		// XXX
		ret = append(ret, Line{
			p1: image.Point{px, py},
			p2: image.Point{px1, py1},
		})

		// XXX
		prevX = px1
		prevY = py1
	}

	/*
	if drawRest {
		// XXX
		ret = append(ret, Line{
			p1: image.Point{prevX, prevY},
			p2: image.Point{x2, y2},
		})
	}
	*/
	
	}

	// Test Y
	{
	if true {
	fmt.Println("cycle y points...")
	
	prevX := x1
	prevY := y1
	for i := 0; i < numSplitsY-1; i++ {
		fmt.Println("-> i:", i)

		px := prevX
		py := prevY

		a := Radians(xAngle)
		//d := segmentLength
		d := ySegmentLength - float64(yOffset)
		px1 := int(math.Round(float64(px) + (d * math.Cos(a))))
		py1 := int(math.Round(float64(py) + (d * math.Sin(a))))

		fmt.Println("px:", px)
		fmt.Println("py:", py)
		fmt.Println("px1:", px1)
		fmt.Println("py1:", py1)

		// XXX
		ret = append(ret, Line{
			p1: image.Point{px, py},
			p2: image.Point{px1, py1},
		})

		// XXX
		prevX = px1
		prevY = py1
	}
	}
	}

	if true {
		res := JoinLinesTest(ret)
		_ = res
		//pp(2)

		// XXX replace
		ret = res
	}

	
	if true {
	if drawRest {
		fmt.Println("add the rest line...")
	
		prevLine := ret[len(ret)-1]
		prevX := prevLine.p2.X
		prevY := prevLine.p2.Y
		// XXX
		ret = append(ret, Line{
			p1: image.Point{prevX, prevY},
			p2: image.Point{x2, y2},
		})
	}
	}

	if false {
	for i := 0; i < testNumSegmentsi; i++ {
		fmt.Println("-> i:", i)
		j := float64(i)/float64(testNumSegmentsi)
		fmt.Println("j:", j)

		t := j
		px := Lerp(float64(x1), float64(x2), t)
		py := Lerp(float64(y1), float64(y2), t)

		fmt.Println("px:", px)
		fmt.Println("py:", py)
	}
	}
	

	if false {
	for i := 0; i < numSplitsX; i++ {
		fmt.Println("-> i:", i)
		j := float64(i)/float64(numSplitsX)
		fmt.Println("j:", j)

		t := j
		px := Lerp(float64(x1), float64(x2), t)
		py := Lerp(float64(y1), float64(y2), t)

		fmt.Println("px:", px)
		fmt.Println("py:", py)
	}
	}
	
	
	//pp(2)

	/*
	base := tileSize
	c := ?
	hypot := ?
	// Origin to c...
	lineTo([0,0], [tileSize*step, c])
	*/

	res := Line{
		// Origin
		p1: image.Point{0, 0},
		// C / top point
		//p2: image.Point{0, 0},
		p2: image.Point{50, 50},
	}
	_ = res

	// XXX
	xret := make([]Line, 0)
	xret = append(xret, res)
	xret = append(xret, Line{
		p1: image.Point{50, 50},
		p2: image.Point{100, 50},
	})

	return sx, sy, ret
	//return sx, sy, xret
}

// -> rx, ry, rw, rh
func tileToRectCoords(x, y, tileSize int) (int, int, int, int) {
	rx := x*tileSize
	ry := y*tileSize
	rw := tileSize
	rh := tileSize
	
	return rx, ry, rw, rh
}

// XXX
// ? return tileId from line segment (line segments's coords)
// Tell tile id from segment's coords
//func getTileIdFromSegment(segmentLine line, tileSize int) (int, int) {
func getLineSegmentTileId(segmentLine Line, tileSize int) (int, int) {

	tileIdX := 0
	tileIdY := 0

	minX := min(segmentLine.p1.X, segmentLine.p2.X)
	maxX := max(segmentLine.p1.X, segmentLine.p2.X)
	Xdiff := maxX - minX

	fmt.Println("minX:", minX)
	fmt.Println("maxX:", maxX)
	fmt.Println("Xdiff:", Xdiff)

	// Checks ?
	if (maxX - minX) > tileSize {
		panic("segment is too big, or incorrect? x diff check")
	}
	// XXX do same for Y
	minY := min(segmentLine.p1.Y, segmentLine.p2.Y)
	maxY := max(segmentLine.p1.Y, segmentLine.p2.Y)
	Ydiff := maxY - minY

	fmt.Println("minY:", minY)
	fmt.Println("maxY:", maxY)
	fmt.Println("Ydiff:", Ydiff)

	// Checks ?
	if (maxY - minY) > tileSize {
		panic("segment is too big, or incorrect? y diff check")
	}

	tileIdX = int(segmentLine.p1.X / tileSize)

	fmt.Println("tileIdX:", tileIdX)

	tileIdY = int(segmentLine.p1.Y / tileSize)

	fmt.Println("tileIdY:", tileIdY)

	return tileIdX, tileIdY
}

// ? Draw segmented line test ?
// XXX add version that just accepts segments ?
func DrawSegmentedLineTest(x1, y1, x2, y2 int, tileSize int) image.Image {
	
	fmt.Println("DrawSegmentedLineTest x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)

	sx, sy, segments := SplitLineIntoTileSegments(x1, y1, x2, y2, tileSize)
	_ = sx
	_ = sy

	fmt.Println("segments:", segments)

	// XXX ?
	imW := max(x1, x2)
	imH := max(y1, y2)
	//imW = 512
	//imH = 512
	imW = max(512, imW)
	imH = max(512, imH)

	im := image.NewNRGBA(image.Rect(0, 0, imW, imH))
	{
		c := color.NRGBA{255, 255, 255, 255}
		draw.Draw(im, im.Bounds(), &image.Uniform{c}, image.Point{}, draw.Src)
	}

	// Draw grid
	{
	gridSizeX := imW/tileSize
	gridSizeY := imH/tileSize
	fmt.Println("gridSizeX:", gridSizeX)
	fmt.Println("gridSizeY:", gridSizeY)
	for y := 0; y < gridSizeY; y++ {
		for x := 0; x < gridSizeX; x++ {
			c := color.NRGBA{0, 192, 128, 255}
			px := x*tileSize
			py := y*tileSize
			drawRectangle(im, px, py, px+tileSize, py+tileSize, c)
		}
	}
	/*
		// XXX
		c := color.NRGBA{0, 128, 255, 255}
		drawRectangle(im, 0, 0, 50, 50, c)
	*/
	}

	fmt.Println("draw segments...")
	for _, s := range segments {
		fmt.Println("-> s:", s)
	
		_ = s
		c := color.NRGBA{0, 0, 255, 255}
		
		bresenham.DrawLine(im, s.p1.X, s.p1.Y, s.p2.X, s.p2.Y, c)

		// Draw line origin
		if true {
			c := color.NRGBA{0, 255, 0, 255}
			draw.Draw(im, image.Rect(s.p1.X, s.p1.Y, s.p1.X+4, s.p1.Y+4), &image.Uniform{c},
				image.Point{}, draw.Src)
		}
	}

	return im
}


// ? Draw segmented line ?
func DrawSegmentedLine(x1, y1, x2, y2 int,
	tiles [][]*image.NRGBA, tileSize int) {
	
	fmt.Println("DrawSegmentedLine x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)
}
