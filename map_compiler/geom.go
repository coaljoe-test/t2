package map_compiler

import (
	//"fmt"
	"math"
)


func Radians(degrees float64) float64 {
	return math.Pi * degrees / 180.0
}

func Degrees(radians float64) float64 {
	return radians * (180.0 / math.Pi)
}


// ?
func AngleBetweenPoints(x1, y1, x2, y2 int) float64 {
	deltaY := math.Abs(float64(y2) - float64(y1))
	deltaX := math.Abs(float64(x2) - float64(x1))
	
	return Degrees(math.Atan2(deltaY, deltaX))
}

// True if point is inside the rectange.
//func InRect(x, y, rx, ry, rw, rh float64) bool {
func InRect(x, y, rx, ry, rw, rh int) bool {
	if x >= rx && x <= rx+rw &&
		y >= ry && y <= ry+rh {
		return true
	} else {
		return false
	}
}
//func DistanceTo(x1, y1, x2, y2 float64) float64 {
func DistanceTo(x1, y1, x2, y2 int) float64 {
	//return Sqrt(Pow(b.x - a.x, 2) - Pow(b.y - a.y, 2))
	//return math.Hypot(b.X()-a.X(), b.Y()-a.Y())
	return math.Hypot(float64(x2) - float64(x1), float64(y2) - float64(y1))
}

func Lerp(a, b, t float64) float64 {
	return (1-t)*a + t*b
}

/*
func Lerp2(a, b Vec2, t float64) Vec2 {
	return Vec2{Lerp(a.X(), b.X(), t),
		Lerp(a.Y(), b.Y(), t)}
}
*/
