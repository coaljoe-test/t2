package map_compiler

import (
	"fmt"
)

// Map def / document (json)
// Map data
// XXX rename to MapData ?
type MapData struct {
	//Info *MapDefInfo
	//Nodes []*MapDefNode
	// XXX these are loaded like def_path
	// to have acceses MapDef.Scenes[0]
	Scenes []*MapDataScene
}

// Map scene info (json)
type MapDataSceneInfo struct {
	//Width int
	//Height int
	Dim [2]int
}

// Map editor info/record (?) (json)
type MapDataEditorInfo struct {
	// optional
	Modifiers []*MapDataEditorModifier
}

// ?
type MapDataEditorModifier struct {
	// _id ?
	//Id uuid
	// _type ?
	//Type string

	// _type ?
	Type string

	Amount int

	// node_id ?
	NodeId uuid

	Enabled bool
}


// Map scene (json)
type MapDataScene struct {
	Info *MapDataSceneInfo
	Meshes []*MapDataMesh
	Nodes []*MapDataNode
	//Scenes
	// XXX extra ?
	// optional
	Editor *MapDataEditorInfo
}

// ?
type MapDataMesh struct {
	// Object / mesh id ?
	Id uuid
	// XXX path ?
	//Mesh string
	// Optional ?
	Mesh *string
	
	// XXX Optional ?
	MeshGen *MapDataMeshGen
}

// Map mesh's gen (mesh_gen) info/data/record (json)
type MapDataMeshGen struct {
	Type string
	Size [3]float64

	// Optional ?
	Texture *string
	// Optional ?
	Color *[3]int
	// Optional ?
	Unshaded *bool
}


// Map scene node record (json)
// an abstraction ?
type MapDataNode struct {
	// _id ?
	Id uuid
	// _type ?
	Type string
	
	Name string
	Description string
	NodeType string

	Pos [3]float64
	Rot [3]float64
	// Default = 1.0,1.0,1.0
	Scale [3]float64

	// Is it optional since
	// MeshGen (mesh_gen) only
	// mesh can exist ?
	// Optional ?
	//Mesh *string
	// XXX
	// Optional ?
	//MeshId *string
	// Is it optional ?
	MeshId *uuid
	// XXX extra ?
	// Optional
	//MeshGen *MapDataMeshGen
	
	// Optional ?
	//Mesh *string
	// Optional ?
	//MeshGen *MapDataMeshGen
	// XXX
	//Sprite *MapDataSprite

	Tags []string

	DefPath string
}

func NewMapData() *MapData {

	md := &MapData{
		//Nodes: make([]*MapDefNode, 0),
		Scenes: make([]*MapDataScene, 0),
	}

	return md
}

func NewMapDataNode() *MapDataNode {
	return &MapDataNode{
		Scale: [3]float64{1, 1, 1},
		Tags: make([]string, 0),
	}
}

func (n *MapDataNode) String() string {
	return fmt.Sprintf("MapDataNode<name: %s, type: %s>",
		n.Name, n.NodeType)
}

func NewMapDataScene() *MapDataScene {
	return &MapDataScene{
		Nodes: make([]*MapDataNode, 0),
	}
}
