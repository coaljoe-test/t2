package map_compiler

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"
	
	"github.com/StephaneBunel/bresenham"
)

func testLineIntersectsXCoord(x1, y1, x2, y2, tileSize int) bool {
	fmt.Println("testLineIntersectsXCoord:",
		"x1:", x1, "y1:", y1, "x2:", x2, "y2:", y2, "tileSize:", tileSize)

	// Min x coord ?
	minX := min(x1, x2)
	// Max x coord ?
	maxX := max(x1, x2)
	// Length ?
	Xdiff := maxX - minX

	fmt.Println("minX:", minX)
	fmt.Println("maxX:", maxX)
	fmt.Println("Xdiff:", Xdiff)

	// Checks ?
	if (maxX - minX) > tileSize {
		panic("line is too big?")
	}

	hit := false
	if minX+Xdiff > tileSize {
		hit = true
	}

	fmt.Println("hit:", hit)

	return hit
}

func testLine() {
	fmt.Println("testLine")

	l := Line{
		image.Point{0, 0},
		image.Point{1, 1},
	}
	_ = l

	l2 := Line{
		image.Point{65, 0},
		image.Point{66, 0},
	}
	_ = l2

	//tileId := getSegmentTileId(l, 64)
	//tileId := getTileIdFromSegment(l, 64)
	//tileIdX, tileIdY := getLineSegmentTileId(l, 64)
	tileIdX, tileIdY := getLineSegmentTileId(l2, 64)
	fmt.Println("tileIdX:", tileIdX)
	fmt.Println("tileIdY:", tileIdY)

	testLineIntersectsXCoord(0, 0, 5, 5, 64)
	testLineIntersectsXCoord(63, 63, 68, 68, 64)

	//pp(2)
}

func SplitLineIntoTileSegments2(x1, y1, x2, y2 int, tileSize int) (int, int, []Line) {

	fmt.Println("SplitLineIntoTileSegments2 x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)

	ret := make([]Line, 0)
	_ = ret
	
	// module length over x coord ?
	lengthXCoord := math.Abs(float64(x2) - float64(x1))
	lengthYCoord := math.Abs(float64(y2) - float64(y1))

	fmt.Println("lengthXCoord:", lengthXCoord)
	fmt.Println("lengthYCoord:", lengthYCoord)

	numSplitsX := int(math.Ceil(float64(lengthXCoord) / float64(tileSize)))
	numSplitsY := int(math.Ceil(float64(lengthYCoord) / float64(tileSize)))

	fmt.Println("numSplitsX:", numSplitsX)
	fmt.Println("numSplitsY:", numSplitsY)

	sx := numSplitsX
	sy := numSplitsY
	_ = sx
	_ = sy
	
	// X angle
	xAngle := AngleBetweenPoints(x1, y1, x2, y2)
	fmt.Println("xAngle:", xAngle)
	//a := Radians(xAngle)
	
	totalLineLength := DistanceTo(x1, y1, x2, y2)
	fmt.Println("totalLineLength:", totalLineLength)
	
	// XXX
	//segmentLength := 1.0
	segmentLength := 3.0
	fmt.Println("segmentLength:", segmentLength)
	testNumSegments := totalLineLength / segmentLength
	fmt.Println("testNumSegments:", testNumSegments)
	testNumSegmentsi := int(testNumSegments)
	fmt.Println("testNumSegmentsi:", testNumSegmentsi)

	if true {
	fmt.Println("cycle points...")
	
	//prevX := int(0)
	//prevY := int(0)
	prevX := x1
	prevY := y1
	for i := 0; i < testNumSegmentsi; i++ {
		fmt.Println("-> i:", i)

		px := prevX
		py := prevY

		a := Radians(xAngle)
		d := segmentLength
		//d := segmentLength - float64(xOffset)
		//d := xSegmentLength - float64(xOffset)
		//d := 1.0
		px1 := int(math.Round(float64(px) + (d * math.Cos(a))))
		py1 := int(math.Round(float64(py) + (d * math.Sin(a))))

		fmt.Println("px:", px)
		fmt.Println("py:", py)
		fmt.Println("px1:", px1)
		fmt.Println("py1:", py1)
		
		add := true
		
		// XXX test line
		if testLineIntersectsXCoord(px, py, px1, py1, tileSize) {
			//pp(2)
			add = false
		}

		if add {
			// XXX
			ret = append(ret, Line{
				p1: image.Point{px, py},
				p2: image.Point{px1, py1},
			})
		}

		// XXX
		prevX = px1
		prevY = py1
	}
	
	}


	return sx, sy, ret
}

// ? Draw segmented line test ?
func DrawSegmentedLineTest2(x1, y1, x2, y2 int, tileSize int) image.Image {
	
	fmt.Println("DrawSegmentedLineTest2 x1:", x1, "y1:", y1,
		"x2:", x2, "y2:", y2, "tileSize:", tileSize)

	sx, sy, segments := SplitLineIntoTileSegments2(x1, y1, x2, y2, tileSize)
	_ = sx
	_ = sy

	fmt.Println("segments:", segments)

	// XXX ?
	imW := max(x1, x2)
	imH := max(y1, y2)
	//imW = 512
	//imH = 512
	imW = max(512, imW)
	imH = max(512, imH)

	im := image.NewNRGBA(image.Rect(0, 0, imW, imH))
	{
		c := color.NRGBA{255, 255, 255, 255}
		draw.Draw(im, im.Bounds(), &image.Uniform{c}, image.Point{}, draw.Src)
	}

	// Draw grid
	{
	gridSizeX := imW/tileSize
	gridSizeY := imH/tileSize
	fmt.Println("gridSizeX:", gridSizeX)
	fmt.Println("gridSizeY:", gridSizeY)
	for y := 0; y < gridSizeY; y++ {
		for x := 0; x < gridSizeX; x++ {
			c := color.NRGBA{0, 192, 128, 255}
			px := x*tileSize
			py := y*tileSize
			drawRectangle(im, px, py, px+tileSize, py+tileSize, c)
		}
	}
	/*
		// XXX
		c := color.NRGBA{0, 128, 255, 255}
		drawRectangle(im, 0, 0, 50, 50, c)
	*/
	}

	fmt.Println("draw segments...")
	for _, s := range segments {
		fmt.Println("-> s:", s)
	
		_ = s
		c := color.NRGBA{0, 0, 255, 255}
		
		bresenham.DrawLine(im, s.p1.X, s.p1.Y, s.p2.X, s.p2.Y, c)

		// Draw line origin
		if true {
			c := color.NRGBA{0, 255, 0, 255}
			//draw.Draw(im, image.Rect(s.p1.X, s.p1.Y, s.p1.X+4, s.p1.Y+4), &image.Uniform{c},
			//	image.Point{}, draw.Src)
			draw.Draw(im, image.Rect(s.p1.X, s.p1.Y, s.p1.X+1, s.p1.Y+1), &image.Uniform{c},
				image.Point{}, draw.Src)
		}
	}

	return im
}
