package map_compiler

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"math"
	//"bytes"
	"slices"
	//"strconv"
	"os"

	//v4 "github.com/fossoreslp/go-uuid-v4"
	//"github.com/geange/gods-generic/sets/linkedhashset"
	//"github.com/geange/gods-generic/utils"
	//"github.com/geange/gods-generic/maps/linkedhashmap"
	//"github.com/zijiren233/gencontainer/lhashmap"
	//"github.com/ugurcsen/gods-generic/maps/linkedhashmap"

	//c "test/object_renderer/render_backend/common"

	"test/map_compiler/text"
)

type MapRenderer struct {
	md *MapData
	// ?
	curScene *MapDataScene
	
	// Maximum map(?) bounds or dimensions ?
	maxDimX int
	maxDimY int
	
	// Default = 512
	//tileW int
	//tileH int
	tileSize int
	// dim ?
	numTilesX int
	numTilesY int
	
	// Tiled image
	//Tiles []*MapTile
	Tiles [][]*image.NRGBA
	TilesDepth [][]*image.NRGBA

	objectRenderer *ObjectRenderer
}

func NewMapRenderer() *MapRenderer {
	mr := &MapRenderer{
		//tileW: 512,
		//tileH: 512,
		tileSize: 512,
		objectRenderer: NewObjectRenderer(),
	}

	return mr
}

func (mr *MapRenderer) initTiles(w, h int) {
	fmt.Println("MapRenderer initTiles w:", w, "h:", h)

	//tileW := 512
	//tileH := 512
	
	tileW := mr.tileSize
	tileH := mr.tileSize

	//// Init ?
	mr.Tiles = make([][]*image.NRGBA, w)
	
	for i := range mr.Tiles {
		mr.Tiles[i] = make([]*image.NRGBA, h)	
	}
	
	//dump(mr.Tiles)
	
	//panic(2)
	
	////

	for y := 0; y < w; y++ {
		for x := 0; x < h; x++ {
			fmt.Println("-> x:", x, "y:", y)
			
			rect := image.Rect(0, 0, tileW, tileH)
			im := image.NewNRGBA(rect)
			mr.Tiles[x][y] = im
		}
	}

	dump(mr.Tiles)
}


func (mr *MapRenderer) SetMapData(md *MapData) {
	fmt.Println("MapRenderer SetMapData md:", md)
	
	mr.md = md
}

//func (mr *MapRenderer) SetMapDataScene(s *MapDataScene) {
//	fmt.Println("MapRenderer SetMapData md:", md)

// Set scene data ?
func (mr *MapRenderer) SetScene(s *MapDataScene) {
	fmt.Println("MapRenderer SetScene s:", s)
	
	mr.curScene = s
}

func (mr *MapRenderer) SetNumTiles(x, y int) {
	fmt.Println("MapRenderer SetNumTiles x:", x, "y:", y)

	mr.numTilesX = x
	mr.numTilesY = y

	// XXX
	mr.initTiles(x, y)
}

func (mr *MapRenderer) SetTileSize(tileSize int) {
	fmt.Println("MapRenderer SetTileSize tileSize:", tileSize)

	mr.tileSize = tileSize
	
	// XXX reinit tiles
	mr.initTiles(mr.numTilesX, mr.numTilesY)
}

func (mr *MapRenderer) Reset() {
	fmt.Println("MapRenderer Reset")
	
	//mr.RenderedObjects.Clear()
	mr.ClearCache()
}

func (mr *MapRenderer) ClearCache() {
	fmt.Println("MapRenderer ClearCache")
	
	//mr.RenderedObjectsCache.Clear()
	ctx.renderCache.Clear()
}

func (mr *MapRenderer) haveNodeInCache(n *MapDataNode) {
	fmt.Println("MapRenderer haveNodeInCache")

    panic("not implemented")
}

// XXX fixme ?
func (mr *MapRenderer) getCurScene() *MapDataScene {
	fmt.Println("MapRenderer getCurScene")

	//panic("not implemented")
	
	/*
	if mr.md == nil {
		pp("MapData is nil")
	}
	
	s := mr.md.Scenes[0]
	*/
	
	// XXX
	s := mr.curScene
	
	return s
}

var _ = `
// Get map extents
func (mr *MapRenderer) EvaluateBounds() {
}
`

// Get map / scene extents
// XXX what if map / scene has sub scenes ?
func (mr *MapRenderer) EvaluateSceneBounds(s *MapDataScene) {
	fmt.Println("MapRenderer EvaluateSceneBounds s:", s)

	minX := math.MaxFloat32
	maxX := -math.MaxFloat32

	minY := math.MaxFloat32
	maxY := -math.MaxFloat32

	//for i, n := range mr.md.Nodes {
	for i, n := range s.Nodes {
		fmt.Println("-> i:", i)

		x := n.Pos[0]
		y := n.Pos[1]

		if x > maxX {
			maxX = x
		}
		if x < minX {
			minX = x
		}
		if y > maxY {
			maxY = y
		}
		if y < minY {
			minY = y
		}
	}

	fmt.Println("minX:", minX)
	fmt.Println("maxX:", maxX)
	fmt.Println("minY:", maxY)
	fmt.Println("maxY:", maxY)
}

func (mr *MapRenderer) EvaluateSceneBounds2(s *MapDataScene) {
	panic("not imlemented")

}

func (mr *MapRenderer) Test1(s *MapDataScene) {
	fmt.Println("MapRenderer Test1 s:", s)

	//s := mr.md.Scenes[0]
	//_ = s

	for i, node := range s.Nodes {
		fmt.Println("-> i:", i)

		_ = node

		fmt.Println("id:", node.Id)
		//fmt.Println("mesh:", node.Mesh)
		fmt.Println("meshId:", node.MeshId)
		xmesh := MdGetMeshForSceneNode(s, node)
		fmt.Println("mesh:", xmesh)
	}
}

func (mr *MapRenderer) Test2(s *MapDataScene) {
	fmt.Println("MapRenderer Test2 s:", s)

	//s := mr.md.Scenes[0]
	//_ = s

	// Meshes list
	meshes := make([]string, 0)

	for i, node := range s.Nodes {
		fmt.Println("-> i:", i)

		_ = node

		fmt.Println("id:", node.Id)
		//fmt.Println("mesh:", node.Mesh)
		meshRec := MdGetMeshForSceneNode(s, node)
		
		if meshRec.Mesh != nil {
			xmesh := *meshRec.Mesh
			fmt.Println("mesh:", xmesh)

			if !slices.Contains(meshes, xmesh) {
				meshes = append(meshes, xmesh)
			}
		}
	}

	fmt.Println()
	fmt.Println("Meshes list:")
	fmt.Println(meshes)

	for i, mesh := range meshes {
		fmt.Println("-> i:", i)

		fmt.Println("mesh:", mesh)

		//path := mesh

		// XXX
		path := "../" + mesh
		
		// Render mesh from path ?
		mr.renderMesh(path, false)
	}
}

// XXX util ?
// Test
// Mesh/MeshNode equal
// XXX for non-generated meshes ?
//func (mr *MapRenderer) MdMeshNodeEqual(n1, n2 *MapDataNode) bool {
func (mr *MapRenderer) MdMeshNodeEqual(s *MapDataScene, n1, n2 *MapDataNode) bool {
	//fmt.Println("MapRenderer MdMeshNodeEqual n1:", n1, "n2:", n2)
	fmt.Println("MapRenderer MdMeshNodeEqual s:", s, "n1:", n1, "n2:", n2)

	// XXX
	//s := mr.md.Scenes[0]
	//s := mr.getCurScene()
	
	//if n1.Mesh == nil || n2.Mesh == nil {
	if !MdSceneNodeHasMesh(s, n1) || !MdSceneNodeHasMesh(s, n2) {
		panic("error, mesh is nil")
	}
	
	mesh1 := MdGetMeshForSceneNode(s, n1)
	mesh2 := MdGetMeshForSceneNode(s, n2)
	
	fmt.Println("x:", n1)
	//fmt.Println("x1:", n1.Mesh)
	fmt.Println("x1:", mesh1)
	fmt.Println("x2:", n1.Rot)
	//if n1.Mesh == n2.Mesh && n1.Rot == n2.Rot {
	if mesh1 == mesh2 && n1.Rot == n2.Rot {
		fmt.Println("true")
		//panic(4)
		return true
	}
	
	return false
}

// XXX
// util ?
func (mr *MapRenderer) MdMeshGenEqual(obj1, obj2 *MapDataMeshGen) bool {
//func (mr *MapRenderer) MdMeshGenEqual(s *MapDataScene, obj1, obj2 *MapDataMeshGen) bool {
	fmt.Println("MapRenderer MdMeshGenEqual obj1:", obj1, "obj2:", obj2)

	if obj1.Type != obj2.Type {
		return false
	}
	if obj1.Size != obj2.Size {
		return false
	}
	/*
	// XXX optional ?
	if obj1.Texture != obj2.Texture {
		return false
	}
	*/
	
	return true
}

// XXX util ?
// Test
// MeshGen/MeshGenNode equal
// XXX for generated meshes ?
//func (mr *MapRenderer) MdMeshGenNodeEqual(n1, n2 *MapDataNode) bool {
func (mr *MapRenderer) MdMeshGenNodeEqual(s *MapDataScene, n1, n2 *MapDataNode) bool {
	fmt.Println("MapRenderer MdMeshGenNodeEqual n1:", n1, "n2:", n2)
	
	meshGen1 := MdGetMeshGenForSceneNode(s, n1)
	meshGen2 := MdGetMeshGenForSceneNode(s, n2)
	
	//if n1.MeshGen == nil || n2.MeshGen == nil {
	if meshGen1 == nil && meshGen2 == nil {
		panic("error, meshgen is nil")
	}
	
	//if mr.MdMeshGenEqual(n1.MeshGen, n2.MeshGen) && n1.Rot == n2.Rot {
	if mr.MdMeshGenEqual(meshGen1, meshGen2) && n1.Rot == n2.Rot {
		fmt.Println("true")
		//panic(4)
		return true
	}
	
	return false
}


// Cache ?
// Render all objects to cache ?
func (mr *MapRenderer) RenderAllObjectsToCache(s *MapDataScene) {
	fmt.Println("MapRenderer RenderAllObjectsToCache s:", s)

	//s := mr.md.Scenes[0]
	//_ = s

	// Meshes list (strings)
	meshes := make([]string, 0)
	// MeshGens list (MapDataMeshGen),
	// just for info ?
	// not needed
	meshGens := make([]*MapDataMeshGen, 0)
	// Nodes list with meshes
	// non-duplicate ?
	nodes := make([]*MapDataNode, 0)

	//// Create a list

	for i, node := range s.Nodes {
		fmt.Println("-> i:", i)

		_ = node
		
		mesh := MdGetMeshForSceneNode(s, node)
		meshGen := MdGetMeshGenForSceneNode(s, node)

		fmt.Println("id:", node.Id)
		//fmt.Println("mesh:", node.Mesh)
		//fmt.Println("meshGen:", node.MeshGen)
		fmt.Println("mesh:", mesh)
		fmt.Println("meshGen:", meshGen)
		fmt.Println("rot:", node.Rot)
		
		// XXX check
		if mesh != nil && meshGen != nil {
			fmt.Println("check...")
			panic("error: both mesh and mesh_gen blocks are presented")
		}
		
		//panic(2)

		// Non-Default rot (rot is set, and non-default)
		if node.Rot != [3]float64{} {
			//nodes = append(nodes, node)

			var _ = `
			// This rot/mesh
			xmesh := node.Mesh
			xrot := node.Rot

			// Check if we not have the same rot/mesh node
			haveNode := slices.ContainsFunc(nodes, func(x *MapDataNode) bool {
				fmt.Println("x:", x)
				fmt.Println("x1:", x.Mesh)
				fmt.Println("x2:", x.Rot)
				if x.Mesh == xmesh && x.Rot == xrot {
					fmt.Println("true")
					//panic(4)
					return true
				}
				return false
			})
			`
			if mesh != nil {
				haveNode := slices.ContainsFunc(nodes, func(x *MapDataNode) bool {
					//return mr.MdMeshNodeEqual(node, x)
					return mr.MdMeshNodeEqual(s, node, x)
				})

				if !haveNode {
					nodes = append(nodes, node)
				}
			} else {
				haveNode := slices.ContainsFunc(nodes, func(x *MapDataNode) bool {
					return mr.MdMeshGenNodeEqual(s, node, x)
				})

				if !haveNode {
					nodes = append(nodes, node)
					meshGens = append(meshGens, meshGen)
				}			
			}
			
			//panic(3)
			
		} else {
		
			// With default rot (or not-set)

			if mesh != nil {
				// Standard mesh ?
				if !slices.Contains(meshes, *mesh.Mesh) {
					meshes = append(meshes, *mesh.Mesh)
					
					nodes = append(nodes, node)
				}
			} else {
				// Mesh gen ?
				fmt.Println("meshgen...")
				// XXX just add node ?
				//nodes = append(nodes, node)
				
				haveNode := slices.ContainsFunc(nodes, func(x *MapDataNode) bool {
					//return mr.MdMeshGenNodeEqual(node, x)
					return mr.MdMeshGenNodeEqual(s, node, x)
				})
	
				if !haveNode {
					fmt.Println("adding meshgen...")
					nodes = append(nodes, node)
					
					// XXX
					// for info/list
					//meshGens = append(meshGens, node.MeshGen)
					meshGens = append(meshGens, meshGen)
				}
				
				//pp(2)
			}
			//slices.Contains

			// XXX
			//nodes = append(nodes, node)
		}

		//if !slices.Contains(nodes, node) {
		//	nodes = append(nodes, node)
		//
		//	// XXX
		//	meshes = append(meshes, node.Mesh)
		//}
	}

	fmt.Println()
	
	fmt.Println("Meshes list:")
	fmt.Println(meshes)

	fmt.Println("MeshGens list:")
	fmt.Println(meshGens)
	
	fmt.Println("Nodes list:")
	fmt.Println(nodes)
	
	fmt.Println()
	
	fmt.Println("dump nodes:")
	dump(nodes)
	
	//panic(2)

	//for i, mesh := range meshes {
	
	for i, n := range nodes {
		fmt.Println("-> i:", i)

		//im, imDepth := mr.renderMeshObject(n)
		//im, imDepth := mr.renderMeshObject(n, true)
		im, imDepth := mr.renderMeshObjectToCache(n)
		_ = im
		_ = imDepth

		//panic(2)
		
		////
		
		// Old code
		
		var _ = `
		// XXX Create record
		
		xId := n.Id
		xKey := RenderedObjectsCacheKey{
			uuid: xId,
			rot: n.Rot,
			scale: n.Scale,
		}
		
		rec := &RenderedObjectData{
			// Optional
			uuid: &xId,
			// Optional
			name: &n.Name,
			rot: n.Rot,
			im: im,
			imDepth: imDepth,
		}

		fmt.Println("id:", xId)
		fmt.Println("rec:", rec)
		fmt.Println("xKey:", xKey)
		
		//mr.RenderedObjectsCache.Add(rec)
		//mr.RenderedObjectsCache.Put(n.Id, rec)
		//mr.RenderedObjectsCache.Put(xKey, rec)
		ctx.renderCache.RenderedObjectsCache.Put(xKey, rec)
		`
		
		//panic(2)
	}
}

func (mr *MapRenderer) renderMeshEx(path string,
	modeDepth bool, rot *[3]float32) image.Image {

	fmt.Println("MapRenderer renderMeshEx path:", path,
		"modeDepth:", modeDepth, "rot:", rot)

	//im := doRenderObjectEx(path, modeDepth, rot)

	objectR := mr.objectRenderer
	im := objectR.RenderObjectEx(path, modeDepth, rot)

	return im
}

func (mr *MapRenderer) renderMesh(path string, modeDepth bool) image.Image {
	fmt.Println("MapRenderer renderMesh path:", path, "modeDepth:", modeDepth)

	objectR := mr.objectRenderer
	//im := doRenderObject(path, modeDepth)
	im := objectR.RenderObject(path, modeDepth)

	return im
}

// Support for generated meshes ?
func (mr *MapRenderer) renderMeshGen(meshGen *MapDataMeshGen, modeDepth bool) image.Image {
	fmt.Println("MapRenderer renderMeshGen meshGen:", meshGen, "modeDepth:", modeDepth)

	objectR := mr.objectRenderer
	im := objectR.RenderObjectGen(meshGen, modeDepth)

	return im
}

func (mr *MapRenderer) renderMeshExGen(meshGen *MapDataMeshGen, modeDepth bool,
	rot *[3]float32, scale *[3]float32) image.Image {

	fmt.Println(
		"MapRenderer renderMeshExGen meshGen:", meshGen, "modeDepth:", modeDepth,
		"rot:", rot, "scale:", scale)

	objectR := mr.objectRenderer
	im := objectR.RenderObjectExGen(meshGen, modeDepth, rot, scale)

	return im
}


func (mr *MapRenderer) renderMeshObjectToCache(n *MapDataNode) (
	image.Image, image.Image) {
	
	fmt.Println("MapRenderer renderMeshObjectToCache:", n)
	
	return mr.renderMeshObject(n, true)
}

// TODO add actual rot
// TODO add scale ?
// like more high level interface
func (mr *MapRenderer) renderMeshObject(n *MapDataNode, addToCache bool) (image.Image, image.Image) {
	fmt.Println("MapRenderer renderMeshObject:", n, "addToCache:", addToCache)
	
	// XXX ?
	//s := mr.md.Scenes[0]
	s := mr.getCurScene()
	
	mesh := MdGetMeshForSceneNode(s, n)
	meshGen := MdGetMeshGenForSceneNode(s, n)

	// XXX Check ?
	//if n.Mesh != nil && n.MeshGen != nil {
	//if MdSceneNodeHasMesh(s, n) && MdSceneNodeHasMeshGen(s, n) {
	if mesh != nil && meshGen != nil {
		panic("error: both mesh and mesh_gen blocks are presented")
	}

	//mesh := *n.Mesh

	var im, imDepth image.Image
	//if n.Mesh != nil {
	if mesh != nil {
			
		meshPath := *mesh.Mesh

		fmt.Println("meshPath:", meshPath)

		//pdump(n)
	
		//path := mesh

		// XXX
		path := "../" + meshPath
	
		fmt.Println("path:", path)
	
		// XXX TODO add actual rot		
		im = mr.renderMesh(path, false)
		imDepth = mr.renderMesh(path, true)
	} else {
		fmt.Println("meshgen...")
		//obj := n.MeshGen
		obj := meshGen
		im = mr.renderMeshGen(obj, false)
		imDepth = mr.renderMeshGen(obj, true)	
	}
	
	////
	
	// XXX Create recored
	
	if addToCache {
		fmt.Println("add to cache...")

		xId := n.Id
		xMeshId := *n.MeshId
		/*
		xKey := RenderedObjectsCacheKey{
			uuid: xId,
			rot: n.Rot,
			scale: n.Scale,
		}
		*/
		xKey := NewRenderedObjectsCacheKey()
		//xKey.uuid = xId
		xKey.uuid = xMeshId
		xKey.rot = n.Rot
		xKey.scale = n.Scale
		
		/*
		rec := &RenderedObjectData{
			// Optional
			uuid: &xId,
			// Optional
			name: &n.Name,
			rot: n.Rot,
			im: im,
			imDepth: imDepth,
		}
		*/
		rec := NewRenderedObjectData()
		// Optional
		//rec.uuid = &xId
		rec.uuid = &xMeshId
		// Optional
		rec.name = &n.Name
		rec.rot = n.Rot
		rec.im = im
		rec.imDepth = imDepth
	
		fmt.Println("id:", xId)
		fmt.Println("rec:", rec)
		fmt.Println("xKey:", xKey)
		
		//mr.RenderedObjectsCache.Add(rec)
		//mr.RenderedObjectsCache.Put(n.Id, rec)
		//mr.RenderedObjectsCache.Put(xKey, rec)
		ctx.renderCache.RenderedObjectsCache.Put(xKey, rec)
		
		fmt.Println("done")
		
		//pp(2)
	}
	
	//pp(2)
	
	return im, imDepth
}


func (mr *MapRenderer) BlitImage() {
	fmt.Println("MapRenderer BlitImage")

}

func (mr *MapRenderer) UpdateTile() {
	fmt.Println("MapRenderer UpdateTile")

}

func (mr *MapRenderer) RenderTile() {
	fmt.Println("MapRenderer RenderTile")

}


// XXX internal ?
// This renders node's object ?
func (mr *MapRenderer) RenderNodeObject(mn *MapDataNode) {
	fmt.Println("MapRenderer RenderNodeObject mn:", mn)

	// Scene node
	if mn.NodeType == "scene" {
		panic(3)
	}

	// XXX mesh node
	//if mn.NodeType != "mesh_node" {
	if mn.NodeType != "mesh" {
		panic("unknown node")
	}
	
	// XXX ?
	mr.renderMeshObject(mn, true)

	//panic(2)
}

// This renders node on the map ?
func (mr *MapRenderer) RenderNode(mn *MapDataNode) {
	fmt.Println("MapRenderer RenderNode mn:", mn)
	
	// XXX
	mr.RenderNodeObject(mn)
	
	pos := [3]float64{0, 0, 0}
	
	//mr.RenderObjectAt(mn.Id, pos)
	
	// Object / mesh id
	meshId := *mn.MeshId
	mr.RenderObjectAt(meshId, pos)
	
	// XXX test
	
	//pos2 := [3]float64{2, 0, 0}
	pos2 := [3]float64{50, 0, 0}
	
	//mr.RenderObjectAt(mn.Id, pos2)
	mr.RenderObjectAt(meshId, pos2)
	
	// XXX test
	
	mr.SaveTiles(GetTmpDir() + "/cache4")
	
	//pp(2)
}

// XXX render object at ?
func (mr *MapRenderer) RenderObjectAt(objectId uuid, pos [3]float64) {
	fmt.Println("MapRenderer RenderObjectAt objectId:", objectId, "pos:", pos)
	
	// XXX list test
	if true {
	//if false {
		fmt.Println("list...")
	
		//objs := ctx.renderCache.RenderedObjectsCache.Values()
		//fmt.Println("objs:", objs)

		keys := ctx.renderCache.RenderedObjectsCache.Keys()
		fmt.Println("keys:", keys)
		fmt.Println("len keys:", len(keys))
		
		k := keys[0]
		fmt.Println("k:", k)
		s := k.ToString()
		fmt.Println("s:", s)
		
		fmt.Println("end list")
		//pp(2)
	}
	
	// RenderedObject
	/*
	k := RenderedObjectsCacheKey{
		uuid: objectId,
		// Default value ?
		rot: [3]float64{0, 0, 0},		
		// Default value ?
		scale: [3]float64{1, 1, 1},
	}
	*/
	k := NewRenderedObjectsCacheKey()
	k.uuid = objectId
	k.rot = [3]float64{0, 0, 0}
	// k := NewRenderedObjectsCacheKey()
	// k.uuid = objectId
	// k.rot = rot
	// XXX TODO rot ?
	//k := RenderedObjectsCacheKey{uuid: objectId, rot: rot}
	//k := RenderedObjectsCacheKey{uuid: objectId, rot: rot, scale: scale}
	//obj := ctx.renderCache.GetObjectWithKey(k)
	obj := ctx.renderCache.GetObjectByKey(k)
	_ = obj
	
	fmt.Println("obj:", obj)

	SavePng(obj.im, GetTmpDir() + "/draw_obj.png")
	SavePng(obj.imDepth, GetTmpDir() + "/draw_obj_depth.png")
	
	// Render to tile ?
	
	// XXX
	tile := mr.getTile(0, 0)
	_ = tile

	SavePng(tile, GetTmpDir() + "/draw_tile.png")
	
	//px := 0
	//py := 0
	//px := 128
	//py := 128
	
	// XXX fixme
	unitsToPxK := 1.0
	
	px := int(pos[0] * unitsToPxK)
	py := int(pos[1] * unitsToPxK)
	
	fmt.Println("px:", px)
	fmt.Println("py:", py)
	
	im := obj.im.(*image.NRGBA)
	
	DrawOnTiles2(im, px, py, mr.Tiles, mr.tileSize)

	//panic(2)
}

func (mr *MapRenderer) getTile(x, y int) *image.NRGBA {
	fmt.Println("MapRenderer getTile x:", x, "y:", y)
	
	if x > mr.numTilesX || y > mr.numTilesY {
		panic("not such tile")
	}
	
	return mr.Tiles[x][y]
}

// Blit object at
// Automatically selects tiles ?
func (mr *MapRenderer) DrawObjectAt(objectId uuid, pos [3]float64) {
	fmt.Println("MapRenderer DrawObjectAt objectId:", objectId, "pos:", pos)
	
	posX := pos[0]
	posY := pos[1]
	posZ := pos[2]
	_ = posZ
	
	tileIdX := int(posX / float64(mr.tileSize))
	tileIdY := int(posY / float64(mr.tileSize))
	_ = tileIdX
	_ = tileIdY
	
	fmt.Println("tileIdX:", tileIdX)
	fmt.Println("tileIdY:", tileIdY)
	
	tileIm := mr.getTile(tileIdX, tileIdY)
	_ = tileIm

	panic("not implemented")
}


//func (mr *MapRenderer)   RenderScene(md *MapDef) {
//	fmt.Println("MapRenderer RenderScene md:", md)
func (mr *MapRenderer) RenderScene(s *MapDataScene) {
	fmt.Println("MapRenderer RenderScene s:", s)
	
	// XXX
	//mr.RenderAllObjectsToCache()

	//nodes := MdGetSceneNodes(s)
	nodes := MdGetSceneNodesWithType(s, "mesh")
	//nodes := MdGetSceneNodesWithType(s, "scene")
	
	fmt.Println("nodes:", nodes)

	//dump(nodes)

	//pdump(s)

	for i, n := range nodes {
		fmt.Println("-> i:", i)

		//RenderNode(n)
		//mr.RenderNodeObject(n)
		mr.RenderNode(n)
		
		//panic(2)
	}
}

// Save cache to disk
// in an arbitrary format / dump ?
func (mr *MapRenderer) SaveCache(path string) {
	fmt.Println("MapRenderer SaveCache path:", path)

	//panic("not implemented")
	
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		panic(err)
	}

	//for i, uuid := range mr.RenderedObjects {
	//for i, rod := range mr.RenderedObjectsCache.Values() {
	
	keys := ctx.renderCache.RenderedObjectsCache.Keys()
	for i, k := range keys {
		// XXX
		v, ok := ctx.renderCache.RenderedObjectsCache.Get(k)
		if !ok {
			panic("error")
		}
		rod := v
	
		fmt.Println("-> i:", i)
		fmt.Println("rod:", rod)
		//fmt.Println("uuid:", rod.uuid)
		
		if rod.uuid != nil {
			fmt.Println(" uuid:", *rod.uuid)
		}
		if rod.name != nil {
			fmt.Println(" name:", *rod.name)
		}
		
		skey := k.ToString()
		fmt.Println("skey:", skey)
		
		//data := rod.im
		//fmt.Println("len data:", len(data))
		
		outPath := path + "/" + skey + ".png"
		fmt.Println("outPath:", outPath)
		
		/*
		err := os.WriteFile(outPath, data, 0666)
		if err != nil {
			panic(err)
		}
		*/
		SavePng(rod.im, outPath)
		if true {
			if rod.imDepth != nil {
				outPathDepth := path + "/" + skey + "_depth.png"
				fmt.Println("outPathDepth:", outPathDepth)
				SavePng(rod.imDepth, outPathDepth)
			} else {
				fmt.Println("tile doesn't have depth...")
			}
		}
		
	}
	
	//panic(2)
}

func (mr *MapRenderer) SaveTiles(path string) {
	fmt.Println("MapRenderer SaveTile path:", path)
	
	err := os.MkdirAll(path, os.ModePerm)
	if err != nil {
		panic(err)
	}
	
	for y := 0; y < mr.numTilesY; y++ {
		for x := 0; x < mr.numTilesX; x++ {
			tileIm := mr.getTile(x, y)
		
			outPath := fmt.Sprintf(path + "/tile_%03d,%03d.png", x, y)
			fmt.Println("outPath:", outPath)
			
			SavePng(tileIm, outPath)
		}
	}
	
	//panic(2)
}

func (mr *MapRenderer) SaveTilesAsImage(path string) {

	fmt.Println("MapRenderer SaveTileAsImage path:", path)

	//resImage := image.NewNRGBA(image.Rectangle{
	//	Max: image.Point{X: 8192, Y: 8192}})
	//im := image.NewNRGBA(image.Rectangle{
	//	Max: image.Point{X: 1024, Y: 1024}})

	imW := mr.numTilesX * mr.tileSize
	imH := mr.numTilesY * mr.tileSize

	im := image.NewNRGBA(image.Rectangle{
		Max: image.Point{X: imW, Y: imH}})

	//bgColor := color.RGBA{0, 0, 255, 255}
	bgColor := color.RGBA{0, 0, 0, 255}
	//bgColor := color.RGBA{255, 255, 255, 255}
	draw.Draw(im, im.Bounds(), &image.Uniform{bgColor}, image.ZP, draw.Src)

	// Draw tile border
	drawBorder := true
	_ = drawBorder
	// Draw tile name/title
	drawName := true
	_ = drawName

	c := color.RGBA{225, 25, 148, 255}
	c2 := color.RGBA{25, 225, 148, 255}

	text.AddText(im, 0, 0, "test", c)
	text.AddText(im, 100, 100, "test2", c)
	text.AddText(im, 200, 200, "test3", c)

	//im2 := LoadPng(GetTmpDir() + "/x.png").(*image.NRGBA)

	///*
	for y := 0; y < mr.numTilesY; y++ {
		for x := 0; x < mr.numTilesX; x++ {
			tileIm := mr.getTile(x, y)
			_ = tileIm

			px := x * mr.tileSize
			py := y * mr.tileSize
			_ = px
			_ = py

			blitImage(im, tileIm, px, py)
			//blitImage(im, im2, px, py)

			if drawBorder {
				//c := color.RGBA{255, 0, 255, 255}
				//c := color.RGBA{255, 255, 255, 255}
				//c := color.RGBA{225, 25, 148, 255}
				//drawRectangle(im, 0, 0, 100, 100, c)
				drawRectangle(im, px, py, px+mr.tileSize, py+mr.tileSize, c)
			}

			if drawName {
				//text.AddText(im, 0, 0, "test", c)
				s := fmt.Sprintf("%d,%d", x, y)
				text.AddText(im, px+3, py, s, c2)
			}
		}
	}
	//*/

	text.AddText(im, 100, 0, "TEST", c)
	text.AddText(im, 100, 13, "TEST2", c)

	SavePng(im, path)
	
	//panic(2)
}


func RenderMapScene(md *MapData) {
	fmt.Println("RenderMapScene md:", md)

	// XXX
	s := md.Scenes[0]

	//for i, n := range md.Nodes {
	for i, n := range s.Nodes {
		fmt.Println("-> i:", i)

		RenderNode(n)
	}	
}
