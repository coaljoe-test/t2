package main

import (
	"fmt"
	"os"
	"flag"
	_path "path"
	
	//glm "github.com/technohippy/go-glmatrix"
	//lm "github.com/xlab/linmath"

	//. "github.com/go-gl/mathgl/mgl64"

	"test/object_renderer/render_backend/backend"
	c "test/object_renderer/render_backend/common"
	"test/object_renderer/render_backend/rx_backend"

	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"

	//"render_backend/rx_backend"
)

func main() {
	fmt.Println("main()")

	fmt.Println("args:", os.Args)
	

	//
	// Opts
	//
	resXOpt := flag.Int("res_x", -1, "renderer res x")
	resYOpt := flag.Int("res_y", -1, "renderer res y")
	// Dont show window/result at the end / Close window on exit
	noShowOpt := flag.Bool("no_show", false, "no show mode")
	setPosOpt := flag.String("set_pos", "0,0,0", "set object position")
	setRotOpt := flag.String("set_rot", "0,0,0", "set object rotation")
	_ = setPosOpt
	_ = setRotOpt

	
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(),
			"Usage: %s [options] [path]\n\nOptions:\n", _path.Base(os.Args[0]))
		flag.PrintDefaults()
	}

	flag.Parse()
	//panic(2)

	// XXX should this be initialized in the backend code / custom code ?
	// rx etc ?
	// what if rx is already initialize ?

	var _ = `

	conf := rx.NewTestFwConf()
	//conf.ResX = 1280
	//conf.ResY = 800
	//conf.ResX = 800
	//conf.ResY = 600
	//conf.ResX = 8192
	//conf.ResY = 8192
	//conf.ResX = 10000
	//conf.ResY = 10000
	conf.ResX = 2048
	conf.ResY = 2048

	if *resXOpt != -1 {
		conf.ResX = *resXOpt
		//panic(2)
	}
	if *resYOpt != -1{
		conf.ResY = *resYOpt
		//panic(2)
	}

	fmt.Println(*resXOpt)
	fmt.Println(*resYOpt)

	//panic(3)
	

	//app := rx.TestFwInit()
	app := rx.TestFwInitConf(conf)
	sce := rx.TestFwCreateDefaultScene()
	_ = sce
	
	// XXX disable debug ?
	xconf := rx.GetConf()
	_ = xconf
	//xconf.DebugDraw = false

	`

	var confResX int
	var confResY int

	conf := rx.NewDefaultConf()
	//conf.ResX = 1280
	//conf.ResY = 800
	//conf.ResX = 800
	//conf.ResY = 600
	//conf.ResX = 8192
	//conf.ResY = 8192
	//conf.ResX = 10000
	//conf.ResY = 10000
	confResX = 2048
	confResY = 2048

	if *resXOpt != -1 {
		confResX = *resXOpt
		//panic(2)
	}
	if *resYOpt != -1{
		confResY = *resYOpt
		//panic(2)
	}

	fmt.Println(*resXOpt)
	fmt.Println(*resYOpt)

	//panic(3)

	conf.DebugDraw = true

	rxi := rx.NewRx(conf)
	rxi.Init(confResX, confResY)

	app := rxi.App
	//win = app.Win()
	sce := rxi.Scene

	// ?
	
	/*
	// Iso cam
	cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	// Game iso 1:2
	cn.SetRot(Vec3{60, 0, 45})

	//fmt.Println(cn.Camera.Fov())
	//panic(2)
	cn.Camera.SetFov(10)


	gc := newGameCamera()
	Ctx.GameCamera = gc
	gc.setup(cn, Vec3{60 ,0, 45}, 40.0)
	*/

	//rxi := rx.Rxi()
	cn := rxi.Camera
	//cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
	mulV := 5.0
	cn.SetPos(Vec3{12.24745*mulV, -12.24745*mulV, 10.0*mulV})
	// Game iso 1:2
	cn.SetRot(Vec3{60, 0, 45})

	// XXX
	fmt.Println(cn.Camera.Fov())
	//panic(2)
	//cn.Camera.SetFov(10)
	//cn.Camera.SetFov(2)
	//cn.Camera.SetFov(4)
	//cn.Camera.SetFov(5)
	//cn.Camera.SetFov(150)

	cn.Camera.SetZnear(0.001)
	cn.Camera.SetZfar(100000.0)
	cn.Camera.SetFov(150)

	////

	// Zoom

	{
		if true {
			nativeResX := 512
			nativeResY := 512
			nativeZoom := 1.0
			_ = nativeResX
			_ = nativeResY
			_ = nativeZoom
	
			zoomV := 1.0

			scale := float64(confResX)/float64(nativeResX)
			scaleZ := 1.0/scale

			
			fmt.Println("scale:", scale)
			fmt.Println("scaleZ:", scaleZ)

			//zoomV = scaleZ
			zoomV = scale
		
			cn.Camera.SetZoom(zoomV)

			//panic(2)
		}

	}
	
	////

	var b backend.RenderBackendI

	fmt.Println("rx_backend init...")
	//xb := rx_backend.NewBackend()
	xb := rx_backend.NewBackend(rxi)
	xb.Init()
	b = xb

	fmt.Println(b)

	//panic(2)

	////

	l1 := sce.GetNodeByName("light1")
	//l1.SetPosZ(10)
	l1.SetPos(Vec3{-2, -15, 2})

	////

	//path := "../untitled.gltf"
	//path := os.Args[1]
	path := flag.Arg(0)
	fmt.Println("path:", path)
	data, err := os.ReadFile(path)
	_ = data

	if err != nil {
		panic(err)
	}

	t := c.NewTransform()

	//// Opts
	setPos := *setPosOpt
	if setPos != "0,0,0" {
		//panic(setPos)

		v := parseVec(setPos)
		//t.SetPos([3]float64{10.0, 0.0, 0.0})
		t.SetPos(v)

		//panic(2)
	}

	setRot := *setRotOpt
	if setRot != "0,0,0" {
		//panic(setRot)

		// X=90
		//curRot := objNode.Rot()
		//panic(curRot)

		
		v := parseVec(setRot)
		//t.SetRot([3]float64{0.0, 0.0, 45.0})
		t.SetRot(v)

		//panic(2)
	}

	//t.SetScale([3]float64{0.5, 0.5, 0.5})
	//t.SetScale([3]float64{2.0, 2.0, 2.0})
	//t.SetScale([3]float64{0.1, 0.1, 0.1})

	//t.SetRot([3]float64{0.0, 0.0, 45.0})

	//t.SetPos([3]float64{10.0, 0.0, 0.0})


	//im := b.RenderObject(data, t)
	im := b.RenderObject(path, t)
	_ = im

	outPath := GetTmpDir() + "/test_out.png"
	//b.RenderObjectFile(data, t, outPath)
	b.RenderObjectToPath(path, t, outPath)


	outPath2 := GetTmpDir() + "/test_out_depth.png"
	//b.RenderObjectDepthFile(data, t, outPath2)
	b.RenderObjectDepthToPath(path, t, outPath2)

	// ?

	//if true { panic(2) }

	noShow := *noShowOpt

	if !noShow {	
		for app.Step() {
			//println("step")
			dt := app.GetDt()
			_ = dt

			//uli.Update(dt)
			//b.Update(dt)

			app.Flip()
		}
	}

}
