module object_renderer

go 1.22.3

//replace object_renderer/rx_backend => ../rx_backend
replace test/object_renderer/render_backend/rx_backend => ./render_backend/rx_backend

//replace object_renderer/common => ../common
replace test/object_renderer/render_backend/common => ./render_backend/common

//replace object_renderer/common => ../common
replace test/object_renderer/render_backend/backend => ./render_backend/backend

//require object_renderer/rx_backend v0.0.0-00010101000000-000000000000
require test/object_renderer/render_backend/rx_backend v0.0.0-00010101000000-000000000000

require (
	kristallos.ga/rx v0.0.0-20230730105147-bf226ad5b775
	kristallos.ga/rx/math v0.0.0-20230730105147-bf226ad5b775
	test/common v0.0.0-00010101000000-000000000000
	test/object_renderer/render_backend/backend v0.0.0-00010101000000-000000000000
	test/object_renderer/render_backend/common v0.0.0-00010101000000-000000000000
)

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.12.0 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240506104042-037f3cc74f2a // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/qmuntal/gltf v0.24.2 // indirect
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
	kristallos.ga/lib/debug v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/ider v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/pubsub v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/sr v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/lib/xlog v0.0.0-20210129133423-4e6cf78261ae // indirect
	kristallos.ga/rx/loaders/gltf_test v0.0.0-00010101000000-000000000000 // indirect
	kristallos.ga/rx/phys v0.0.0-20210204202831-c0aefa214cc6 // indirect
	kristallos.ga/rx/transform v0.0.0-20230730105147-bf226ad5b775 // indirect
)

//replace kristallos.ga/rx => ../../rx_gltf_test
replace kristallos.ga/rx => ../../rx_upstream

replace kristallos.ga/rx/loaders/gltf_test => ../../rx_gltf_test/loaders/gltf_test

replace test/common => ../common
