package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
//	"github.com/go-gl/gl/v3.3-core/gl"
	"os"
	
	"github.com/disintegration/imaging"
)


// TODO: add jpeg support?
func saveImage(im image.Image, path string, flipY bool) {
	//Log.Dbg("%F im:", im, "path:", path, "flipY:", flipY)

	f, err := os.Create(path)
	if err != nil {
		fmt.Println("can't save image to path; path: ", path)
		panic(err)
	}
	defer f.Close()
	
	if flipY {
		if im.ColorModel() == color.Gray16Model {
			im = flipYImageGray16(im)
		} else {
			im = flipYImage(im)
		}
	}

	// disable compression
	//enc := &png.Encoder{CompressionLevel: -1}
	// Use fast compression
	enc := &png.Encoder{CompressionLevel: -2}
	enc.Encode(f, im)
}


// Flip rgba image
func flipYImage(im image.Image) image.Image {

	// Checks
	if im.ColorModel() == color.Gray16Model {
		panic("bad image color model")
	}
	
	im2 := imaging.FlipV(im)

	return im2
}

// Flip gray16 image
func flipYImageGray16(im image.Image) image.Image {

	// Checks
	if im.ColorModel() != color.Gray16Model {
		panic("bad image color model")
	}

	//im = color.Gray16Model.Convert(im)
	b := im.Bounds()
	//im2 := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	im2 := image.NewGray16(image.Rect(0, 0, b.Dx(), b.Dy()))
	for y := 0; y < b.Dy(); y++ {
		for x := 0; x < b.Dx(); x++ {
			c := im.(*image.Gray16).Gray16At(x, y)
			im2.SetGray16(x, b.Dy()-1-y, c)
		}
	}
	//draw.Draw(im2, im2.Bounds(), im, b.Min, draw.Src)

	// Debug
	if false {
		f, err := os.Create("/tmp/x_16.png")
		if err != nil {
			panic(err)
		}
		defer f.Close()
		png.Encode(f, im2)
		//png.Encode(f, im)
	}

	return im2
}
