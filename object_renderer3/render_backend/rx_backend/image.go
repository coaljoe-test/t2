package rx_backend

import (
	"fmt"
	"os"
	"image"
	"image/png"
)

func SaveImage(im image.Image, path string) {
	f, err := os.Create(path)
	if err != nil {
		fmt.Println("can't save image to path; path: ", path)
		panic(err)
	}
	defer f.Close()

	// disable compression
	enc := &png.Encoder{CompressionLevel: -1}
	enc.Encode(f, im)
}

func LoadImage(path string) image.Image {
	f, err := os.Open(path)
	if err != nil {
		fmt.Println("can't load image; path: ", path)
		panic(err)
	}
	defer f.Close()
	
	// XXX todo: add format / ext checks ?

	im, _, err := image.Decode(f)
	if err != nil {
		panic(err)
	}

	return im
}
