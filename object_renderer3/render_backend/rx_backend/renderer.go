package rx_backend

// TODO: use custom cameras instead of game cameras ?

import (
	"fmt"
	"image"
	//"math"
	//"image/draw"
	"encoding/binary"
	//"io/ioutil"
	//"bytes"
	"os"

	"kristallos.ga/rx"
	. "kristallos.ga/rx/math"
	//"github.com/go-gl/gl/v2.1/gl"
	"kristallos.ga/rx/loaders/gltf_test"

	"test/object_renderer/render_backend/backend"
	c "test/object_renderer/render_backend/common"
)

type TestRenderer struct {
	r              *rx.Renderer
	sceneRt        *rx.RenderTarget
	depthRt        *rx.RenderTarget
	
	// XXX
	// Scene ?
	scene *rx.Scene

	// Current mesh / node ?
	// ? current node, for rendering
	mesh *rx.Node

	//// Proc Textures
	// An input texture
	procTex        *rx.Texture
	procTexImg     *image.NRGBA
	// Cropped texture ?
	procTexCrop    *rx.Texture
	procTexCropImg *image.NRGBA
	// XXX ?
	// an output texture / final texture
	//finalTex      *rx.Texture
	procFinalTex    *rx.Texture
	procFinalTexImg *image.NRGBA

	//// Proc Misc ?
	procTexXCorr   int
	procTexYCorr   int

	//// XXX Depth tex
	depthTex    *rx.Texture
	// XXX gray16 (conv) version ?
	depthTexImg *image.Gray16

	// ?
	initialized bool
}

func newTestRenderer() *TestRenderer {
	s := &TestRenderer{
	}

	return s
}

func (s *TestRenderer) init() {
	fmt.Println("TestRenderer init")

	if s.initialized {
		panic(2)
	}

	s.r = rx.Rxi().Renderer
	
	// ? what if the object is off-screen (too big)
	// needs testing
	width, height := s.r.Size()

	fmt.Println("width:", width)
	fmt.Println("height:", height)
	//panic(2)
	
	s.sceneRt = rx.NewRenderTarget("sceneRt", width, height, false) // for dynamic objects
	s.depthRt = rx.NewRenderTarget("depthRt", width, height, true)

	// XXX ?
	// Extra ?

	s.initDefaultScene()

	// ?
	s.initialized = true

	//panic(2)
}

// XXX
// Create default scene
// TODO: add options to not init default scene for example ?
// like in xgui
func (s *TestRenderer) initDefaultScene() {
	fmt.Println("TestRenderer initDefaultScene")

		// XXX this should be in backend ?

	// XXX
	{

		conf := rx.NewTestFwConf()
		//conf.ResX = 1280
		//conf.ResY = 800
		//conf.ResX = 800
		//conf.ResY = 600
		//conf.ResX = 8192
		//conf.ResY = 8192
		//conf.ResX = 10000
		//conf.ResY = 10000
		//conf.ResX = 2048
		//conf.ResY = 2048

		conf.ResX = 512
		conf.ResY = 512

		//panic(3)
	

		//app := rx.TestFwInit()
		//app := rx.TestFwInitConf(conf)
		//_ = app
		sce := rx.TestFwCreateDefaultScene()
		_ = sce
		// XXx
		s.scene = sce
	
		// XXX disable debug ?
		xconf := rx.GetConf()
		_ = xconf
		//xconf.DebugDraw = false

		// ?
	
		/*
		// Iso cam
		cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		//fmt.Println(cn.Camera.Fov())
		//panic(2)
		cn.Camera.SetFov(10)


		gc := newGameCamera()
		Ctx.GameCamera = gc
		gc.setup(cn, Vec3{60 ,0, 45}, 40.0)
		*/

		rxi := rx.Rxi()
		cn := rxi.Camera
		//cn.SetPos(Vec3{12.24745, -12.24745, 10.0})
		mulV := 5.0
		cn.SetPos(Vec3{12.24745*mulV, -12.24745*mulV, 10.0*mulV})
		// Game iso 1:2
		cn.SetRot(Vec3{60, 0, 45})

		// XXX
		fmt.Println(cn.Camera.Fov())
		//panic(2)
		//cn.Camera.SetFov(10)
		//cn.Camera.SetFov(2)
		//cn.Camera.SetFov(4)
		//cn.Camera.SetFov(5)
		//cn.Camera.SetFov(150)

		cn.Camera.SetZnear(0.001)
		cn.Camera.SetZfar(100000.0)
		cn.Camera.SetFov(150)

		////

		// Zoom

		{
			if true {
				nativeResX := 512
				nativeResY := 512
				nativeZoom := 1.0
				_ = nativeResX
				_ = nativeResY
				_ = nativeZoom
	
				zoomV := 1.0

				scale := float64(conf.ResX)/float64(nativeResX)
				scaleZ := 1.0/scale

			
				fmt.Println("scale:", scale)
				fmt.Println("scaleZ:", scaleZ)

				//zoomV = scaleZ
				zoomV = scale
		
				cn.Camera.SetZoom(zoomV)

				//panic(2)
			}

		}
	
		////

		l1 := sce.GetNodeByName("light1")
		//l1.SetPosZ(10)
		//l1.SetPos(Vec3{-2, -15, 2})
		//l1.SetPos(Vec3{-2, -15, 100})
		//l1.SetPos(Vec3{-2, -15, 200})
		l1.SetPos(Vec3{-2, -15, 110})

	}

	////	

	// ?
	//s.initialized = true
}


// XXX
func (s *TestRenderer) GetProcTexImg() *image.NRGBA {
	return s.procTexImg
}

// XXX
func (s *TestRenderer) GetFinalTex() *rx.Texture {
	return s.procFinalTex
}

// XXX
func (s *TestRenderer) GetDepthTex() *rx.Texture {
	return s.depthTex
}

// XXX
func (s *TestRenderer) GetDepthTexImg() *image.Gray16 {
	return s.depthTexImg
}

// ?
func (tr *TestRenderer) GltfDataToMeshData(data []byte) *c.MeshData {
	fmt.Println("TestRenderer GltfDataToMeshData")

	l := gltf_test.NewGltfLoader()
	//ret, _ := l.LoadData(data)
	//ret, ok := l.Load("/tmp/box_textured.glb")
	//ret, ok := l.Load("/tmp/box.glb")
	ret, ok := l.Load("/tmp/triangle.glb")
	if !ok {
		panic("error")
	}

	m := ret[0]
	_ = m
	

	panic("not implemented")
}

// ?
// LoadGltf from path
func (tr *TestRenderer) LoadGltf(path string) {
	fmt.Println("TestRenderer LoadGltf path:", path)

	//pp(2)

	l := gltf_test.NewGltfLoader()

	ret, ok := l.LoadSpawn(path)
	_ = ret

	//ret, _ = l.LoadData(data)
	if !ok {
		panic("error")
	}

	m := ret[0]
	_ = m
	
	// XXX
	tr.mesh = m

	//panic("not implemented")
}

// ?
// LoadGltfFromData
func (tr *TestRenderer) LoadGltfFromData(data []byte) {
	fmt.Println("TestRenderer LoadGltfFromfData")

	//pp(2)

	var ret []*rx.Node

	if false {

		var ok bool

		l := gltf_test.NewGltfLoader()
		//ret, _ := l.LoadData(data)
		//ret, ok := l.Load("/tmp/box_textured.glb")
		//ret, ok := l.Load("/tmp/box.glb")
		//ret, ok := l.Load("/tmp/triangle.glb")
		//ret, ok := l.LoadSpawn("/tmp/triangle.glb")
		//ret, ok := l.LoadSpawn("/tmp/untitled.glb")
		//ret, ok := l.LoadSpawn("/tmp/untitled_scaled.glb")
		ret, ok = l.LoadSpawn("../res1/testdata/cube_100.glb")
		//ret, ok := l.LoadSpawn("../cube_100_z200.glb")
		//ret, ok := l.LoadSpawn("../cube_100_z300.glb")
		if !ok {
			panic("error")
		}
	}

	// XXX test, fixme ?

	if true {
	
		var ok bool
	
		l := gltf_test.NewGltfLoader()

		// XXX write to tmp location

		// XXX
		//path := "/tmp/tmp.glb"
		path := os.TempDir() + "/tmp.glb"
		os.WriteFile(path, data, 0666)

		fmt.Println("tmp path: "+path)
		//panic(2)

		ret, ok = l.LoadSpawn(path)

		//ret, _ = l.LoadData(data)
		if !ok {
			panic("error")
		}
	}

	m := ret[0]
	_ = m
	
	// XXX
	tr.mesh = m

	//panic("not implemented")
}

// ?
// Load mesh data to mesh / scene ?
func (tr *TestRenderer) LoadMeshData(md *c.MeshData) {
	fmt.Println("TestRenderer LoadMeshData")

	//pp(2)

	//pdump(md)

	// XXX
	// Create mesh buffer from mesh data
	mb := rx.NewMeshBuffer()
	mb.Add(md.Positions, md.Normals, md.Texcoords, md.Indices)

	// XXX
	// Create new node
	n2 := rx.NewMeshNode()
	
	// XXX
	// Set data from mesh buffer
	n2.Mesh.LoadFromMeshBuffer(mb, "", "")

	//n2 := rx.GenCubeMeshNode(1, 1, 1)
	//n2 := rx.GenCubeMeshNode(10, 10, 10)

	tr.scene.Add(n2)

	// XXX ?
	// Test set color ?
	var mat *rx.Material
	if true {
		// ?
		mat = rx.NewMaterial("res/materials/base/static.json")
		//mat := rx.NewMaterial("res/materials/base/model.json")
		//mat := rx.NewMaterial("res/materials/base/fill.json")
		mat.Diffuse = Vec3{1, 0, 0}
		//mat.Specular = Vec3{1, 0, 0}
		//mat.Ambient = Vec3{1, 0, 0}
		//mat.Hardness = 100
		// XXX ?
		//mat.SetUnshaded(true)

		n2.Mesh.SetMaterial(mat)

		//n2.Mesh.Material().Diffuse = Vec3{1, 0, 0}

		// ?
		//n2.Mesh.Material().SetUnshaded(true)
		//n2.Mesh.SetMaterial(nil)
	}

	// XXX set actual material color
	if true {
		if md.Color != nil {
			c := *md.Color
			//mat.Diffuse = c
			cv := Vec3{
				float64(c[0]) / 255,
				float64(c[1]) / 255,
				float64(c[2]) / 255,
			}
			mat.Diffuse = cv
		}
	}


	m := n2
	_ = m
	
	// XXX
	tr.mesh = m
}


func (s *TestRenderer) makeSpriteImage(md *c.MeshData, opts MakeSpriteImageOpts, t backend.TransformI) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImage opts:", opts)

	s.doMakeSpriteImage(md, opts, false, t)
}

func (s *TestRenderer) makeSpriteImageDefault(md *c.MeshData, t backend.TransformI) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImageDefault")

	opts := NewMakeSpriteImageOpts()
	s.makeSpriteImage(md, opts, t)
}

// Make image from gltf path
func (tr *TestRenderer) makeSpriteImageDefaultGltf(path string, t backend.TransformI) {
	fmt.Println("TestRenderer makeSpriteImageDefaultGltf path:", path, "t:", t)

	//b.testRenderer.makeSpriteImageDefault(md)

	//md := tr.GltfDataToMeshData(data)
	//b.testRenderer.makeSpriteImageDefault(md)

	tr.LoadGltf(path)
	tr.makeSpriteImageDefault(nil, t)

	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()

}

// Make image from gltf buf ?
func (tr *TestRenderer) makeSpriteImageDefaultGltfFromData(data []byte, t backend.TransformI) {
	fmt.Println("TestRenderer makeSpriteImageDefaultGltfFromData data: [data]", "t:", t)

	//b.testRenderer.makeSpriteImageDefault(md)

	//md := tr.GltfDataToMeshData(data)
	//b.testRenderer.makeSpriteImageDefault(md)

	tr.LoadGltfFromData(data)
	tr.makeSpriteImageDefault(nil, t)

	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()

}

// Make image from mesh data ?
func (tr *TestRenderer) makeSpriteImageDefaultMeshData(md *c.MeshData, t backend.TransformI) {
	fmt.Println("TestRenderer makeSpriteImageDefaultMeshData md: [md]", "t:", t)

	dump(md)

	//b.testRenderer.makeSpriteImageDefault(md)

	//md := tr.GltfDataToMeshData(data)
	//b.testRenderer.makeSpriteImageDefault(md)

	tr.LoadMeshData(md)
	tr.makeSpriteImageDefault(nil, t)

	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()

}


func (s *TestRenderer) makeSpriteDepthImage(md *c.MeshData, opts MakeSpriteImageOpts, t backend.TransformI) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteDepthImage opts:", opts)
	
	s.doMakeSpriteImage(md, opts, true, t)
}

func (s *TestRenderer) makeSpriteDepthImageDefault(md *c.MeshData, t backend.TransformI) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImageDefault")

	opts := NewMakeSpriteImageOpts()
	s.makeSpriteDepthImage(md, opts, t)
}

// ?
func (tr *TestRenderer) makeSpriteDepthImageDefaultGltfData(data []byte, t backend.TransformI) {
	fmt.Println("TestRenderer makeSpriteDepthImageDefaultGltfData data: [data]", "t:", t)

	//b.testRenderer.makeSpriteImageDefault(md)

	//md := tr.GltfDataToMeshData(data)
	//b.testRenderer.makeSpriteImageDefault(md)

	tr.LoadGltfFromData(data)
	tr.makeSpriteDepthImageDefault(nil, t)

	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()

}

// TODO: make smaller (non-fullscreen) rt, like 512x512 ?
func (s *TestRenderer) doMakeSpriteImage(md *c.MeshData,
	opts MakeSpriteImageOpts, depthMode bool,
	t backend.TransformI) {
	
	fmt.Println("[ul] SpriteMeshSys doMakeSpriteImage opts:", opts, "depthMode:",  depthMode,
		"t:", t)

	//panic(2)

	/*
	{
		if opts.DepthMode {
			pp("depth mode not supported")
		}
	}
	*/

	{
		if depthMode {
			//pp("depth mode not supported")
		}
	}

	// Focus on model
	// XXX should be first
	if !opts.NoCameraFocus {
		//Ctx.GameCamera.panToPoint(sm.meshNode.Pos())
		//panic("not implemented ?")
	} else {
		fmt.Println("warning: not focusing camera on the object")
	}
	
	// XXX update transform ?
	if true {
		newPos := Vec3{t.Pos()[0], t.Pos()[1], t.Pos()[2]}
		newRot := Vec3{t.Rot()[0], t.Rot()[1], t.Rot()[2]}
		newScale := Vec3{t.Scale()[0], t.Scale()[1], t.Scale()[2]}
		s.mesh.SetPos(newPos)
		s.mesh.SetRot(newRot)
		s.mesh.SetScale(newScale)
	}

	// Render dynamic objects to fbo
	//
	s.sceneRt.Bind()
	//s.depthRt.Bind()

	//gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	s.r.Clear()

	//cam := s.r.CurCam()
	cam := rx.Rxi().Camera
	s.r.SetCamera(cam)

	//s.r.SetActiveNode(sm.meshNode)

	var _ = `
	// XXX
	newMn := CreateMeshNodeFromMeshData(md)
	//newMn.Spawn()
	s.r.SetActiveNode(newMn)
	`
	s.r.SetActiveNode(s.mesh)
	
	//s.r.RenderAll()
	s.r.RenderScene()
	//s.r.SetActiveNode(nil)

	s.sceneRt.Unbind()


	s.depthRt.Bind()

	s.r.Clear()
	
	s.r.RenderScene()
	
	s.depthRt.Unbind()

	// XXX finally
	s.r.SetActiveNode(nil)
	
	//// Set proc tex
	s.procTex = s.sceneRt.Framebuffer().Tex()
	s.procTexImg = s.procTex.GetTextureDataAsImage(true).(*image.NRGBA)
	//s.procTexImg = rx.

	// XXX Debug
	s.procTex.Save(GetTmpDir() + "/ul__procTex.png")
	rx.SaveImage(s.procTexImg, GetTmpDir() + "/ul__procTexImg.png", false)

	//// Set depth tex
	s.depthTex = s.depthRt.Framebuffer().Tex()
	// XXX depth16conv version ?
	s.depthTexImg = s.depthTex.GetTextureDataDepth16ConvAsImage(true).(*image.Gray16)

	// XXX test

	rawDataI := s.depthTex.GetTextureData()
	_ = rawDataI

	rawData := rawDataI.([]uint32)

	println("len:", len(rawData))

	//var buf *bytes.Buffer = new(bytes.Buffer)

	f, err  := os.Create(GetTmpDir() + "/out_raw.data")
	if err != nil {
		panic(err)
	}
	
	//err := binary.Write(buf, binary.LittleEndian, rawData)
	err = binary.Write(f, binary.LittleEndian, rawData)
	if err != nil {
		panic(err)
	}

	f.Close()

	/*
	err = ioutil.WriteFile("/tmp/output.txt", buf, 0644)
	if err != nil {
		panic(err)
	}
	*/

	s.depthTex.SaveDepth16Conv(GetTmpDir() + "/out_depth16.png")

	//pp(2)

	// XXX Debug
	//s.depthTex.Save("/tmp/ul__depthTex.png")
	//rx.SaveImage(s.depthTexImg, "/tmp/ul__depthTexImg.png", false)

	//s.procData()
	//s.procData(opts)
	
	//s.procData(opts.Crop, sm.renW, sm.renH)
	renW := 512
	renH := 512
	s.procData(opts.Crop, renW, renH)

	// XXX finally ?
	//s.finalTex = 

	//pp(2)
}



//func (s *SpriteMeshSys) procData(opts MakeSpriteImageOpts) {
func (s *TestRenderer) procData(crop bool, renW, renH int) {
	//fmt.Println("SpriteMeshSys procData opts:", opts)
	fmt.Println("[ul] SpriteMeshSys procData crop:", crop)

	// ???
	//if !opts.Crop {
	if !crop {
		//fmt.Println("...return")
		//return
	}

	//if opts.DepthMode {
	//	panic("not supported")
	//}

	im := s.procTexImg
	w := im.Rect.Dx()
	h := im.Rect.Dy()

	// XXX do crop
	if crop {

		// Find image extents
		minX, maxX, 
		minY, maxY := imUtilGetImageExtents(im)

		p("minX:", minX)
		p("maxX:", maxX)
		p("minY:", minY)
		p("maxY:", maxY)
		//pp(2)

		if maxX == 0 || maxY == 0 {
			// Empty image?
			pp("error: bad image: maxX:", maxX, "maxY:", maxY)
		}

		cropIm, cropW, cropH := imUtilCropImageToContext(im)

		s.procTexCropImg = cropIm

		// Update yCorr
		yCorr := (h / 2) - minY
		//pp(yCorr)
		s.procTexYCorr = yCorr

		xCorr := (w / 2) - minX
		//pp(xCorr)
		s.procTexXCorr = xCorr

		p("xCorr:", xCorr)
		p("yCorr:", yCorr)

		/*
			b := cropIm.Bounds()
			rgba := image.NewRGBA(b)
			if rgba.Stride != rgba.Rect.Size().X*4 {
				panic("unsupported stride")
			}
			draw.Draw(rgba, rgba.Bounds(), cropIm, b.Min, draw.Src)
			saveImage(rgba, "/tmp/x1.png")

			data_raw := rgba.Pix
			_ = data_raw
		*/

		s.procTexCrop = rx.NewEmptyTexture(cropW, cropH, rx.TextureFormatRGBA)
		s.procTexCrop.SetTextureDataFromImage(cropIm, true)
	}

	//// XXX debug

	if crop {
		if true {
			fmt.Println("Save debug proc crop textures...")
			s.procTexCrop.Save(GetTmpDir() + "/ul__procTexCrop.png")
			rx.SaveImage(s.procTexCropImg, GetTmpDir() + "/ul__procTexCropImg.png", false)
		}
	}

	//// XXX set final tex ?

	// Finally
	
	
	if crop {
		s.procFinalTex = s.procTexCrop
		s.procFinalTexImg = s.procTexCropImg
	} else {
		s.procFinalTex = s.procTex
		s.procFinalTexImg = s.procTexImg

	}
	
	// Crop final image to size ?

	 if true {
	//if false {
		//var finalTex *rx.Texture
		//var finalTexImg *image.NRGBA
		
		var _ = `
		// Find image extents
		minX, maxX, 
		minY, maxY := s.getImageExtents(im)

		p("w:", w)
		p("h:", h)
		p("minX:", minX)
		p("maxX:", maxX)
		p("minY:", minY)
		p("maxY:", maxY)

		maxW := int(math.Max(math.Abs(float64(w/2 - minX)), math.Abs(float64(w/2 - maxX)))) * 2
		maxH := int(math.Max(math.Abs(float64(h/2 - minY)), math.Abs(float64(h/2 - maxY)))) * 2

		p("maxW:", maxW)
		p("maxH:", maxH)

		//pp(2)

		im2 := s.cropImageToSizeFromCenter(s.procFinalTexImg, maxW, maxH)
		SaveImage(im2, GetTmpDir() + "/x2.png")

		pp(2)

		finalTex := rx.NewEmptyTexture(maxW, maxH, rx.TextureFormatRGBA)
		s.procTexCrop.SetTextureDataFromImage(im2, true)

		`

		p("renW:", renW)
		p("renH:", renH)

		im2 := imUtilCropImageToSizeFromCenter(s.procFinalTexImg, renW, renH)
		SaveImage(im2, GetTmpDir() + "/x2.png")

		//pp(2)

		tex := rx.NewEmptyTexture(renW, renH, rx.TextureFormatRGBA)
		tex.SetTextureDataFromImage(im2, true)

		//pp(2)

		// Update, fixme ?
		s.procFinalTex = tex
		s.procFinalTexImg = im2

		//pp(2)
	}
}