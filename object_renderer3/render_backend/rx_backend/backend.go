package rx_backend

import (
	"fmt"
	"image"

	"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"

	// ?
	// c0
	// cbase ?
	// base common ?
	//c0 "test/common"
	//cbase "test/common"
	c_base "test/common"

	//"object_renderer/backend"
	"test/object_renderer/render_backend/backend"
	c "test/object_renderer/render_backend/common"
)

//var b *Backend

type Backend struct {
	testRenderer *TestRenderer
}

//func NewBackend() *Backend {
func NewBackend(rxi *rx.Rx) *Backend {
	fmt.Println("rx_backend: NewBackend()")

	if rxi == nil {
		pp("rxi is nil")
	}

	b := &Backend{
		testRenderer: newTestRenderer(),
	}

	return b
}

func (b *Backend) Init() {
	fmt.Println("rx_backend: Backend Init()")

	// ?
	b.testRenderer.init()

	// XXX
	if true {
		c_base.PrepareTmpDir()
	}
}


var _ = `
func (b *Backend) RenderObject(data []byte, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObject len data:", len(data), "t:", t)

	md := b.testRenderer.GltfDataToMeshData(data)
	b.testRenderer.makeSpriteImageDefault(md)

	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	im := b.testRenderer.GetProcTexImg()

	return im
}
`


func (b *Backend) RenderObject(path string, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObject path:", path, "t:", t)

	//b.testRenderer.makeSpriteImageDefaultGltfFromData(data, t)
	b.testRenderer.makeSpriteImageDefaultGltf(path, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	im := b.testRenderer.GetProcTexImg()

	return im
}

func (b *Backend) RenderObjectFromData(data []byte, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectFromData len data:", len(data), "t:", t)

	b.testRenderer.makeSpriteImageDefaultGltfFromData(data, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	im := b.testRenderer.GetProcTexImg()

	return im
}

func (b *Backend) RenderObjectToPath(path string, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectToPath path:", path, "t:", t, "outPath:", outPath)

	im := b.RenderObject(path, t)

	SavePng(im, outPath)
}

func (b *Backend) RenderObjectFromDataToPath(data []byte, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectFromDataToPath len data:", len(data), "t:", t, "outPath:", outPath)

	im := b.RenderObjectFromData(data, t)

	SavePng(im, outPath)
}

//// Depth

func (b *Backend) RenderObjectDepth(path string, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectDepth path:", path, "t:", t)

	b.testRenderer.makeSpriteImageDefaultGltf(path, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()
	im := b.testRenderer.GetDepthTexImg()

	return im
}

func (b *Backend) RenderObjectDepthFromData(data []byte, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectDepthFromData len data:", len(data), "t:", t)

	b.testRenderer.makeSpriteImageDefaultGltfFromData(data, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()
	im := b.testRenderer.GetDepthTexImg()

	return im
}

func (b *Backend) RenderObjectDepthToPath(path string, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectDepthToPath path:", path, "t:", t, "outPath:", outPath)

	im := b.RenderObjectDepth(path, t)

	SavePng(im, outPath)
}

func (b *Backend) RenderObjectDepthFromDataToPath(data []byte, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectDepthFromDataToPath len data:", len(data), "t:", t, "outPath:", outPath)

	im := b.RenderObjectDepthFromData(data, t)

	SavePng(im, outPath)
}

//// Mesh Data

func (b *Backend) RenderObjectMeshData(md *c.MeshData, t backend.TransformI) image.Image {
	//panic("not implemented")
	
	fmt.Println("Backend RenderObjectMeshData md: [md]", "t:", t)

	b.testRenderer.makeSpriteImageDefaultMeshData(md, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	im := b.testRenderer.GetProcTexImg()

	return im
}

func (b *Backend) RenderObjectMeshDataToPath(md *c.MeshData, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectMeshDataToPath md: [md]",  "t:", t, "outPath:", outPath)

	im := b.RenderObjectMeshData(md, t)

	SavePng(im, outPath)
}

func (b *Backend) RenderObjectMeshDataDepth(md *c.MeshData, t backend.TransformI) image.Image {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectMeshDataDepth md: [md]", "t:", t)

	b.testRenderer.makeSpriteImageDefaultMeshData(md, t)
	// XXX
	// ? fixme ?
	//im := b.testRenderer.GetFinalTex()
	//im := b.testRenderer.GetProcTexImg()
	im := b.testRenderer.GetDepthTexImg()

	return im
}

func (b *Backend) RenderObjectMeshDataDepthToPath(md *c.MeshData, t backend.TransformI, outPath string) {
	//panic("not implemented")

	fmt.Println("Backend RenderObjectMeshDataToPath md: [md]",  "t:", t, "outPath:", outPath)

	im := b.RenderObjectMeshDataDepth(md, t)

	SavePng(im, outPath)
}


func (b *Backend) RenderHeightmap(hmData interface{}) image.Image {
	panic("not implemented")
}

func (b *Backend) RenderHeightmapDepth(hmData interface{}) image.Image {
	panic("not implemented")
}

func (b *Backend) RenderHeightmapTexture(hmData interface{}, tex image.Image) image.Image {
	panic("not implemented")
}

//// Extra ?

func (b *Backend) GenCubeMeshData(sx, sy, sz float64) *c.MeshData {

	fmt.Println("Backend GenCubeMeshData sx: sx",  "sy:", sy, "sz:", sz)
	
	//panic("not implemented")

	gmd := rx.GenCubeMeshData(sx, sy, sz)

	md := b.meshDataFromGenMeshData(gmd)

	return md
}

func (b *Backend) GetObjectBoundingBox(path string) [6]float64 {

	fmt.Println("Backend GetObjectBoundingBox path:", path)
	
	//panic("not implemented")

	// XXX load mesh

	b.testRenderer.LoadGltf(path)
	// XXX
	mesh := b.testRenderer.mesh

	bbox := mesh.Mesh.GetAABB()

	//fmt.Println("bbox:", bbox)

	//panic(2)

	var ret [6]float64

	min := bbox.Min()
	max := bbox.Max()
	
	ret[0] = min[0]
	ret[1] = min[1]
	ret[2] = min[2]
	ret[3] = max[0]
	ret[4] = max[1]
	ret[5] = max[2]

	return ret
}

// Convert GenMeshData to MeshData
// util ?
func (b *Backend) meshDataFromGenMeshData(gmd *rx.GenMeshData) *c.MeshData {

	md := c.NewMeshData()
	// XXX ?
	md.Positions = gmd.Positions
	md.Normals = gmd.Normals
	md.Texcoords = gmd.Texcoords
	md.Indices = gmd.Indices

	return md
}

// XXX

/*
func (b *Backend) Draw3dLine(p1, p2 [3]float64, color [3]int) {
	panic("not implemented")
}

func (b *Backend) Draw3dGrid(pos [3]float64, w, h int, cellSize float64, color [3]int, drawOrigin bool) {
	grid := rx.NewGridHelper()
	//grid.Color = Vec3{0.2, 0.3, 0.8}
	//grid.Color = Vec3{1.0, 1.0, 1.0}
	grid.Color = Vec3{1.0, 0.0, 0.0}
	grid.Width = 200
	grid.Height = 200
	//grid.CellSize = 2
	grid.CellSize = 1
	//grid.CellSize = 10
	grid.DrawOrigin = true
	grid.Spawn()

}
*/
