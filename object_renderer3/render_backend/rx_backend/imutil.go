package rx_backend

import (
	"fmt"
	"image"
	"math"
)

// util
//func (s *TestRenderer) getImageExtents(im *image.NRGBA) (int, int, int, int) {
func imUtilGetImageExtents(im *image.NRGBA) (int, int, int, int) {
	w := im.Rect.Dx()
	h := im.Rect.Dy()

	// Find image extents
	minX, maxX := math.MaxInt32, 0
	minY, maxY := math.MaxInt32, 0
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//p("->", x, y)
			c := im.NRGBAAt(x, y)
			if c.A != 0 {
				if x < minX {
					minX = x
				}
				if x > maxX {
					maxX = x
				}
				if y < minY {
					minY = y
				}
				if y > maxY {
					maxY = y
				}
			}
		}
	}

	p("minX:", minX)
	p("maxX:", maxX)
	p("minY:", minY)
	p("maxY:", maxY)
	//pp(2)

	return minX, maxX, minY, maxY
}

// util
//func (s *TestRenderer) cropImageToContext(im *image.NRGBA) (*image.NRGBA, int, int) {
func imUtilCropImageToContext(im *image.NRGBA) (*image.NRGBA, int, int) {
	fmt.Println("cropImageToContext")

	// Find image extents
	minX, maxX, 
	//minY, maxY := s.getImageExtents(im)
	minY, maxY := imUtilGetImageExtents(im)

	p("minX:", minX)
	p("maxX:", maxX)
	p("minY:", minY)
	p("maxY:", maxY)
	//pp(2)

	if maxX == 0 || maxY == 0 {
		// Empty image?
		pp("error: bad image: maxX:", maxX, "maxY:", maxY)
	}

	// Crop
	cropW := maxX - minX
	cropH := maxY - minY
	//cropIm := image.NewNRGBA(image.Rect(0, 0, cropW, cropH))
	cropIm := im.SubImage(image.Rect(minX, minY, maxX, maxY)).(*image.NRGBA)
	SaveImage(cropIm, GetTmpDir() + "/x.png")

	return cropIm, cropW, cropH
}

// Crop image from center
// util
//func (s *TestRenderer) cropImageToSizeFromCenter(im *image.NRGBA, w, h int) *image.NRGBA {
func imUtilCropImageToSizeFromCenter(im *image.NRGBA, w, h int) *image.NRGBA {
	fmt.Println("cropImageToSizeFromCenter w:", w, "h:", h)
	
	bounds := im.Bounds()

	width := bounds.Dx()
	height := bounds.Dy()
	fmt.Println("im width:", width)
	fmt.Println("im height:", height)
	//cropSize := image.Rect(0, 0, w-100, h)
	cropSize := image.Rect(0, 0, w, h)
	// XXX full rect ?
	//cropSize := image.Rect((width/2) - (w/2), (height/2) - (h/2), w, h)
	fmt.Println("x cropSize:", cropSize)
	//cropSize = cropSize.Add(image.Point{width/2, height/2})
	//cropSize = cropSize.Add(image.Point{w/2, h/2})
	//cropSize = cropSize.Add(image.Point{100, 0})
	//cropSize = cropSize.Add(image.Point{width/2-w, height/2-h})
	//cropSize = cropSize.Add(image.Point{-(width/2), -(height/2)})
	cropSize = cropSize.Add(image.Point{(width/2) - (w/2), (height/2) - (h/2)})
	fmt.Println("cropSize:", cropSize)
	//var si image.SubImage	
	//si = im
	//cropIm := si.SubImage(cropSize)
	cropIm := im.SubImage(cropSize)
	
	// fixme ?
	ret := cropIm.(*image.NRGBA)
	
	return ret
}
