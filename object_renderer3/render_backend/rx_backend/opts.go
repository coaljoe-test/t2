package rx_backend

import (
	"fmt"
)

type MakeSpriteImageOpts struct {
	//ColorMode bool
	// Render in depth mode ?
	DepthMode bool
	// Crop to content
	Crop bool
	// Don't focus / pan camera on the mesh
	NoCameraFocus bool
}

// XXX Default ?
func NewMakeSpriteImageOpts() MakeSpriteImageOpts {
	return MakeSpriteImageOpts{
		//ColorMode: true,
		DepthMode: false,
		Crop: false,
		//Crop: true,
		NoCameraFocus: false,
	}
}

// XXX fixme ?
// XXX just use s := fmt.Sprinf("%#v", opts) where it is needed ?
func (opts MakeSpriteImageOpts) String() string {
	s := "MakeSpriteImageOpts{"
	s += fmt.Sprintf("DepthMode: %v, ", opts.DepthMode)
	s += fmt.Sprintf("Crop: %v, ", opts.Crop)
	s += fmt.Sprintf("NoCameraFocus: %v}", opts.NoCameraFocus)
	return s
}
