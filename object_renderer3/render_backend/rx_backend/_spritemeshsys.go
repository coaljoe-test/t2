package ulexite

// TODO: use custom cameras instead of game cameras ?

import (
	"fmt"
	"image"
	"math"
	//"image/draw"
	"encoding/binary"
	//"io/ioutil"
	//"bytes"
	"os"

	"kristallos.ga/rx"
	//. "kristallos.ga/rx/math"
	//"github.com/go-gl/gl/v2.1/gl"
)

type SpriteMeshSys struct {
	*GenSystem[*SpriteMeshNode]
	r              *rx.Renderer
	sceneRt        *rx.RenderTarget
	depthRt        *rx.RenderTarget

	//// Proc Textures
	// An input texture
	procTex        *rx.Texture
	procTexImg     *image.NRGBA
	// Cropped texture ?
	procTexCrop    *rx.Texture
	procTexCropImg *image.NRGBA
	// XXX ?
	// an output texture / final texture
	//finalTex      *rx.Texture
	procFinalTex    *rx.Texture
	procFinalTexImg *image.NRGBA

	//// Proc Misc ?
	procTexXCorr   int
	procTexYCorr   int

	//// XXX Depth tex
	depthTex    *rx.Texture
	// XXX gray16 (conv) version ?
	depthTexImg *image.Gray16
}

func newSpriteMeshSys() *SpriteMeshSys {
	s := &SpriteMeshSys{
		GenSystem: newGenSystem[*SpriteMeshNode](),
	}

	return s
}

func (s *SpriteMeshSys) init() {
	s.r = rx.Rxi().Renderer
	
	// ? what if the object is off-screen (too big)
	// needs testing
	width, height := s.r.Size()
	
	s.sceneRt = rx.NewRenderTarget("sceneRt", width, height, false) // for dynamic objects
	s.depthRt = rx.NewRenderTarget("depthRt", width, height, true)
}

func (s *SpriteMeshSys) add(el *SpriteMeshNode) {
	if s.has(el) {
		pp("already have elem: el:", el)
	}
	s.addElem(el)
}

func (s *SpriteMeshSys) remove(el *SpriteMeshNode) {
	if !s.has(el) {
		pp("don't have such elem: sm:", el)
	}
}

func (s *SpriteMeshSys) has(el *SpriteMeshNode) bool {
	return s.hasElem(el)
}

// XXX
func (s *SpriteMeshSys) GetProcTexImg() *image.NRGBA {
	return s.procTexImg
}

// XXX
func (s *SpriteMeshSys) GetFinalTex() *rx.Texture {
	return s.procFinalTex
}

// XXX
func (s *SpriteMeshSys) GetDepthTex() *rx.Texture {
	return s.depthTex
}

// XXX
func (s *SpriteMeshSys) GetDepthTexImg() *image.Gray16 {
	return s.depthTexImg
}


func (s *SpriteMeshSys) makeSpriteImage(sm *SpriteMeshNode, opts MakeSpriteImageOpts) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImage opts:", opts)

	s.doMakeSpriteImage(sm, opts, false)
}

func (s *SpriteMeshSys) makeSpriteImageDefault(sm *SpriteMeshNode) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImageDefault")

	opts := NewMakeSpriteImageOpts()
	s.makeSpriteImage(sm, opts)
}

func (s *SpriteMeshSys) makeSpriteDepthImage(sm *SpriteMeshNode, opts MakeSpriteImageOpts) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteDepthImage opts:", opts)
	
	s.doMakeSpriteImage(sm, opts, true)
}

func (s *SpriteMeshSys) makeSpriteDepthImageDefault(sm *SpriteMeshNode) {
	fmt.Println("[ul] SpriteMeshSys makeSpriteImageDefault")

	opts := NewMakeSpriteImageOpts()
	s.makeSpriteDepthImage(sm, opts)
}

// TODO: make smaller (non-fullscreen) rt, like 512x512 ?
func (s *SpriteMeshSys) doMakeSpriteImage(sm *SpriteMeshNode,
	opts MakeSpriteImageOpts, depthMode bool) {
	
	fmt.Println("[ul] SpriteMeshSys doMakeSpriteImage opts:", opts, "depthMode:",  depthMode)

	//panic(2)

	/*
	{
		if opts.DepthMode {
			pp("depth mode not supported")
		}
	}
	*/

	{
		if depthMode {
			//pp("depth mode not supported")
		}
	}

	// Focus on model
	// XXX should be first
	if !opts.NoCameraFocus {
		Ctx.GameCamera.panToPoint(sm.meshNode.Pos())
	} else {
		fmt.Println("warning: not focusing camera on the object")
	}

	// Render dynamic objects to fbo
	//
	s.sceneRt.Bind()
	//s.depthRt.Bind()

	//gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
	s.r.Clear()

	//cam := s.r.CurCam()
	cam := rx.Rxi().Camera
	s.r.SetCamera(cam)

	s.r.SetActiveNode(sm.meshNode)
	//s.r.RenderAll()
	s.r.RenderScene()
	//s.r.SetActiveNode(nil)

	s.sceneRt.Unbind()


	s.depthRt.Bind()

	s.r.Clear()
	
	s.r.RenderScene()
	
	s.depthRt.Unbind()

	// XXX finally
	s.r.SetActiveNode(nil)
	
	//// Set proc tex
	s.procTex = s.sceneRt.Framebuffer().Tex()
	s.procTexImg = s.procTex.GetTextureDataAsImage(true).(*image.NRGBA)
	//s.procTexImg = rx.

	// XXX Debug
	s.procTex.Save("/tmp/ul__procTex.png")
	rx.SaveImage(s.procTexImg, "/tmp/ul__procTexImg.png", false)

	//// Set depth tex
	s.depthTex = s.depthRt.Framebuffer().Tex()
	// XXX depth16conv version ?
	s.depthTexImg = s.depthTex.GetTextureDataDepth16ConvAsImage(true).(*image.Gray16)

	// XXX test

	rawDataI := s.depthTex.GetTextureData()
	_ = rawDataI

	rawData := rawDataI.([]uint32)

	println("len:", len(rawData))

	//var buf *bytes.Buffer = new(bytes.Buffer)

	f, err  := os.Create("/tmp/out_raw.data")
	if err != nil {
		panic(err)
	}
	
	//err := binary.Write(buf, binary.LittleEndian, rawData)
	err = binary.Write(f, binary.LittleEndian, rawData)
	if err != nil {
		panic(err)
	}

	f.Close()

	/*
	err = ioutil.WriteFile("/tmp/output.txt", buf, 0644)
	if err != nil {
		panic(err)
	}
	*/

	s.depthTex.SaveDepth16Conv("/tmp/out_depth16.png")

	//pp(2)

	// XXX Debug
	//s.depthTex.Save("/tmp/ul__depthTex.png")
	//rx.SaveImage(s.depthTexImg, "/tmp/ul__depthTexImg.png", false)

	//s.procData()
	//s.procData(opts)
	s.procData(opts.Crop, sm.renW, sm.renH)

	// XXX finally ?
	//s.finalTex = 

	//pp(2)
}



//func (s *SpriteMeshSys) procData(opts MakeSpriteImageOpts) {
func (s *SpriteMeshSys) procData(crop bool, renW, renH int) {
	//fmt.Println("SpriteMeshSys procData opts:", opts)
	fmt.Println("[ul] SpriteMeshSys procData crop:", crop)

	// ???
	//if !opts.Crop {
	if !crop {
		//fmt.Println("...return")
		//return
	}

	//if opts.DepthMode {
	//	panic("not supported")
	//}

	im := s.procTexImg
	w := im.Rect.Dx()
	h := im.Rect.Dy()

	// XXX do crop
	if crop {

		// Find image extents
		minX, maxX, 
		minY, maxY := s.getImageExtents(im)

		p("minX:", minX)
		p("maxX:", maxX)
		p("minY:", minY)
		p("maxY:", maxY)
		//pp(2)

		if maxX == 0 || maxY == 0 {
			// Empty image?
			pp("error: bad image: maxX:", maxX, "maxY:", maxY)
		}

		cropIm, cropW, cropH := s.cropImageToContext(im)

		s.procTexCropImg = cropIm

		// Update yCorr
		yCorr := (h / 2) - minY
		//pp(yCorr)
		s.procTexYCorr = yCorr

		xCorr := (w / 2) - minX
		//pp(xCorr)
		s.procTexXCorr = xCorr

		p("xCorr:", xCorr)
		p("yCorr:", yCorr)

		/*
			b := cropIm.Bounds()
			rgba := image.NewRGBA(b)
			if rgba.Stride != rgba.Rect.Size().X*4 {
				panic("unsupported stride")
			}
			draw.Draw(rgba, rgba.Bounds(), cropIm, b.Min, draw.Src)
			saveImage(rgba, "/tmp/x1.png")

			data_raw := rgba.Pix
			_ = data_raw
		*/

		s.procTexCrop = rx.NewEmptyTexture(cropW, cropH, rx.TextureFormatRGBA)
		s.procTexCrop.SetTextureDataFromImage(cropIm, true)
	}

	//// XXX debug

	if crop {
		if true {
			fmt.Println("Save debug proc crop textures...")
			s.procTexCrop.Save("/tmp/ul__procTexCrop.png")
			rx.SaveImage(s.procTexCropImg, "/tmp/ul__procTexCropImg.png", false)
		}
	}

	//// XXX set final tex ?

	// Finally
	
	
	if crop {
		s.procFinalTex = s.procTexCrop
		s.procFinalTexImg = s.procTexCropImg
	} else {
		s.procFinalTex = s.procTex
		s.procFinalTexImg = s.procTexImg

	}
	
	// Crop final image to size ?

	 if true {
	//if false {
		//var finalTex *rx.Texture
		//var finalTexImg *image.NRGBA
		
		var _ = `
		// Find image extents
		minX, maxX, 
		minY, maxY := s.getImageExtents(im)

		p("w:", w)
		p("h:", h)
		p("minX:", minX)
		p("maxX:", maxX)
		p("minY:", minY)
		p("maxY:", maxY)

		maxW := int(math.Max(math.Abs(float64(w/2 - minX)), math.Abs(float64(w/2 - maxX)))) * 2
		maxH := int(math.Max(math.Abs(float64(h/2 - minY)), math.Abs(float64(h/2 - maxY)))) * 2

		p("maxW:", maxW)
		p("maxH:", maxH)

		//pp(2)

		im2 := s.cropImageToSizeFromCenter(s.procFinalTexImg, maxW, maxH)
		SaveImage(im2, "/tmp/x2.png")

		pp(2)

		finalTex := rx.NewEmptyTexture(maxW, maxH, rx.TextureFormatRGBA)
		s.procTexCrop.SetTextureDataFromImage(im2, true)

		`

		p("renW:", renW)
		p("renH:", renH)

		im2 := s.cropImageToSizeFromCenter(s.procFinalTexImg, renW, renH)
		SaveImage(im2, "/tmp/x2.png")

		//pp(2)

		tex := rx.NewEmptyTexture(renW, renH, rx.TextureFormatRGBA)
		tex.SetTextureDataFromImage(im2, true)

		//pp(2)

		// Update, fixme ?
		s.procFinalTex = tex
		s.procFinalTexImg = im2

		//pp(2)
	}
}

// util
func (s *SpriteMeshSys) getImageExtents(im *image.NRGBA) (int, int, int, int) {
	w := im.Rect.Dx()
	h := im.Rect.Dy()

	// Find image extents
	minX, maxX := math.MaxInt32, 0
	minY, maxY := math.MaxInt32, 0
	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//p("->", x, y)
			c := im.NRGBAAt(x, y)
			if c.A != 0 {
				if x < minX {
					minX = x
				}
				if x > maxX {
					maxX = x
				}
				if y < minY {
					minY = y
				}
				if y > maxY {
					maxY = y
				}
			}
		}
	}

	p("minX:", minX)
	p("maxX:", maxX)
	p("minY:", minY)
	p("maxY:", maxY)
	//pp(2)

	return minX, maxX, minY, maxY
}

// util
func (s *SpriteMeshSys) cropImageToContext(im *image.NRGBA) (*image.NRGBA, int, int) {
	fmt.Println("cropImageToContext")

	// Find image extents
	minX, maxX, 
	minY, maxY := s.getImageExtents(im)

	p("minX:", minX)
	p("maxX:", maxX)
	p("minY:", minY)
	p("maxY:", maxY)
	//pp(2)

	if maxX == 0 || maxY == 0 {
		// Empty image?
		pp("error: bad image: maxX:", maxX, "maxY:", maxY)
	}

	// Crop
	cropW := maxX - minX
	cropH := maxY - minY
	//cropIm := image.NewNRGBA(image.Rect(0, 0, cropW, cropH))
	cropIm := im.SubImage(image.Rect(minX, minY, maxX, maxY)).(*image.NRGBA)
	SaveImage(cropIm, "/tmp/x.png")

	return cropIm, cropW, cropH
}

// Crop image from center
// util
func (s *SpriteMeshSys) cropImageToSizeFromCenter(im *image.NRGBA, w, h int) *image.NRGBA {
	fmt.Println("cropImageToSizeFromCenter w:", w, "h:", h)
	
	bounds := im.Bounds()

	width := bounds.Dx()
	height := bounds.Dy()
	fmt.Println("im width:", width)
	fmt.Println("im height:", height)
	//cropSize := image.Rect(0, 0, w-100, h)
	cropSize := image.Rect(0, 0, w, h)
	// XXX full rect ?
	//cropSize := image.Rect((width/2) - (w/2), (height/2) - (h/2), w, h)
	fmt.Println("x cropSize:", cropSize)
	//cropSize = cropSize.Add(image.Point{width/2, height/2})
	//cropSize = cropSize.Add(image.Point{w/2, h/2})
	//cropSize = cropSize.Add(image.Point{100, 0})
	//cropSize = cropSize.Add(image.Point{width/2-w, height/2-h})
	//cropSize = cropSize.Add(image.Point{-(width/2), -(height/2)})
	cropSize = cropSize.Add(image.Point{(width/2) - (w/2), (height/2) - (h/2)})
	fmt.Println("cropSize:", cropSize)
	//var si image.SubImage	
	//si = im
	//cropIm := si.SubImage(cropSize)
	cropIm := im.SubImage(cropSize)
	
	// fixme ?
	ret := cropIm.(*image.NRGBA)
	
	return ret
}

// XXX fixme?
func (s *SpriteMeshSys) render() {
	//panic("not implemented")
	for _, sm := range s.elems {
		sm.Render()
	}
}

func (s *SpriteMeshSys) update(dt float64) {
	for _, sm := range s.elems {
		sm.Update(dt)
	}
}
