package backend

import (
	"image"
	"io"
	c "render_backend/common"
)

// Render/object backend
type RenderBackendI interface {
	// ?
	RenderBackendExtI

	// api / system
	// ?
	RenderObject(ob interface{}) image.Image
	RenderObjectDepth(ob interface{}) image.Image

	// ?
	RenderHeightmap(hmData interface{}) image.Image
	// Textured ?
	RenderHeightmapTexture(hmData interface{}, tex image.Image) image.Image
	// See also:
	// https://github.com/anoved/hmstl

	// ?
	RenderHeightmapDepth(hmData interface{}) image.Image

	// extra ?
	// Get mesh voxels ?
	//VoxelizeMesh(mesh interface{}) VoxelData

	// ?
	RenderObjectOpt(ob interface{}) image.Image

	// ?
	RenderObjectMeshData(md *c.MeshData) image.Image

	// ?
	// Render object from data ?
	RenderObjectData(buf []byte) image.Image
	
	// ?
	// From memory / buffer ?
	RenderObjectData2(f io.Reader) image.Image
}

// Extra ?
type RenderBackendExtI interface {
	GenCube(size int)
	GenCubeMeshData(size int) c.MeshData

	// ?
	RenderCube(size int)
	RenderCubeDepth(size int)
}
