package backend

type TransformI interface {
	// Get pos ?
	Pos() [3]float64
	SetPos(v [3]float64)
	// Get rot ?
	Rot() [3]float64
	SetRot(v [3]float64)
	// Get scale ?
	Scale() [3]float64
	SetScale(v [3]float64)
}
