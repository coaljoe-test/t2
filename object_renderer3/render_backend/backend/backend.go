package backend

import (
	"image"
	c "test/object_renderer/render_backend/common"
)

// Version 0.1

// Render/object backend
type RenderBackendI interface {
	// ?
	RenderBackendExtI

	// api / system
	// ?
	//RenderObject(ob interface{}) image.Image
	//RenderObjectDepth(ob interface{}) image.Image
	// ? use gltf data bytes ?
	// From gltf data
	//RenderObject(data []byte, transform TransformI) image.Image
	//RenderObjectDepth(data []byte, transform TransformI) image.Image

	RenderObject(path string, transform TransformI) image.Image
	RenderObjectDepth(path string, transform TransformI) image.Image

	RenderObjectFromData(data []byte, transform TransformI) image.Image
	RenderObjectDepthFromData(data []byte, transform TransformI) image.Image

	// extra ?
	// XXX add filetype / ext ?
	//RenderObjectToPath(data []byte, transform TransformI, outPath string)
	//RenderObjectDepthToPath(data []byte, transform TransformI, outPath string)

	RenderObjectToPath(path string, transform TransformI, outPath string)
	RenderObjectDepthToPath(path string, transform TransformI, outPath string)

	RenderObjectFromDataToPath(data []byte, transform TransformI, outPath string)
	RenderObjectDepthFromDataToPath(data []byte, transform TransformI, outPath string)

	// ?
	RenderHeightmap(hmData interface{}) image.Image
	// Textured ?
	RenderHeightmapTexture(hmData interface{}, tex image.Image) image.Image
	// See also:
	// https://github.com/anoved/hmstl

	// ?
	RenderHeightmapDepth(hmData interface{}) image.Image

	// ?
	// Mesh data version ?
	RenderObjectMeshData(md *c.MeshData, transform TransformI) image.Image
	RenderObjectMeshDataDepth(md *c.MeshData, transform TransformI) image.Image
	// ?
	RenderObjectMeshDataToPath(md *c.MeshData, transform TransformI, outPath string)
	RenderObjectMeshDataDepthToPath(md *c.MeshData, transform TransformI, outPath string)

	// Extra ?
	//Draw3dLine(p1, p2 [3]float64, color [3]int)
	//Draw3dGrid(pos [3]float64, w, h int, cellSize float64, color [3]int, drawOrigin bool)

	// optional ?
	// Set up camera / view
	// fov is ortho-scale
	//SetupCamera(fov float64, znear, zfar float64, pos, rot [3]float64)

	// ?
	//SetupScene(lights []lightI)

	// extra ?
	// Get mesh voxels ?
	//VoxelizeMesh(mesh interface{}) VoxelData

	// extra ?
	// Get mesh bounding box / aabb ?
	//LoadMesh(path string) mesh
	//RenderMesh(mesh)
	//LoadObject(path string) object
	//RenderObject(object)
	GetObjectBoundingBox(path string) [6]float64
	//GetObjectBoundingBox(path string) [2][3]float64

	/*
	// ?
	RenderObjectOpt(ob interface{}) image.Image

	// ?
	RenderObjectMeshData(md *c.MeshData) image.Image

	// ?
	// Render object from data ?
	RenderObjectData(buf []byte) image.Image
	
	// ?
	// From memory / buffer ?
	RenderObjectData2(f io.Reader) image.Image
	*/

	/*
	// ? Return backend features ?
	QueryFeatures() []string
	*/
}

// Extra ?
type RenderBackendExtI interface {
	//GenCube(sx, sy, sz float64)
	GenCubeMeshData(sx, sy, sz float64) *c.MeshData

	// ?
	//RenderCube(sx, sy, sz int)
	//RenderCubeDepth(size int)

	// ?
	//RenderGen(genData *GenData)
	// mesh gen data = mesh_gen
	// like:
	//   gen_type = cube
	//   // param
	//   size = [10, 10, 10]
	//   (optional) color = [255, 0, 0]
	//   (optional) unshaded = true
	//   (optional) texture = ?
	//RenderMeshGen(meshGenData *meshGenData, transform TransformI) image.Image
	//RenderMeshGenDepth(meshGenData *meshGenData, transform TransformI) image.Image
	// ?
	//RenderMeshGenToPath(meshGenData *meshGenData, transform TransformI, outPath string)
	//RenderMeshGenDepthToPath(meshGenData *meshGenData, transform Transform, outPath string)
}
