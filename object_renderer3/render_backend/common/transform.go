package common

import (
	//"fmt"
)

// Transform data (TransformData ?)
// not an actual transform (?)
type Transform struct {
	pos [3]float64
	rot [3]float64
	scale [3]float64
}

func NewTransform() *Transform {
	t := &Transform{
		scale: [3]float64{1, 1, 1},
	}
	
	return  t
}

func (t *Transform) Pos() [3]float64 {
	return t.pos
}

func (t *Transform) SetPos(v [3]float64) {
	t.pos = v
}

func (t *Transform) Rot() [3]float64 {
	return t.rot
}

func (t *Transform) SetRot(v [3]float64) {
	t.rot = v
}

func (t *Transform) Scale() [3]float64 {
	return t.scale
}

func (t *Transform) SetScale(v [3]float64) {
	t.scale = v
}
