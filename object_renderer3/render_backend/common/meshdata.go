package common

import (
	"image"
)

// Based on MeshBuffer ?
type MeshData struct {
	Positions []float32
	Normals   []float32
	Texcoords []float32
	Indices   []uint32
	//HasPositions bool
	//HasNormals bool
	//HasTexCoords bool
	//HasIndices bool

	//// XXX
	// Maybe not really mesh data
	// but mesh gen data
	// Can be put into a struct (like MeshGenData, or just MeshGen (?))
	// or just 'internal' struct ?
	// But is still used for generation

	// Optional diffuse texture ?
	Texture image.Image

	// ? (optional ?)
	// Optional fill color / diffuse color (?)
	//FillColor [4]float64
	//FillColor [4]int
	//Color [3]float64
	//Color *[3]float64
	Color *[3]int
}

func NewMeshData() *MeshData {
	md := &MeshData{
		Positions: make([]float32, 0),
		Normals: make([]float32, 0),
		Texcoords: make([]float32, 0),
		Indices: make([]uint32, 0),
	}

	return md
}

func (md *MeshData) HasPositions() bool {
	return len(md.Positions) > 0
}

func (md *MeshData) HasNormals() bool {
	return len(md.Normals) > 0
}

func (md *MeshData) HasTexcoords() bool {
	return len(md.Texcoords) > 0
}

func (md *MeshData) HasIndices() bool {
	return len(md.Indices) > 0
}
