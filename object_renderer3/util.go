package main

import (
	"strings"
	"strconv"

	c "test/common"
)

func parseVec(s string) [3]float64 {
	r := strings.Split(s, ",")
	var v [3]float64
	for i := 0; i < 3; i++ {
		x, err := strconv.ParseFloat(r[i], 64)
		if err != nil {
			panic(err)
		}
		//v = append(v, x)
		v[i] = x
	}
	//panic(v)
	return v
}

// ?


var GetTmpDir = c.GetTmpDir