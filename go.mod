module test

go 1.20

require (
	github.com/gen2brain/raylib-go/raylib v0.0.0-20230621111137-9def9b04c9ec
	github.com/go-gl/mathgl v1.0.0
)

require golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
