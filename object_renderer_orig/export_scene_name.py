import bpy
import sys
import os

print("->", sys.argv)
print("export active scene name")

tmpdir = "/tmp/object_renderer"
if not os.path.exists(tmpdir):
	os.mkdir(tmpdir)

f = open(tmpdir + "/" + "scene_name.txt", "w")
#f.write("test\n")
f.write(bpy.context.scene.name + "\n")
#f.write("\n")
f.close()

print("done")
