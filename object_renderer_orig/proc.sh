#!/bin/bash

background=""
#background="--background"
blender=blender-2.7


#$blender "$1" $background --python render.py


# Export active scene's name
$blender "$1" -b --python export_scene_name.py #&& true
scene_name=`cat /tmp/object_renderer/scene_name.txt | tr -d '\n'`


$blender res/render_scene.blend $background --python render.py -- "$1" "$scene_name"
