#!/bin/bash -x

## https://www.imagemagick.org/Usage/transform/#gridding

#  magick rose: -background SkyBlue \
#          -crop 10x0 +repage -splice 3x0 +append \
#          -crop 0x10 +repage -splice 0x3 -append \
#          grid_tile.png

convert /tmp/test_image.png -background cyan \
          -crop 512x0 +repage -splice 3x0 +append \
          -crop 0x512 +repage -splice 0x3 -append \
          /tmp/grid_tile.png
