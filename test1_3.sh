#!/bin/bash -x

## https://www.imagemagick.org/Usage/transform/#gridding

#  magick rose: -background SkyBlue \
#          -crop 10x0 +repage -splice 3x0 +append \
#          -crop 0x10 +repage -splice 0x3 -append \
#          grid_tile.png

convert /tmp/test_image.png -background cyan \
          -crop 512x512 +repage -border 1 +adjoin \
          -pointsize 24 +repage -annotate +0+0 %[fx:t] +gravity \
          +append -crop 5x1@ -append +repage \
          PNG24:/tmp/grid_tile.png
