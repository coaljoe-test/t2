// Get bounding box (volume) of entire map (?)
func GetMapBoundingBox() BBox {
	...
}

// Get bounding box of a scene
func GetSceneBoundingBox() BBox {
	...
}