#!/bin/bash -x

mkdir -p /tmp/out_tiles

convert /tmp/test_image.png -background cyan \
          +repage -crop 24x24 \
          /tmp/out_tiles/tile%04d.png
