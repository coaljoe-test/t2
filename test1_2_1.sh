#!/bin/bash -x

## https://www.imagemagick.org/Usage/transform/#gridding

#  magick rose: -background SkyBlue \
#          -crop 10x0 +repage -splice 3x0 +append \
#          -crop 0x10 +repage -splice 0x3 -append \
#          grid_tile.png


#          xc:none -gravity Center -pointsize 18 -weight Bold -fill Blue \
#          -annotate 0 "test" \

#          -crop 512x0 +repage -splice 3x0 +append \
#          -crop 0x512 +repage -splice 0x3 -append \

#          miff:- | magick - "z.jpg"


convert \( /tmp/test_image.png -background cyan \
          -crop 512x512 -border 1 +repage \
          -pointsize 300 -gravity Center -annotate 0 "%[p]" \
        \) \
	      -append \
	      z.png
