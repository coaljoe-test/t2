#!/bin/bash

#mkdir -p /tmp/out_tiles/tiles_seq
#mkdir -p /tmp/out_tiles/tiles
mkdir -p /tmp/out_tiles/
mkdir -p /tmp/tmp_tiles_seq

in_file=/tmp/test_image.png

## XXX
rm -rf /tmp/tmp_tiles_seq/*
rm -rf /tmp/out_tiles/*

## XXX
file_ext=""${in_file#*.}""

echo 
echo "in_file: $in_file"
echo "file_ext: $file_ext"
echo 

## Seq
(
set -x
convert $in_file \
          +repage -crop 512x512 \
          /tmp/tmp_tiles_seq/tile_%04d.$file_ext
)


tile_size=512
image_w=$(identify -format "%w" $in_file)
image_h=$(identify -format "%h" $in_file)
num_tiles_x=$(echo $image_w / $tile_size | bc)
num_tiles_y=$(echo $image_h / $tile_size | bc)

echo
echo "tile_size: $tile_size"
echo "image_w: $image_w"
echo "image_h: $image_h"
echo "num_tiles_x: $num_tiles_x"
echo "num_tiles_y: $num_tiles_y"
echo

## "Renames"

for y in $(seq 0 $(( $num_tiles_y - 1  )) )
do
    for x in $(seq 0 $(( $num_tiles_x - 1 )) )
    do
    	echo "->"
        echo "x: $x" 
        echo "y: $y"
        #fn="tile_$x,$y"
        
        fn=$(printf "tile_%04d,%04d" "$x" "$y")
        #fn=$(printf "tile_%04d,%04d" "$y" "$x")
        #fn=$(printf "tile_%03d,%03d" "$y" "$x")

        tile_id=$(( $y * $num_tiles_y + $x ))

        xtile_id=$(printf "%04d" "$tile_id")
        
        echo "tile_id: $tile_id"
        
        echo "xtile_id: $xtile_id"
        
        echo "fn: $fn"

        cmd="cp /tmp/tmp_tiles_seq/tile_$xtile_id.$file_ext "
        cmd+="/tmp/out_tiles/$fn.$file_ext"
        
        echo "cmd: $cmd"        
        eval $cmd
    done
done

