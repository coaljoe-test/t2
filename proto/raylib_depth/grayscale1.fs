#version 120


// Input vertex attributes (from vertex shader)
varying vec2 fragTexCoord;
varying vec4 fragColor;

// Input uniform values
uniform sampler2D texture0;
uniform vec4 colDiffuse;

// NOTE: Add here your custom variables

void main()
{
    // Texel color fetching from texture sampler
    //vec4 texelColor = texture2D(texture0, fragTexCoord)*colDiffuse*fragColor;

    // Convert texel color to grayscale using NTSC conversion weights
    //float gray = dot(texelColor.rgb, vec3(0.299, 0.587, 0.114));

    // Calculate final fragment color
    //gl_FragColor = vec4(gray, gray, gray, texelColor.a);

	//gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);

	gl_FragColor = vec4(gl_FragCoord.z);

	/*
	float ndcDepth = ndcPos.z =
      (2.0 * gl_FragCoord.z - gl_DepthRange.near - gl_DepthRange.far) /
      (gl_DepthRange.far - gl_DepthRange.near);
	//float ndcPos.z =
	float clipDepth = ndcDepth / gl_FragCoord.w;

	gl_FragColor = vec4((clipDepth * 0.5) + 0.5); 
	*/

	//float zbuffer = gl_FragCoord.w * 500.0;
	//float zbuffer = gl_FragCoord.w;   
	float zbuffer = gl_FragCoord.z * 50;
	gl_FragColor = vec4(zbuffer, zbuffer, zbuffer, 1.0);


	//gl_FragColor = vec4(gl_FragCoord.x, gl_FragCoord.y, gl_FragCoord.z, 1.0);


	//gl_FragColor = vec4(0.0, 1.0, 0.0, 1.0);

}
