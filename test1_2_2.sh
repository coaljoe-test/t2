#!/bin/bash -x

mkdir -p /tmp/tmp_tiles_1

# XXX
rm -rf /tmp/tmp_tiles_1/*

## https://www.imagemagick.org/Usage/transform/#gridding

#  magick rose: -background SkyBlue \
#          -crop 10x0 +repage -splice 3x0 +append \
#          -crop 0x10 +repage -splice 0x3 -append \
#          grid_tile.png


#          xc:none -gravity Center -pointsize 18 -weight Bold -fill Blue \
#          -annotate 0 "test" \

#          -crop 512x0 +repage -splice 3x0 +append \
#          -crop 0x512 +repage -splice 0x3 -append \

in_file=/tmp/test_image.png

tile_size=512
image_w=$(identify -format "%w" $in_file)
image_h=$(identify -format "%h" $in_file)
num_tiles_x=$(echo $image_w / $tile_size | bc)
num_tiles_y=$(echo $image_h / $tile_size | bc)

echo
echo "tile_size: $tile_size"
echo "image_w: $image_w"
echo "image_h: $image_h"
echo "num_tiles_x: $num_tiles_x"
echo "num_tiles_y: $num_tiles_y"
echo

convert /tmp/test_image.png -background cyan \
          -crop 512x512 -border 1 +repage +adjoin \
          -pointsize 300 -gravity Center -annotate 0 "%[p]" \
          PNG24:/tmp/tmp_tiles_1/tile_%04d.png


#montage -mode concatenate -tile 4x  /tmp/tmp_tiles_1/tile_\*.png \
montage -mode concatenate -tile $num_tiles_x  /tmp/tmp_tiles_1/tile_*.png \
        /tmp/z.png
