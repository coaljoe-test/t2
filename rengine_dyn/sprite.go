package rengine

import (
	//"image"
	"github.com/veandco/go-sdl2/sdl"

)

// SpriteRegion ?
type SpriteImage struct {
	// Pos
	X int
	Y int
	// Size
	W int
	H int

	// On/off ?
	spriteState bool

	//Depth image.Image
	DepthTex *sdl.Texture
	Tex *sdl.Texture

}

func NewSpriteImage() *SpriteImage {
	si := &SpriteImage{}

	return si
}

func NewSpriteImageFromFile(path, pathDepth string) {
	si := NewSpriteImage()
	si.Load(path, pathDepth)
}

func (si *SpriteImage) Load(path, pathDepth string) {
	tex := SdlLoadPngTex(path)
	texDepth := SdlLoadPngTex(pathDepth)

	_ = tex
	_ = texDepth
}
