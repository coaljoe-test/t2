#!/bin/bash -x

./crop_image.sh 256x256 a_d_conv16.png tmp/out_image1.png
./crop_image.sh 256x256 b_d_conv16.png tmp/out_image2.png
./crop_image.sh 256x256 bg_d_conv16.png tmp/out_image3.png

./tile_images.sh tmp/out_image*.png tmp/result_depth.png