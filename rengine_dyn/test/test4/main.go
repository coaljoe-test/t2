package main

import (
	"fmt"
	"time"
	ren "test/rengine"
)

func main() {
	fmt.Println("main()")

	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	//im1 := ren.LoadPng("test_gray8_depth.png")
	//im2 := ren.LoadPng("test_gray8_depth.png")
	im1 := ren.LoadPng("../../bg_d_conv16.png")
	im2 := ren.LoadPng("../../bg_conv8.png")
	im3 := ren.LoadPng("../../a_d_conv16.png")
	im4 := ren.LoadPng("../../a_conv8.png")
	im5 := ren.LoadPng("../../b_d_conv16.png")
	im6 := ren.LoadPng("../../b_conv8.png")
	_ = im1
	_ = im2
	_ = im3
	_ = im4
	_ = im5
	_ = im6

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	//ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)
	
	// 0 = bg
	// 0 + A = A
	// 0 + A + B = AB ?
	// 0 + (A,B); - (A) = 0 + (B) ?
	// 0 + (A,B) = 0 + (A,B) ?

	t := time.Now()
	// 0 + A
	// (add A)
	fmt.Println("(add A)")
	out, outDepth := ren.SWDepthMergeImages16(im1, im2, im3, im4, true)
	fmt.Println("d:", time.Now().Sub(t))

	// (in) [0 + A]
	// 0 + (A,B) = 0 + AB
	// (add B)
	fmt.Println("(add B)")
	out1, outDepth1 := ren.SWDepthMergeImages16(outDepth, out, im5, im6, true)
	fmt.Println("d1:", time.Now().Sub(t))

	ren.SavePng(out, "/tmp/out.png")
	ren.SavePng(outDepth, "/tmp/out_depth.png")

	ren.SavePng(out1, "/tmp/out1.png")
	ren.SavePng(outDepth1, "/tmp/out_depth1.png")

	if true {
		// (in) [0 + AB]
		// 0 + (A,B) - (B) = 0 + A
		// (sub B)
		fmt.Println("(sub B)")
		// Full rebuild ?
		// 0 + A
		// bg + A set
		out2, outDepth2 := ren.SWDepthMergeImages16(im1, im2, im3, im4, true)
		
		// Same as add a ?
		ren.SavePng(out2, "/tmp/out2.png")
		ren.SavePng(outDepth2, "/tmp/out_depth2.png")
	}

	fmt.Println("exiting...")
}
