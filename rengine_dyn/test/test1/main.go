package main

import (
	"fmt"
	"time"
	
	ren "test/rengine"
)

func main() {
	fmt.Println("main()")
	
	fmt.Println(ren.Name)
	
	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	//im1 := ren.LoadPng("test_gray8_depth.png")
	//im2 := ren.LoadPng("test_gray8_depth.png")
	im1 := ren.LoadPng("/tmp/q1.png")
	im2 := ren.LoadPng("/tmp/q2.png")
	_ = im1
	_ = im2

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	//ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)

	t := time.Now()
	//outIm := ren.MergeDepthImagesGray2(im1, im2, false)
	//outIm := ren.MergeDepthImagesGray2(im1, im2, true)
	outIm := ren.MergeDepthImagesGray16(im1, im2, true)
	fmt.Println("d:", time.Now().Sub(t))

	ren.SavePng(outIm, "/tmp/out.png")

}
