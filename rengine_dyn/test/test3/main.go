package main

import (
	"fmt"
	"time"
	ren "test/rengine"
)

func main() {
	fmt.Println("main()")

	//im := ren.LoadPng("depth.png")
	//im := ren.LoadPng("test_gray16_depth16.png")
	//im1 := ren.LoadPng("test_gray8_depth.png")
	//im2 := ren.LoadPng("test_gray8_depth.png")
	im1 := ren.LoadPng("../../bg_d_conv16.png")
	im2 := ren.LoadPng("../../bg_conv8.png")
	im3 := ren.LoadPng("../../a_d_conv16.png")
	im4 := ren.LoadPng("../../a_conv8.png")
	_ = im1
	_ = im2
	_ = im3
	_ = im4

	//ren.MulImageDataGray(im, 1.0)
	//ren.MulImageDataGray(im, 2.0)
	//ren.MulImageDataGray(im, 0.5)
	//ren.MulImageDataGray(im, 10.0)

	t := time.Now()
	out, outDepth := ren.SWDepthMergeImages16(im1, im2, im3, im4, true)
	fmt.Println("d:", time.Now().Sub(t))

	ren.SavePng(out, "/tmp/out.png")
	ren.SavePng(outDepth, "/tmp/out_depth.png")


	fmt.Println("exiting...")
}
