package rengine

import (
	"fmt"
//	"os"
	"image"
//_	"image/png"
	"image/color"
	"math"
)

// ? Gray 8 ?
// in-place ?
func MulImageDataGray(im image.Image, v float64) {
	fmt.Println("MulImageDataGray v:", v)

	cm := im.ColorModel()
	fmt.Println("cm: ", cm)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	imGray := im.(*image.Gray)

	b := im.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c := imGray.GrayAt(x, y)

			//fmt.Println("c:", c)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			 
			oldV := c.Y
			oldFloatV := float64(oldV)/255.0
			// clamp ?
			newFloatV := math.Min(oldFloatV * v, 1.0)
			newV := int(newFloatV*255.0)
			_ = newV

			//fmt.Println("oldFloatV:", oldFloatV)
			//fmt.Println("newFloatV:", newFloatV)
			//fmt.Println("newV:", newV)

			// clamp ?
			newV2 := int(math.Min(float64(oldV) * v, 255.0))
			//fmt.Println("newV2:", newV2)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV2)}
				imGray.SetGray(x, y, newC)
			}
		}
	}
}


// ? Gray 8 ?
// Combine depths ?
func MergeDepthImagesGray(inIm1, inIm2 image.Image) image.Image {
	fmt.Println("DepthMergeImageGray")

	return MergeDepthImagesGray2(inIm1, inIm2, false)
}

// ? Gray 8 ?
// Combine depths ?
func MergeDepthImagesGray2(inIm1, inIm2 image.Image, invert bool) image.Image {
	fmt.Println("DepthMergeImageGray2 invert:", invert)

	cm1 := inIm1.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	im1 := inIm1.(*image.Gray)
	im2 := inIm2.(*image.Gray)

	out := image.NewGray(im1.Bounds())

	b := im1.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := im1.GrayAt(x, y)
			c2 := im2.GrayAt(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			if !invert {
				if c2.Y > c1.Y {
					newV = c2.Y
				}
			} else {
				if c2.Y < c1.Y {
					newV = c2.Y
				}
			}	

			
			
			// clamp ?
			//fmt.Println("newV2:", newV)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV)}
				out.SetGray(x, y, newC)
			}
		}
	}

	return out
}

// ? Gray 16 ?
// Combine depths ?
func MergeDepthImagesGray16(inIm1, inIm2 image.Image, invert bool) image.Image {
	fmt.Println("DepthMergeImageGray16 invert:", invert)

	cm1 := inIm1.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	im1 := inIm1.(*image.Gray16)
	im2 := inIm2.(*image.Gray16)

	out := image.NewGray16(im1.Bounds())

	b := im1.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := im1.Gray16At(x, y)
			c2 := im2.Gray16At(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			if !invert {
				if c2.Y > c1.Y {
					newV = c2.Y
				}
			} else {
				if c2.Y < c1.Y {
					newV = c2.Y
				}
			}	

			
			
			// clamp ?
			//fmt.Println("newV2:", newV)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray16{uint16(newV)}
				out.SetGray16(x, y, newC)
			}
		}
	}

	return out
}

// ? Gray 8 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
// invertDepth - invert the depth textures / values
func SWDepthMergeImages(bgDepth, bgColor, objDepth, objColor image.Image,
 invertDepth bool) (image.Image, image.Image) {
	fmt.Println("SWDepthMergeImages invertDepth:", invertDepth)

	cm1 := bgColor.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	//im1 := inIm1.(*image.Gray)
	//im2 := inIm2.(*image.Gray)

	out := image.NewNRGBA(bgColor.Bounds())
	outDepth := image.NewGray(bgColor.Bounds())


	b := out.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := bgDepth.(*image.Gray).GrayAt(x, y)
			c2 := objDepth.(*image.Gray).GrayAt(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			x1 := bgColor.(*image.NRGBA).NRGBAAt(x, y)
			x2 := objColor.(*image.NRGBA).NRGBAAt(x, y)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			newVC := x1
			if !invertDepth {
				if c2.Y > c1.Y {
					newV = c2.Y
					newVC = x1
				}
			} else {
				if c2.Y < c1.Y {
					newV = c2.Y
					newVC = x2
				}
			}


			
			
			// clamp ?
			//fmt.Println("newV2:", newV)

			if true {
				//newC := color.Gray{uint8(newV)}
				//newC := color.Gray{uint8(newV)}
				out.SetNRGBA(x, y, newVC)
			}

			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV)}
				outDepth.SetGray(x, y, newC)
			}
		}
	}

	return out, outDepth
}

// ? Gray 16 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
// invertDepth - invert the depth textures / values
func SWDepthMergeImages16(bgDepth, bgColor, objDepth, objColor image.Image,
 invertDepth bool) (image.Image, image.Image) {
	fmt.Println("SWDepthMergeImages16 invertDepth:", invertDepth)

	cm1 := bgColor.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }
	
	out := image.NewNRGBA(bgColor.Bounds())
	outDepth := image.NewGray16(bgColor.Bounds())


	b := out.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := bgDepth.(*image.Gray16).Gray16At(x, y)
			c2 := objDepth.(*image.Gray16).Gray16At(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			x1 := bgColor.(*image.NRGBA).NRGBAAt(x, y)
			x2 := objColor.(*image.NRGBA).NRGBAAt(x, y)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			newVC := x1
			if !invertDepth {
				if c2.Y > c1.Y {
					newV = c2.Y
					newVC = x1
				}
			} else {
				if c2.Y < c1.Y {
					newV = c2.Y
					newVC = x2
				}
			}

			
			// clamp ?
			//fmt.Println("newV2:", newV)

			if true {
				//newC := color.Gray{uint8(newV)}
				//newC := color.Gray{uint8(newV)}
				out.SetNRGBA(x, y, newVC)
			}

			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray16{uint16(newV)}
				outDepth.SetGray16(x, y, newC)
			}
		}
	}

	return out, outDepth
}


var _ = `
// ? Gray 8 ?
// bgDepth = background depth map texture
// bgColor = background color texture
// objDepth = foreground/object depth map texture
// objColor = foreground/object color / pixels texture
func SWDepthMergeImages(bgDepth, bgColor, objDepth, objColor image.Image) image.Image {
	fmt.Println("SWDepthMergeImages")

	cm1 := inIm1.ColorModel()
	fmt.Println("cm1: ", cm1)
	//fmt.Printf("%#T\n", cm)

	//_panic(2)

	//if true { panic(2) }

	im1 := inIm1.(*image.Gray)
	im2 := inIm2.(*image.Gray)

	out := image.NewGray(im1.Bounds())

	b := im1.Bounds()
	w, h := b.Max.X, b.Max.Y

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			//fmt.Printf("->  x=%d, y=%d\n", x, y)
		
			//c := im.At(x, y)
			c1 := im1.GrayAt(x, y)
			c2 := im2.GrayAt(x, y)

			//fmt.Println("c1:", c1)
			//fmt.Println("c2:", c2)
			
			//r, g,  b, a := c.RGBA()
			//_ = a
			//fmt.Println("r:", r, "g:", g, "b:", b, "a:", a)
			//fmt.Println("r:", r, "g:", g, "b:", b)

			newV := c1.Y
			if c2.Y > c1.Y {
				newV = c2.Y
			}

			
			
			// clamp ?
			//fmt.Println("newV2:", newV)


			if true {
				//newC := color.Gray{uint8(newV)}
				newC := color.Gray{uint8(newV)}
				out.SetGray(x, y, newC)
			}
		}
	}

	return out
}
`
