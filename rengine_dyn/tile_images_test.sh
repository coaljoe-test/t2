#!/bin/bash -x

./crop_image.sh 256x256 a_conv8.png tmp/out_image1.png
./crop_image.sh 256x256 b_conv8.png tmp/out_image2.png
./crop_image.sh 256x256 bg_conv8.png tmp/out_image3.png

./tile_images.sh tmp/out_image*.png tmp/result.png