package rengine

import (
	"fmt"
	"os"
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
)

// Load png texture ?
func SdlLoadPngTex(path string) *sdl.Texture {
	fmt.Println("SdlLoadPngTex path:", path)

	//_panic(3)

	f, err := os.Open(path)
    if err != nil {
        panic(err)
    }
    _ = f

    image, err := img.Load(path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to load PNG: %s\n", err)
		panic(2)
	}
	//defer image.Free()

	var texture *sdl.Texture
	//texture, err = renderer.CreateTextureFromSurface(image)
	texture, err = sdlCtx.Renderer.CreateTextureFromSurface(image)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to create texture: %s\n", err)
		panic(2)
	}
	//defer texture.Destroy()

	// XXX ?
	image.Free()

    return texture
}

