module common/types

go 1.22.3

require kristallos.ga/rx v0.0.0-20240612080209-1fc7c212cf81

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4 // indirect
	github.com/bitly/go-simplejson v0.5.1 // indirect
	github.com/cheekybits/genny v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/disintegration/imaging v1.6.2 // indirect
	github.com/emirpasic/gods v1.18.1 // indirect
	github.com/fossoreslp/go-uuid-v4 v1.0.0 // indirect
	github.com/go-gl/gl v0.0.0-20231021071112-07e5d0ea2e71 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20240506104042-037f3cc74f2a // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/iancoleman/orderedmap v0.3.0 // indirect
	golang.org/x/image v0.18.0 // indirect
	kristallos.ga/lib/debug v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/ider v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/pubsub v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/sr v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/lib/xlog v0.0.0-20231206203330-ec813b10aad7 // indirect
	kristallos.ga/rx/math v0.0.0-20240612080209-1fc7c212cf81 // indirect
	kristallos.ga/rx/phys v0.0.0-20240612080209-1fc7c212cf81 // indirect
	kristallos.ga/rx/transform v0.0.0-20240612080209-1fc7c212cf81 // indirect
)

//replace kristallos.ga/rx => ../../../rx_upstream
