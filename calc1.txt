- calculate object (mesh ?) dimencions, based on it bounding box / volume / size
- calculate image / object "extents", in 2d and 3d ?
  - projected
- for various came / scale / angle settings ?
  - what angle is actually used ?
  - real / game isometry ?
- how many tiles an object render can take
  - 2x2, 1x2, 3x2 etc
- how many pixels / size the render can take (of object or mesh)
  - estimate ?
  - should big objects get rendered in tiles ?
    - ? GL_MAX_VIEWPORT_DIMS [https://www.khronos.org/opengl/wiki/GLAPI/glViewport]
    - 8192 x 8192 (?)
      - how many it is in "world" dimentions/units ?
    - GL_MAX_RENDERBUFFER_SIZE ?
      - 4096x4096 (?)
  - what is the biggest object / limitation ?
  - what is the max bounding volume for render, or for object ?
- tile object rendering ?
  - as a mode ?
  - or default ?
- 3d bounding box to 2d bounding box

info:
  - https://blender.stackexchange.com/questions/280844/how-to-get-the-2d-bounding-box-of-a-3d-object-using-the-python-bpy-module [https://archive.is/9mnHl]
  - https://blog.csdn.net/yujie181152/article/details/105061131 [https://archive.is/le6dR]



test1:

scale = 4.0

res = 512,512

tile_size = 512 (?)

// camera
camera angle = ?
camera matrix = ?

// 3d
// bounding box 3d
object_bound_size = ?
object_bound_size = [10, 10, 10]
object_bounds_3d = ?
object_bounding_box_3d = [10, 10, 10]

// 2d extents
// 2d bounding box
object_bounds_2d = ?
object_bounding_box_2d = ?