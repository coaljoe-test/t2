#!/usr/bin/env python

import sys, os, json


path = sys.argv[1]
print('path:', path)


## List scenes and sub-scenes

def load_json(path):
    f = open(path, 'r')
    d = json.load(f)
    print("d:", d)

    if d['_type'] != 'scene':
        print('wrong type? _type:', d['_type'])
        exit(2)

    print()
    print('nodes:', d['nodes'])
    print('scene ({}) nodes'.format(len(d['nodes'])))


def main():
    load_json(path)


main()