package main

import (
	"fmt"
	//glm "github.com/technohippy/go-glmatrix"
	//lm "github.com/xlab/linmath"
	. "github.com/go-gl/mathgl/mgl64"
	"github.com/gen2brain/raylib-go/raylib"
	
)

func main() {
	fmt.Println("main()")

	////  XXX init ?
	
	//
	// Viz XXX separate ?
	//
	rl.SetConfigFlags(rl.FlagVsyncHint)
	rl.InitWindow(800, 450, "")

	rl.SetTargetFPS(60)


	// Default / start cam position ?
	camPosition := rl.NewVector3(0.0, 10.0, 10.0)

	camera := rl.Camera3D{}
	camera.Position = camPosition
	camera.Target = rl.NewVector3(0.0, 0.0, 0.0)
	camera.Up = rl.NewVector3(0.0, 1.0, 0.0)
	//camera.Fovy = 45.0
	camera.Fovy = 20.0
	//camera.Projection = rl.CameraPerspective
	camera.Projection = rl.CameraOrthographic

	cubePosition := rl.NewVector3(0.0, 0.0, 0.0)
	_ = cubePosition

	//shiftVec := glm.NewVec3()
	shiftAmt := 0.2

	//rl.BeginDrawing()
	//rl.BeginMode3D(camera)
	//mesh := rl.GenMeshCube(1.0, 1.0, 8.0)
	//model := rl.LoadModelFromMesh(mesh)
	model := rl.LoadModel("eiffel_tower.obj")
	_ = model
	//rl.EndMode3D()
	//rl.EndDrawing()

	//
	// Test
	//
	if true {
		//sb.MoveTo(Vec3{10, 0, 0}, 1.0)
	}


	orbitalCam := false
	// Set an orbital camera mode
	//rl.SetCameraMode(camera, rl.CameraOrbital)
	//camMode := rl.CameraFree
	//camMode := rl.CameraCustom
	defaultCamMode := rl.CameraMode(0)
	// curCamMode ?
	camMode := defaultCamMode
	_ = camMode

	cubePos := Vec3{}
	_ = cubePos

	for !rl.WindowShouldClose() {
		// XXX input
		{
			// Toggle Camera
			if rl.IsKeyPressed(rl.KeySpace) {
				//rl.SetCameraMode(camera, 0)
				orbitalCam = !orbitalCam
				if orbitalCam {
					//rl.SetCameraMode(camera, rl.CameraOrbital)
					camMode = rl.CameraOrbital
				} else {
					//rl.SetCameraMode(camera, 0)
					//camMode = rl.CameraFree
					//camMode = rl.CameraCustom
					camMode = defaultCamMode
					//camera.Position = rl.NewVector3(0.0, 10.0, 10.0)
					camera.Position = camPosition
				}
			}
		
			newPos := Vec3{}
			//newPos := sb.Pos()
		
			if rl.IsKeyDown(rl.KeyRight) {
				newPos[0] += shiftAmt
			}
			if rl.IsKeyDown(rl.KeyLeft) {
				newPos[0] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyUp) {
				//b.pos[1] -= shiftAmt
				newPos[2] -= shiftAmt
			}
			if rl.IsKeyDown(rl.KeyDown) {
				//b.pos[1] += shiftAmt
				newPos[2] += shiftAmt		
			}

			// XXX ?
			cubePos = newPos
		}

		//rl.UpdateCamera(&camera, camMode) // Update camera
		//rl.UpdateCamera(&camera, 0) // Update camera
	
		if orbitalCam {
			rl.UpdateCamera(&camera, camMode) // Update camera
		}
	
		rl.BeginDrawing()

		rl.ClearBackground(rl.RayWhite)

		rl.BeginMode3D(camera)

		//rl.DrawCube(cubePosition, 2.0, 2.0, 2.0, rl.Red)
		//rl.DrawCubeWires(cubePosition, 2.0, 2.0, 2.0, rl.Maroon)
		if true {
			x := rl.NewVector3(float32(cubePos[0]), float32(cubePos[1]), float32(cubePos[2]))
			rl.DrawCube(x, 2.0, 2.0, 2.0, rl.Red)
			rl.DrawCubeWires(x, 2.0, 2.0, 2.0, rl.Maroon)
		}

		if true {
			x := rl.NewVector3(0, 0, 0)
			rl.DrawModel(model, x, 1.0, rl.White)
		}

		rl.DrawGrid(10, 1.0)

		rl.EndMode3D()


		//rl.DrawText("Congrats! You created your first window!", 190, 200, 20, rl.LightGray)

		rl.EndDrawing()
	}

	rl.CloseWindow()
}
