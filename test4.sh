#!/bin/bash -x

## Info:
## https://stackoverflow.com/questions/74043350/image-magic-select-orientation-of-the-stacked-image


#montage -border 1 -mode concatenate -tile 8x8 -border 1 /tmp/out_tiles/*.png         /tmp/x4.png


convert /tmp/out_tiles/*.png -transpose miff:- | montage - -tile 8x8 miff:- | convert - -transpose /tmp/result.png
