
- map editor
  - in game
  - needs some sort of preview ?
- editor depends on game
- editor depends on game render ?
- map_compiler module
- map_compiler standalone / cli ?
- object compiler ?
- object_renderer module
- object_renderer standalone / cli ?
- support for z height ?
- support for height maps ?
- simple
- modular ?

todo:
- test map_compiler
- test map format
- rendering shadows ?
- ? compile_tile_test test
  - and speed test
  - change / update tile test
  - multi tile / big objects ?
- ? world view ?
- ? render entire world ?
- ? render with top-bottom camera ?
- ? draw bounding box for objects (post) ?

map format:
- nodes
- nodes have types, like building or other
- nodes are objects ?


ideas:
- tunable hue etc for map objecs ?
- also global parameter ?
- loading only compiled maps in runtime for simplicity ?
- having both res and res_src (optional) for maps ?
  - res_src contain 3d sources or objects, or original .blend files ?
  - each map has this ?
- [v] rengine modele for rendering
- make editor a separate module ?
- [v] rendering backend api / service
  - for rendering backend
  - backends:
    - ? rx
    - ? raylib
    - ? custom/internal (mini renderer)
    - ? pyrender
- ? evaluate heightmap use
- depth atlas with all objects and tiles that make a depth tile when merged/combined
  - do they need masks ?
  - same needed for non-depth tiles ?  
- parts / multi part objects ?
  - nested / compound object / object groups ?
  - sub objects ?
